<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/create','ApiController@create');
Route::post('/accept_payment','PaymentController@store');
Route::post('/init','ApiController@api');
Route::middleware('auth:api')->get('/user', function (Request $request) {
    Route::post('/createInvoice','ApiController@create');
    return $request->user();
});
