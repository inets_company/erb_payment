<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
Route::get('/resentAllBarcodes', function () {
    $payments = \App\Model\Payment::all();
    $api = (new \App\Http\Controllers\ApiController());
    foreach ($payments as $payment) {
        $api->sendBarcodeForm($payment->id);
    }
});

Auth::routes();
Route::get('/testing', 'ApiController@sendBarcodeForm');

Route::group(['middleware' => 'web'], function() {
    Route::get('/find', 'HomeController@search');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/user/profile/{id}', 'UserController@profile');
    Route::get('/user/ticket/{id}', 'UserController@ticket');
    Route::get('/myticket/{id}', 'SettingController@downloadTicket');

    Route::get('/user/nametag/{id}', 'UserController@nametag');
    Route::any('/user/password', 'UserController@password');
    Route::get('/download/{id}', 'BookingController@downloadPdf');
    Route::get('/search', 'BookingController@search')->name('search');
    Route::get('/search_certificate', 'certificateController@search');
    Route::get('/invoice/feeDelete/{id}', 'InvoiceController@feeDelete');
    Route::post('/invoice/addUserFee/{id}', 'InvoiceController@addUserFee');
    Route::any('/booking/{id}/card', 'PaymentController@card');
    Route::post('/setting/getedit', 'SettingController@getEdits');
   

    Route::resource('booking', 'BookingController');
    Route::resource('invoice', 'InvoiceController');
    Route::resource('payment', 'PaymentController');
    Route::resource('user', 'UserController');
    Route::resource('inbox', 'InboxController');
    Route::resource('setting', 'SettingController');
    Route::resource('certificate', 'certificateController');
    
     Route::get('/user/applicants/{id?}/{pg?}','UserController@show');
});

Route::get('/', 'BookingController@landing');
Route::get('/create_billing', 'InvoiceController@createBilling');
Route::get('/update_billing', 'InvoiceController@updateBilling');


