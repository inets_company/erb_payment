<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use \App\Http\Controllers\BookingController;
use \App\Http\Controllers\ApiController;
use \App\Model\Payment;
use \App\Model\Invoice;
use DB;

class Kernel extends ConsoleKernel {

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
            // \App\Console\Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule) {
        $schedule->call(function () {
            $this->run();
        })->everyMinute();
      
        $schedule->call(function () {
            $this->sendPaymentReminder();
        })->fridays();
    }

    public function run() {
        $this->checkSchedule();
    }

    public function sendSms() {
        $messages = \App\Model\Sms::where('status', 0)->limit(8)->get();
        $setting = \App\Model\Setting::first();
        if (count($messages) > 0) {
            foreach ($messages as $sms) {
                $karibusms = new \karibusms();
                $karibusms->API_KEY = $setting->api_key;
                $karibusms->API_SECRET = $setting->api_secret;
                $karibusms->set_name(strtoupper('ERB'));
                $karibusms->karibuSMSpro = $setting->sms_type;
                $result = (object) json_decode($karibusms->send_sms($sms->phone, $sms->body));
                $sms->update(['status' => 1,
                    'return_code' => json_encode($result), 'updated_at' => 'now()']);
            }
        }
        return $this->sendEmails();
    }

    public function sendEmails() {
        $emails = \App\Model\Email::where('status', 0)->limit(6)->get();
        if (count($emails) > 0) {
            foreach ($emails as $message) {
                if (filter_var($message->email, FILTER_VALIDATE_EMAIL)) {
                    try {
                        $data = ['content' => $message->body, 'link' => 'www.engineersday.co.tz', 'name' => $message->user->name];

                        \Mail::send('email.page', $data, function ($m) use ($message) {
                            $m->from('inetsnoreply@inetstz.com', 'Engineers Registration Board -AED2018');
                            if (preg_match('/Barcode/',$message->subject)) {
                                $m->bcc('engineersday2017@gmail.com');
                            }
                            $m->to($message->email)->subject($message->subject);
                        });
                        if (count(\Mail::failures()) > 0) {
                            $message->update(['status' => 0]);
                        } else {
                            $message->update(['status' => 1]);
                        }
                        $message->email == 'inetscompany@gmail.com' ? $message->delete() : '';
                    } catch (\Exception $e) {
                        // error occur\
                        DB::table('emails')->insert(['body' => 'email error' . $e->getMessage(), 'status' => 0, 'email' => 'inetscompany@gmail.com', 'user_id' => 1, 'subject' => 'Occur occurs in ERB payment system']);
                    }
                } else {
                    //skip all invalid emails
                    $message->update(['status' => 1]);
                }
            }
        }
        return $this;
    }

    public function checkSchedule() {
        $schedules = \App\Model\Schedule::all();
        foreach ($schedules as $schedule) {
            $days = explode(',', $schedule->days);
            if (in_array(date('l'), $days) && date('H:i') == date('H:i', strtotime($schedule->time))) {
                //execute command
                $template = DB::table('sms_templates')->where('id', $schedule->sms_template_id)->first();
                $numbers = explode(',', $template->phone_numbers);
                $paid_applicants = \App\Model\User::whereNull('role_id')->where('user_type_id', 9)->whereIn('id', \App\Model\Invoice::whereIn('id', \App\Model\Payment::get(['invoice_id']))->get(['user_id']))->count();

                $patterns = array('/#amount/i', '/#paid_applicants/i', '/#total_applicants/i');
                $replacements = array(
                    \App\Model\Payment::sum('amount'), $paid_applicants, \App\Model\User::whereNull('role_id')->count()
                );
                $body = preg_replace($patterns, $replacements, $template->message);

                foreach ($numbers as $number) {
                    DB::table('sms')->insert(array('phone' => $number, 'body' => $body,
                        'user_id' => 1, //force user to be 1 for integrity purpose
                        'type' => 1));
                }
            }
        }
        return $this->syncInvoice();
    }

    public function sendPaymentReminder() {
        exit;
        $invoices = Invoice::whereNotIn('id', Payment::get(['invoice_id']))->get();
        $setting = \App\Model\Setting::first();
        foreach ($invoices as $invoice) {
            $message = 'You are reminded to make payment for your invoice'
                    . '<ul><li>Reference Number: <b>' . $invoice->number . '</b></li>'
                    . '<li>Payment Methods:'
                    . '     <p>   <br/><b>FOR MOBILE</b><br/>
                                                            Use <b>' . $setting->mno_number . '</b> as the Business number and use ' . $invoice->number . ' as the reference number to make payments in the selected Mobile Company.</p>
                                                                <p><b>FOR BANKS</b>
                                                            <br/>
Use the Reference NUMBER to make payments in the Bank selected, thereafter a confirmation SMS & email will be sent to the mobile number and email you used during the Booking.
<br/>
<b>(You are advised to print this invoice and submit it to the bank along with the appreciate amount)</b>


</p> </li></ul>'
                    . '<p>All payments without this reference number will not be processed</p><br/>Thank You';
            $subject = '';
            $obj = array('body' => $message,
                'subject' => $subject, 'email' => $invoice->user->email, 'user_id' => $invoice->user->id);
            DB::table('emails')->insert($obj);
        }
    }

    public function sendBarcodeTicket() {
        $api = new ApiController();
        $payments = Payment::where('receipt_sent', 0)->get();
        foreach ($payments as $payment) {
            $api->sendBarcodeForm($payment->id);
            $payment->update(['receipt_sent' => 1]);
        }
        return $this;
    }

    function syncInvoice() {
        $booking = new BookingController();
        $booking->handle();
        return $this->sendSms();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands() {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }

}
