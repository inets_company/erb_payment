<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use DB;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    public $data = [];

    public function __construct() {
        $this->data['setting'] = \App\Model\Setting::first();
        $this->logRequest();
    }

    public function user() {
        return Auth::user();
    }

    public function logRequest() {
        
    }

    public function segment($id) {
        $enc = mb_detect_encoding(trim(request()->segment($id)), "UTF-8,ISO-8859-1");
        return iconv($enc, "UTF-8", trim(request()->segment($id)));
    }

    /**
     * 
     * @param type $fields : Array  values to push , 
      @param $url : Url to fetch or push records
     */
    public function curlServer($fields, $url, $header = null) {
// Open connection
        $ch = curl_init();
// Set the url, number of POST vars, POST data

        curl_setopt($ch, CURLOPT_URL, $url);
        $pheader = $header == null ? array(
            'application/x-www-form-urlencoded'
                ) :$header ;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $pheader);

        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    public function send_email($email, $subject, $message, $attachment = null) {

        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $user = \App\Model\User::where('email', $email)->first();
            $obj = array('body' => is_array($message) ? $message['message'] : $message, 'subject' => $subject, 'email' => $email, 'attachment' => $attachment);
            if (count($user) > 0) {
                $_array = array('user_id' => $user->id);
                DB::table('emails')->insert(array_merge($_array, $obj));
            } else {
                DB::table('emails')->insert($obj);
            }
        }
        return TRUE;
    }

    public function send_sms($phone_number, $message, $message_type = 1) {
        if ((strlen($phone_number) > 6 && strlen($phone_number) < 20) && $message != '') {
            $user = \App\Model\User::where('phone', $phone_number)->first();
            if (count($user) > 0) {
                DB::table('sms')->insert(array('phone' => $phone_number,
                    'body' => $message,
                    'user_id' => $user->id,
                    'type' => 1));
            } else {
                DB::table('sms')->insert(array('phone' => $phone_number, 'body' => $message, 'type' => $message_type));
            }
        }
        return TRUE;
    }

    public function uploadExcel() {
        $this->PHPExcel = new \PHPExcel();
        try {
            // it will be your file name that you are posting with a form or c
            $folder = "storage/";
            is_dir($folder) ? '' : mkdir($folder, 0777, True);


            $file = request()->file('file');

            $name = time() . rand(4343, 3243434) . '.' . $file->guessClientExtension();
            $move = $file->move($folder, $name);

            $path = $folder . $name;

            if (!$move) {
                die('upload Error');
            } else {
                $objPHPExcel = \PHPExcel_IOFactory::load($path);
            }
        } catch (Exception $e) {
            $this->resp->success = FALSE;
            $this->resp->msg = 'Error Uploading file';
            echo json_encode($this->resp);
        }

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        $headings = $sheet->rangeToArray('A1:' . $highestColumn . 1, NULL, TRUE, FALSE);

        $data = array();
        for ($row = 2; $row <= $highestRow; $row++) {
            //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
            $rowData[0] = array_combine($headings[0], $rowData[0]);
            $data = array_merge_recursive($data, $rowData);
        }
        unlink($path);
        return $data;
    }

}
