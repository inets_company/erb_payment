<?php

namespace App\Http\Controllers;

use \App\Model\Invoice;
use \App\Model\User;
use \App\Model\Fee;
use \App\Model\Setting;
use \App\Model\Profession;
use \App\Model\Invoice_fee;
use \App\Http\Controllers\BookingController;
use Illuminate\Http\Request;
use DB;
use Auth;

class InvoiceController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
        $this->data['setting'] = \App\Model\Setting::first();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $from = $this->data['from'] = request('from');
        $to = $this->data['to'] = request('to');
        $from_date = date('Y-m-d H:i:s', strtotime($from . ' -1 day'));
        $to_date = date('Y-m-d H:i:s', strtotime($to . ' +1 day'));
        $this->data['invoices'] = ($from != '' && $to != '') ?
                Invoice::whereBetween('date', [$from_date, $to_date])->where('type', 0)->get() :
                Invoice::where('type', 0)->get();
        return view('invoice.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

        $this->data['amount'] = (new BookingController())->getFeeAmount();
        return view('invoice.create', $this->data);
    }

    public function storeInvoice($request, $user_id, $fee_id, $is_bulk = false) {
        $payment_invoice = json_decode($request);
        if (count($payment_invoice) == 1 && isset($payment_invoice->number)) {
            $invoice_param = [
                'number' => $payment_invoice->number,
                'user_id' => $user_id,
                'title' => request('title'),
                'optional_name' => request('optional_name'),
                'date' => 'now()',
                'bil_id' => request('bilId'),
                'type' => $is_bulk == FALSE ? 0 : 1,
                'year' => date('Y')];
            $invoice = Invoice::create($invoice_param);
            return $is_bulk == false ?
                    Invoice_fee::create(['invoice_id' => $invoice->id, 'fee_id' => $fee_id, 'amount' => $payment_invoice->amount, 'item_name' => request('name'), 'note' => 'Engineers  Registration Board (ERB), Engineers Day Event']) :
                    (object) ['invoice_id' => $invoice->id, 'amount' => $payment_invoice->amount];
        } else {
            return NULL;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //echo  json_encode($request->all()); exit;
        if ($request->file('file')) {
            return $this->createInvoceByExcel($request);
        } else if ((int) request('noexcel') == 1) {
            return $this->createInvoceByExcel($request, TRUE);
        } else {
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|email|unique:users,email',
                'phone' => 'required|numeric|unique:users,phone',
                'employer_id' => 'required',
                'profession_id' => 'required'
            ]);
            return $this->createSingleUserInvoice($request);
        }
    }

    public function createSingleUserInvoice($request) {
        $amount = (new BookingController())->getFeeAmount();
        $phone = validate_phone_number(request('phone'));
        if (count($phone) <> 2) {
            return redirect()->back()->with('error', 'Phone number ' . request('phone') . ' is not valid');
        }
        $valid_phone = $phone[1];

        $user_info = User::orWhere(['phone' => $valid_phone, 'email' => $request->email])->first();
        if (count($user_info) == 0) {
            $user_type = \App\Model\User_type::where('name', 'non-sponsored')->first();
            $user = User::create(array_merge($request->except('_token', 'phone'), ['password' => 123456789, 'phone' => $valid_phone, 'created_by' => Auth::user()->id, 'user_type_id' => count($user_type) == 1 ? $user_type->id : NULL]));
        } else {
            return redirect()->back()->with('error', 'User with those information already exists with Invoice Number ');
        }
        $setting = Setting::first();
        $booking = array_merge($request->all(), ['user_id' => $user->id,
            'amount' => $amount,
            'username' => $setting->username,
            'password' => $setting->password
        ]);
        $booking_control = new BookingController();
        $fee = \App\Model\Fee::find(1);
        $booking_control->createBooking($booking, $fee->id, false);
        return redirect(url('invoice'));
    }

    private function checkKeysExists($value) {
        $required = array('phone', 'name', 'email', 'profession');
        $data = array_shift($value);
        if (count(array_intersect_key(array_flip($required), $data)) === count($required)) {
            //All required keys exist! 
            $status = 1;
        } else {
            $missing = array_intersect_key(array_flip($required), $data);
            $data_miss = array_diff(array_flip($required), $missing);
            $status = ' <div class="alert alert-danger">Column with title <b>' . implode(', ', array_keys($data_miss)) . '</b>  miss from Excel file. Please make sure file is in the same format as a sample file</div>';
        }
        return $status;
    }

    public function createInvoceByExcel($request, $noexcel = false) {
        ini_set('max_execution_time', 300);
        $amount = (new BookingController())->getFeeAmount();

        $employer = \App\Model\Employer::find($request->user_id);
        $user_base_records = ['name' => $employer->name, 'email' => strtolower($request->user_id) . '@erb.go.tz'];
        $user_info = User::orWhere($user_base_records)->first();
        $user = (count($user_info) < 1) ?
                User::create(array_merge($user_base_records, ['password' => bcrypt('123456789'), 'phone' => time(), 'is_employer' => 1, 'employer_id' => $request->user_id])) : $user_info;
        $fee = \App\Model\Fee::find(1);

        if (count($user->invoice()->first()) == 0) {
            $setting = Setting::first();
            $booking = array_merge($user_base_records, ['user_id' => $user->id,
                'amount' => $amount, 'phone' => time(),
                'username' => $setting->username,
                'password' => $setting->password
            ]);
            $booking_control = new BookingController();

            $return = $booking_control->createBooking($booking, $fee->id, 1);
        } else {
            $return = (object) ['invoice_id' => $user->invoice()->first()->id, 'amount' => $amount];
        }
        return $this->storeUserRecords($return, $fee->id, $employer, $noexcel);
    }

    public function storeUserRecords($return, $fee_id, $employer, $noexcel = false) {
        $data = $noexcel == false ? $this->uploadExcel() : (array) request('users');
        $status = $noexcel == false ? $this->checkKeysExists($data) : 1;
        if ((int) $status == 1) {
            $status = '';

            foreach ($data as $value) {
                $valid_phone = validate_phone_number($value['phone']);
                if (!filter_var($value['email'], FILTER_VALIDATE_EMAIL)) {

                    $status .= ' <div class="alert alert-danger">This email <b>' . $value['email'] . '</b>  is not valid. Record skipped </div>';
                } else if (count($valid_phone) <> 2) {
                    $status .= ' <div class="alert alert-danger">This phone number <b>' . $value['phone'] . '</b>  is not valid. Record skipped </div>';
                } else {
                    $prof = $value['profession'];
                    $profession = Profession::where(DB::raw('lower(name)'), strtolower($prof))->first();
                    if (count($profession) == 1) {
                        $profession_id = $profession->id;
                    } else {
                        $p = Profession::create(['name' => $prof]);
                        $profession_id = $p->id;
                    }
                    $user_data = [
                        'phone' => $valid_phone[1],
                        'email' => $value['email'],
                    ];

                    $user_info = User::orWhere($user_data)->first();

                    $user = (count($user_info) == 0) ?
                            User::create(array_merge($user_data, ['password' => bcrypt('user12345'),
                                'name' => $value['name'], 'profession_id' => $profession_id, 'employer_id' => $employer->id, 'is_employer' => 0, 'user_type_id' => 7])) : $user_info;

                    $note = 'Engineers  Registration Board (ERB), Engineers Day Event';
//check if this user has invoice already either created by himselft or through upload from other companies
                    $user_invoice = $user->invoiceFee()->where('fee_id', $fee_id)->first();
                    if (count($user_invoice) == 0) {
                        $invoice_fee_data = [
                            'invoice_id' => $return->invoice_id,
                            'user_id' => $user->id,
                            'fee_id' => $fee_id
                        ];
                        $invoice_fee = Invoice_fee::where($invoice_fee_data)->first();

                        if (count($invoice_fee) == 0) {
                            Invoice_fee::create(array_merge(['note' => $note, 'amount' => $return->amount, 'item_name' => $user->name], $invoice_fee_data));
                            $status .= ' <div class="alert alert-success">User <b>' . $user->name . '</b>  uploaded successfully from Excel file.</div>';
                            $this->sendNotificationToUser($user, 1);
                        } else {
                            $status .= ' <div class="alert alert-warning">Invoice fee for user <b>' . $user->name . '</b>  already created</div>';
                        }
                    } else {
                        $status .= ' <div class="alert alert-warning">User <b>' . $user->name . '</b>  has an invoice number ' . $user_invoice->invoice->number . '  for ' . $user_invoice->invoice->user->name . ' already generated on ' . $user_invoice->created_at . '.</div>';
                    }
                }
            }
        }
        $this->data['status'] = $status;
        return $noexcel == false ? view('upload_status', $this->data) : 'success';
    }

    public function sendNotificationToUser($user, $sponsored = null) {
        $patterns = array(
            '/#name/i', '/#invoice/i', '/#email/i', '/#phone/i'
        );
        $replacements = array(
            $user->name, '', $user->email, $user->phone
        );
        $template = \App\Model\Sms_template::where('name', $sponsored == 1 ? 'sponsored' : 'welcome')->first();
        $sms = preg_replace($patterns, $replacements, $template->message);

        DB::table("emails")->insert(array('body' => $sms, 'subject' => 'Payment Accepted', 'email' => $user->email, 'user_id' => $user->id));
        DB::table('sms')->insert(array('phone' => $user->phone, 'body' => $sms, 'user_id' => $user->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        if ((int) $id > 0) {
            $this->data['invoice'] = Invoice::find($id);
        } else {
            //bulk invoices
            return $this->bulkInvoice();
        }
        $this->data['fee'] = Fee::first();
        return view('invoice.single', $this->data);
    }

    public function bulkInvoice() {
        $from = $this->data['from'] = request('from');
        $to = $this->data['to'] = request('to');
        $from_date = date('Y-m-d H:i:s', strtotime($from . ' -1 day'));
        $to_date = date('Y-m-d H:i:s', strtotime($to . ' +1 day'));
        $this->data['invoices'] = ($from != '' && $to != '') ?
                Invoice::whereBetween('date', [$from_date, $to_date])->where('type', 1)->get() :
                Invoice::where('type', 1)->get();
        $this->data['fee'] = Fee::first();
        return view('invoice.bulk', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $this->data['invoice'] = Invoice::find($id);
        return view('invoice.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    public function addUserFee($id) {
        $phone = validate_phone_number(request('phone'));
        if (count($phone) <> 2) {
            return redirect()->back()->with('error', 'Phone number ' . request('phone') . ' is not valid');
        }
        $valid_phone = $phone[1];

        $user_info = User::orWhere(['phone' => $valid_phone, 'email' => request('email')])->first();

        $obj = array_merge(request()->except('_token', 'phone'), ['password' => bcrypt('user12345'), 'phone' => $valid_phone, 'created_by' => Auth::user()->id]);
        $user = (count($user_info) == 0) ?
                User::create($obj) : $user_info;
        $note = 'Engineers  Registration Board (ERB), Engineers Day Event';
        $amount = (new BookingController())->getFeeAmount();
        $invoice_fee_data = [
            'invoice_id' => $id,
            'user_id' => $user->id,
        ];
        $invoice_fee_info = Invoice_fee::where($invoice_fee_data)->first();

        if (count($invoice_fee_info) == 0) {
            Invoice_fee::create(array_merge(['note' => $note, 'amount' => $amount, 'item_name' => $user->name], $invoice_fee_data));
            $this->sendNotificationToUser($user, 1);
            return redirect()->back()->with('success', 'Records added successful');
        } else {
            return redirect()->back()->with('warning', 'Records Exists');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $invoice = Invoice::find($id);
        User::find($invoice->user_id)->delete();
        return redirect()->back()->with('success', 'Invoice Deleted');
    }

    public function feeDelete($id) {
        Invoice_fee::find($id)->delete();
        return redirect()->back()->with('success', 'Invoice Deleted');
    }

    public function createBilling() {
        //select all users with no invoice join all users with invoices but not paid
        //loop through to create billing
        //
        $setting = \App\Model\Setting::first();
        $amount = (new BookingController())->getFeeAmount();
        $users = DB::select("select * from erb_new_payment.users where id not in (select user_id from erb_new_payment.invoices) and is_employer=0 and name not in ('BEATRICE MUNISHI','YARED PETER NGALABA','OMARY CHITAWALA','ENG. ANOLD A. KILEO','ISAKWISA AMBOKILE','ASHERY MWAIRWA KASEE') ");
        foreach ($users as $user) {

            $booking = ['user_id' => $user->id,
                'amount' => $amount,
                'username' => $setting->username,
                'password' => $setting->password,
                'name' => $user->name
            ];
            $booking_control = new \App\Model\BookingController();
            $fee = \App\Model\Fee::find(1);
            return $booking_control->createBooking($booking, $fee->id, false);
        }
    }

    public function updateBilling() {
        //select all users with no invoice join all users with invoices but not paid
        //loop through to create billing
        //
        $setting = \App\Model\Setting::first();
        $amount = (new BookingController())->getFeeAmount();
        DB::statement('delete from erb_new_payment.invoices where id not in (select invoice_id from erb_new_payment.payments)');
        $users = DB::select("select * from erb_new_payment.users where id in (select user_id from erb_new_payment.invoices where id not in (select invoice_id from erb_new_payment.payments)) and is_employer=0");
        foreach ($users as $user) {

            $booking = ['user_id' => $user->id,
                'amount' => $amount,
                'username' => $setting->username,
                'password' => $setting->password,
                'name' => $user->name
            ];
            $booking_control = new \App\Model\BookingController();
            $fee = \App\Model\Fee::find(1);
            return $booking_control->createBooking($booking, $fee->id, false);
        }
    }

}
