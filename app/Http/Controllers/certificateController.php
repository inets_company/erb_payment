<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Model\User;
use \App\Model\Event;
use \App\Model\Fee;
use \App\Model\User_type;
use PDF;

class certificateController extends Controller {

    public function __construct() {
        if (request('auth') == NULL) {
            // $this->middleware('auth');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $type = request('user_type');
        $user_types = $type == null || $type == 0 ? User_type::get(['id']) : [$type];
        if ($type == null || $type == 0) {
            $this->data['applicants'] = User::whereNull('role_id')->get();
        } else {
            $this->data['applicants'] = $type == 120 ? User::where('is_employer', 1)->get() : User::whereNull('role_id')->whereIn('user_type_id', $user_types)->get();
        }
        $this->data['event'] = \App\Model\Event::all();
        return view('certificate.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    function getName($id) {
        $user = User::find($id);
        return PDF::loadHTML('<h1 style="font-size:120px">' . $user->name . '</h1>')
                        ->setPaper('a4', 'landscape')
                        ->setOptions(['dpi' => 250, 'defaultFont' => 'sans-serif'])
                        ->setWarnings(false)
                        ->save('storage/app/myfile' . $id . '.pdf');
    }

    function createCertificate($id) {
        $pdf = new \setasign\Fpdi\Fpdi('L');
        // add a page
        $pdf->AddPage();
        // set the source file to doc1.pdf and import a page
        $pdf->setSourceFile("storage/app/certificate.pdf");
        $tplIdx = $pdf->importPage(1);
        // use the imported page and place it at point 5,1 with a width of 283 mm, 200mm height
        $pdf->useTemplate($tplIdx, 5, 1, 283, 200);
        // set the source file to doc2.pdf and import a page
        $this->getName($id);
        $pdf->setSourceFile("storage/app/myfile" . $id . ".pdf");
        $tplIdx = $pdf->importPage(1);
        // use the imported page and place it at point 95,98 with a width of 210 mm
        $pdf->useTemplate($tplIdx, 90, 94, 210);

        $pdf->Output();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {

        if ((int) $id > 0) {
            $this->data['token'] = request('auth');
            if (strlen(request('auth')) > 2) {
                $auth_token = decrypt($this->data['token']);
                if ($auth_token != $id) {
                    die('Request is not valid. Please click the link as supplied in your email address');
                }
            }
            return $this->createCertificate($id);
            //return view('certificate.show', $this->data);
        } else if ($id == 'printall') {
            $user_id = request('source') == 'client' ? \Crypt::decrypt(request('ids')) : request('ids');
            $this->data['users'] = $user_id == null ? User::all() :
                    User::whereIn('id', explode(',', trim($user_id, ',')))->get();
            $this->data['event'] = Event::first();
            return view('certificate.printall', $this->data);
        } else if ($id == 'send') {
            return $this->sendCertificates();
        }
    }

    /**
     * 
     * @param type $payment_id
     * @access : Via kernel background operation
     */
    function sendCertificates() {
        $users = User::whereNull('role_id')->where('is_employer', '<>', 1)->get();
        foreach ($users as $user) {
            $att = $user->attendance()->first();
            if (count($att) == 1 ) {
                
                $id = $user->id;
                $content = 'Please Click the link below to download/print your certificate'
                        . '<br/>'
                        . '<br/>'
                        . '<a href="https://engineersday.co.tz/certificate/' . $id . '?auth=' . encrypt($id) . '" style="display: inline-block; margin-bottom: 0; font-weight: 40px; text-align: center;
    vertical-align: middle; cursor: pointer; background-image: none; border: 1px solid transparent; white-space: nowrap; padding: 12px 24px; font-size: 14px; line-height: 1.428571429; border-radius: 4px;color: #fff; background-color: #5cb85c; border-color: #4cae4c;">Event Certificate</a>';
                $this->send_email($user->email, 'AED 2018- Certificate of Attendance', $content);
            }
        }
        return redirect()->back()->with('success', 'Success');
    }

    public function search() {
        $user_base_records = ['email' => trim(strtolower(request('tag'))), 'phone' => validate_phone_number(request('tag'))[1]];
        $user_info = User::orWhere($user_base_records)->first();

        if (count($user_info) == 0) {
            echo json_encode(['message' => 'Sorry: Information does not exists', 'alert_status' => 'alert-danger']);
        } else {
            $no_such_invoice = json_encode([
                'message' => 'Sorry: No certificate has been created under such email or phone',
                'alert_status' => 'alert-danger']);

            if ($user_info->role_id == NULL) {
                if ((int) $user_info->is_employer == 1) {

                    echo json_encode(['message' => 'Success: This information is registered as employer. No certificate will be issued to employer', 'alert_status' => 'alert-success']);
                } else {

                    echo json_encode(['message' => '<b>Success</b>: Click here to get your Certificate  <a href="' . url('certificate/' . $user_info->id . '?source=client&ids=' . \Crypt::encrypt($user_info->id)) . '&auth=' . \Crypt::encrypt($user_info->id) . '" id="link" class="badge badge-success" target="_blank">Click here to view your certificate</a> ', 'alert_status' => 'alert-success']);
                }
            } else {
                echo $no_such_invoice;
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
//
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
//
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
//
    }

}
