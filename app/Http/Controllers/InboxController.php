<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Model\Sms;
use \App\Model\Email;
use \App\Model\Sms_template;
use \App\Model\Schedule;
use \App\Model\User;
use \App\Http\Controllers\ApiController;

class InboxController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $this->data['sms'] = Sms::all(90);
        return view('inbox.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
        dd($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        if ($id == 'template') {
            $this->data['templates'] = Sms_template::all();
            return view('inbox.template', $this->data);
        } else if ($id == 'getTemplate') {
            $template_id = request('template');
            $template = Sms_template::find($template_id);
            return count($template) == 1 ? $template->message : '';
        } else if ($id == 'schedule') {
            $this->data['schedules'] = Schedule::all();
            return view('inbox.schedule', $this->data);
        } else if ($id == 'resend') {
            request('type') == 'email' ? Email::find(request('id'))->update(['status' => 0]) :
                            Sms::find(request('id'))->update(['status' => 0]);
            return 1;
        } else if ($id == 'email') {
            $this->data['emails'] = Email::orderBy('created_at','desc')->get();
            return view('inbox.email', $this->data);
        } else if ($id == 'attachment') {
            $this->data['attachments'] = [];
            return view('inbox.attachment', $this->data);
        } else if ($id == 'resend_barcode') {
            //$api = new ApiController();
            $payment = \App\Model\Payment::find(request('id'));
            //$api->sendBarcodeForm($payment->id);
            $payment->update(['receipt_sent' => 1]);
            return 1;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    public function sendSms() {
        $message = request('message');
        $to = request('to');
        if ($to == 'write') {
            $phones = explode(',', request('phone'));
            foreach ($phones as $phone) {
                $this->send_sms($phone, $message);
            }
        } else {
            $users = $to == 0 ? User::all() :
                    User::where('user_type_id', $to)->get();
            foreach ($users as $user) {
                $this->send_sms($user->phone, $message);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        request('t') == 'email' ? Email::find($id)->delete() : Sms::find($id)->delete();
        return redirect()->back()->with('success', 'Deleted');
    }

}
