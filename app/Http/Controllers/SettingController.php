<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Model\User_type;
use \App\Model\Role;
use \App\Model\Fee;
use \App\Model\Event;
use \App\Model\User;
use \App\Model\Setting;
use \App\Model\Profession;
use \App\Model\Sms_template;
use CodeItNow\BarcodeBundle\Utils\BarcodeGenerator;
use PDF;
use DB;

class SettingController extends Controller {

    public function __construct() {
         if (request('auth') == NULL) {
            $this->middleware('auth');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $this->data['setting'] = Setting::first();
        return view('setting.index', $this->data);
    }

    public function createBarCode($user_id) {
        $user = User::find($user_id);
        $barcode = new BarcodeGenerator();
        $barcode->setText($user->number);
        $barcode->setType(BarcodeGenerator::Code128);
        $barcode->setScale(2);
        $barcode->setThickness(25);
        $barcode->setFontSize(10);
        return $barcode->generate();
    }

    public function createTicketParam($user_id) {
        $this->data['barcode'] = $this->createBarCode($user_id);
        $this->data['event'] = Event::first();
        $this->data['user'] = User::find($user_id);
        $invoice = \App\Model\Invoice::where('user_id', $user_id)->first();
        if (count($invoice) == 1) {
            $this->data['payment'] = $invoice->payment()->first();
        } else {
            $this->data['payment'] = [];
        }
        $this->data['padding_ticket'] = 1;
        $this->data['id'] = $user_id;
        PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
        return view('email.ticket_attachment', $this->data);
    }

    public function downloadTicket($user_id) {

        $this->data['barcode'] = $this->createBarCode($user_id);
        $this->data['event'] = Event::first();
        $this->data['user'] = User::find($user_id);
        $invoice = \App\Model\Invoice::where('user_id', $user_id)->first();
        if (count($invoice) == 1) {
            $this->data['payment'] = $invoice->payment()->first();
        } else {
            $this->data['payment'] = [];
        }
        PDF::setOptions(['defaultFont' => 'sans-serif']);
        $pdf = PDF::loadView('email.ticket_attachment', $this->data);
        return $pdf->download('receipt.pdf');
    }

    public function getUserTicket($user_id = null) {
        $this->data['barcode'] = $this->createBarCode($user_id);
        $this->data['event'] = Event::first();
        $this->data['user'] = User::find($user_id);
        $invoice = \App\Model\Invoice::where('user_id', $user_id)->first();
        if (count($invoice) == 1) {
            $this->data['payment'] = $invoice->payment()->first();
        } else {
            $this->data['payment'] = [];
        }
        $this->data['padding_ticket'] = 1;
        $this->data['id'] = $user_id;
        return view('email.ticket_attachment', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }


     public function addPermission() {
        $permission_id = request('id');
        $role_id = request('role_id');
        $data = array(
            'role_id' => $role_id,
            'permission_id' => $permission_id
        );
        $insert_id = DB::table('role_permissions')->insertGetId($data, 'id');
        if ($insert_id > 0) {
            echo 'Success: Added';
        } else {
            echo 'Error: Please Refresh';
        }
    }

    public function removePermission() {
        $permission_id = request('id');
        $role_id = request('role_id');
        $data = array(
            'role_id' => $role_id,
            'permission_id' => $permission_id
        );
        DB::table('role_permissions')->where($data)->delete();
        echo 'Success : Removed';
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        if (isset($request->tag)) {
            return $this->{$request->tag}();
        } else {
            $this->validate($request, [
                "name" => "required",
                "phone" => "required",
                "email" => "required|email",
                "box" => "required",
                "address" => "required"
            ]);
            $setting = Setting::first();
            count($setting) == 0 ? Setting::create($request->all()) : $setting->update($request->all());
            return redirect()->back()->with('success', '<strong>Success!</strong> Information added successfully.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        if ($id == 'role') {
            $this->data['roles'] = Role::all();
            return view('setting.role', $this->data);
        } else if ($id == 'fee') {
            $this->data['fees'] = Fee::all();
            return view('setting.fee', $this->data);
        } else if ($id == 'event') {
            $this->data['events'] = Event::all();
            return view('setting.event', $this->data);
        } else if ($id == 'profession') {
            $this->data['professions'] = Profession::all();
            return view('setting.profession', $this->data);
        } else if ($id == 'permission') {
            $this->data['roles'] = Role::all();
            $this->data['role_id'] = request('id');
            $this->data['groups'] = \App\Model\Permission_group::all();
            return view('setting.permission', $this->data);
        } else {
            $this->data['users'] = User_type::all();
            return view('setting.usertype', $this->data);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        if (request('user') == 'fee') {
            Fee::find($id)->update(request()->all());
        } else if (request('user') == 'event') {
            Event::find($id)->update(request()->all());
        } else if (request('user') == 'role') {
            Role::find($id)->update(request()->all());
        } else if (request('user') == 'user_type') {
            User_type::find($id)->update(request()->all());
        } else if (request('user') == 'user') {
            User::find($id)->update(request()->all());
        } else if (request('user') == 'profession') {
            Profession::find($id)->update(request()->all());
        } else if (request('user') == 'sms_template') {
            Sms_template::find($id)->update(request()->all());
        }
        return redirect()->back()->with('success', 'Information updated successfully.');
    }

    public function getEdits() {
        if (request('table') == 'event') {
            $edits = Event::find(request('id'))->toJson();
        } else if (request('table') == 'fee') {
            $edits = Fee::find(request('id'))->toJson();
        } else if (request('table') == 'role') {
            $edits = Role::find(request('id'))->toJson();
        } else if (request('table') == 'user_type') {
            $edits = User_type::find(request('id'))->toJson();
        } else if (request('table') == 'user') {
            $edits = User::find(request('id'))->toJson();
        } else if (request('table') == 'profession') {
            $edits = Profession::find(request('id'))->toJson();
        } else if (request('table') == 'sms_template') {
            $edits = Sms_template::find(request('id'))->toJson();
        }
        return $edits;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
