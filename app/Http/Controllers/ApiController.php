<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Model\Invoice;
use \App\Model\Client;
use \App\Model\Payment;
use \App\Model\Invoice_fee;
use \App\Model\Invoice_fees_payment;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\PaymentController;
use DB;
use PDF;

class ApiController extends Controller {

    /**
     *
     * @var Integer
     */
    public $payment_id;

    /**
     *
     * @var Mixed
     * @uses show the invoice number to accept payment,
     *       the invoice is used in communicaiton with bank to
     *       identify who pays and is unique
     */
    private $invoice;

    /**
     *
     * @var Mixed parameters received in api
     */
    private $api_param;

    /**
     *
     * @var Mixed
     */
    private $api_info;

    /**
     * @access public : Load payment modal and initialize transaction ID
     *
     */
    public $usertype;
    public $live_username = '109M17SA01DINET';
    public $live_password = 'LuHa6bAjKV5g5vyaRaRZJy*x5@%!yBBBTVy';
    public $live_url = 'https://api.mpayafrica.co.tz/v2/auth';

    /**
     * 
     * @access public : Configuration of EGA PARAMETERS
     */
    public $ega_submit_url = 'http://196.192.79.19:80/api/event/generate-bill'; //'http://154.118.230.227:8093/api/event/generate-bill';
    public $ega_resubmit_url = '';
    public $ega_secret = 'ERB-MIS';

    /**
     *
     * @response codes
     */
    // Successful response
    const SUCCESS = 200; //
    //Invalid Token
    const INVALID_TOKEN = 201;
    //Invalid checksum
    const INVALID_CHECKSUM = 202;
    //Payment reference number already paid – During verify method (applies for FIXED amount type)
    const INVOICE_PAID = 203;
    //Invalid payment reference number
    const INVALID_INVOICE = 204;
    //Payment reference number has expired
    const INVOICE_EXPIRED = 205;
    //Duplicate entry : Return the receipt number of the transaction.
    const DUPLICATE_PAYMENT = 206;
    //Transaction reference number already paid – During post method
    const TRANSACTION_EXISTS = 207;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getUsers() {
        
    }

    public function getReceipt() {
        
    }

    public function getInvoices() {
        
    }

    private function checkKeysExists($value) {
        $required = array('name', 'email', 'phone', 'title', 'optional_name');
        $data = $value;
        if (count(array_intersect_key(array_flip($required), $data)) === count($required)) {
            //All required keys exist!
            return TRUE;
        } else {
            $missing = array_intersect_key(array_flip($required), $data);
            $data_miss = array_diff(array_flip($required), $missing);
            die(json_encode(['message' => 'Missing Fields : ' . implode(', ', array_keys($data_miss)), 'success' => 0, 'code' => 601]));
        }
    }

    public function validateUser($username, $password) {
        $client = Client::where('username', $username)->where('password', $password)->first();
        count($client) == 1 ? TRUE : die(json_encode(['message' => 'Wrong username or password', 'success' => 0, 'code' => 604]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request) {
        try {
            $this->validateUser($request->username, $request->password);
            $user_records = ['name' => $request->name, 'email' => $request->email, 'phone' => $request->phone, 'password' => 123456789];
            $sub_records = ['title' => $request->title, 'optional_name' => $request->optional_name];
            $this->checkKeysExists(array_merge($user_records, $sub_records));
            $record = ( new \App\Http\Controllers\UserController())->saveUser($user_records, $sub_records);
            $invoice_number = $this->createInvoiceNo();
            $invoice_param = ['number' => $invoice_number,
                'user_id' => $record->id,
                'year' => date('Y')];
            $invoice = Invoice::create($invoice_param);
            Invoice_fee::create(['invoice_id' => $invoice->id, 'amount' => request('amount'), 'item_name' => request('name'), 'note' => request('description')]);
            $this->handle($invoice->id); //push invoice in a que
            return json_encode(array_merge($invoice->toArray(), ['amount' => request('amount')]));
        } catch (Exception $exc) {
            return json_encode(['status' => 0, 'message' => 'Invoice Failed to be created', 'error' => $exc->getTraceAsString()]);
        }
    }

    public function handle($invoice_id) {
        $invoices = DB::select('select * from paygate.invoice_view where invoice_id=' . $invoice_id);
        if (count($invoices) > 0) {
            foreach ($invoices as $invoice) {
                $token = $this->getToken($invoice);
                if (strlen($token) > 4) {
                    $fields = array(
                        "reference" => $invoice->number,
                        "student_name" => $invoice->name,
                        "student_id" => $invoice->user_id,
                        "amount" => $invoice->amount,
                        "type" => 'ERB',
                        "code" => "10",
                        "callback_url" => "http://158.69.112.216:8010/api/init",
                        "token" => $token
                    );
                    $push_status = $invoice->status == 2 ? 'invoice_update' : 'invoice_submission';
                    //live invoice

                    $url = 'https://api.mpayafrica.co.tz/v2/' . $push_status;

                    $curl = $this->curlServer($fields, $url);
                    $result = json_decode($curl);
                    if (($result->status == 1 && strtolower($result->description) == 'success') || $result->description == 'Duplicate Invoice Number') {
//update invoice no
                        DB::table('paygate.invoices')
                                ->where('number', $invoice->number)->update(['sync' => 1, 'return_message' => $curl, 'push_status' => $push_status]);
                    } else {
                        DB::table('api.requests')->insert(['content' => $curl . ', request=' . json_encode($fields)]);
                    }
                }
            }
        }
    }

    /**
     *
     * @param type $schema
     * @return type
     *             $user = '107M17S666D381';
      $pass = 'rWh$abB!P5&$MWvj$!DTe29F#vAu2tmct!2';
     *
      Username: 109M17SA01DINET
      Password : LuHa6bAjKV5g5vyaRaRZJy*x5@%!yBBBTVy  , mother of mercy
     */
    public function getToken($invoice = null) {
        //later on this invoice will have a client id, thus we will select specific client but for now, lets use ERB
        //if ($invoice->schema_name == 'beta_testing') {
        //testing invoice
//            $setting = DB::table($invoice->optional_name . '.setting')->first();
//            $url = 'https://wip.mpayafrica.com/v2/auth';
        // } else {
        //live invoice
        $setting = DB::table('clients')->first();
        $url = 'https://api.mpayafrica.co.tz/v2/auth';
        // }
        $user = $setting->api_username;
        $pass = $setting->api_password;
        $request = $this->curlServer([
            'username' => $user,
            'password' => $pass
                ], $url);
        $obj = json_decode($request);
        if (isset($obj) && is_object($obj) && isset($obj->status) && $obj->status == 1) {
            return $obj->token;
        }
    }

    public function createInvoiceNo() {
        $client = Client::first();
        $invoiceNo = $client->institution_code . rand(11, 9997);
        //  $invoiceNo = $client->institution_code . rand(1, 999) . substr(str_shuffle('ABCDEFGHJKLMNPQRSTUVWXYZ'), 0, 3) . rand(1, 999);
        $data = Invoice::where('number', $invoiceNo)->first();
        if (!empty($data)) {
            return $this->createInvoiceNo();
        } else {
            return $invoiceNo;
        }
    }

    /**
     *
     * @param type $schema
     * @return type
     *             $user = '107M17S666D381';
      $pass = 'rWh$abB!P5&$MWvj$!DTe29F#vAu2tmct!2';
     *
      Username: 109M17SA01DINET
      Password : LuHa6bAjKV5g5vyaRaRZJy*x5@%!yBBBTVy  , mother of mercy
     */
    private function logRequests() {
        $ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
        $data = ['content' => json_encode(request()->all()),
            'remote_ip' => $this->get_remote_ip(),
            'remote_hostname' => gethostbyaddr($ip)
        ];
        return DB::table('requests')->insert($data);
    }

    public function request_value($key) {

        if (strlen(request('paymentReference')) > 3 || strlen(request('reference')) > 3 || strlen(request('checksum')) > 3) {
            return request($key);
        } else {
            $param = array_keys(request()->all());
            $ar_param = is_array($param) && count($param) > 0 ? $param[0] : [];
            $request = count($ar_param) > 0 ? (array) json_decode($ar_param) : [];
            return isset($request[$key]) ? $request[$key] : NULL;
        }
    }

    /**
     *
     * @return int : Request position number
     */
    private function initApiRequest() {

        $api_key = $this->request_value("api_key");
        $api_secret = $this->request_value("api_secret");
//check keys if they exist and get user information

        if (strlen($this->request_value('checksum')) > 4) {
            $this->api_info = \App\Model\Api::where('secret', $this->request_value('token'))->first();
            return $this->validateCrdbRequests();
        } else if (strlen(request('reference')) > 2) {
//            $api = DB::table('requests')->where(DB::raw('lower(content)'), 'like', request('token'))->first();
//            if (count($api) == 1) {
            $this->api_info = \App\Model\Api::first();
//            } else {
//                $this->api_info = \App\Model\Api::where('key', $api_key)->where('secret', $api_secret)->first();
//            }

            return $this->validateNormalRequests();
        } else {
            $data = array(
                'status' => 201,
                'statusDesc' => 'Invalid request',
                'data' => json_encode($_REQUEST)
            );
            die(json_encode($data));
        }
    }

    public function validateCrdbRequests() {
        if (count($this->api_info) == 0) {

            $data = array(
                'status' => 201,
                'statusDesc' => 'Invalid token',
                'data' => ['reference' => $this->request_value('paymentReference')]
            );
            die(json_encode($data));
        } else {
            if ((int) request('billId') > 0) {
                //update bilID with valid invoice number
                DB::table('invoices')->where('bil_id', (int) request('billId'))->update(['number' => request('controlNumber')]);
                $data['request'] = 1;
                $data['status'] = 203;
                $data['statusDesc'] = 'success';
                $data['param'] = array(
                    'invoice' => request()->all()
                );
                return json_encode($data);
            }
            return $this->request_value('transactionRef') == null ? 1 : 2;
        }
    }

    public function validateNormalRequests() {
        if (count($this->api_info) == 0) {

            $data = array(
                'status' => 0,
                'description' => 'Invalid api key or secret',
                'reference' => $this->request_value('reference')
            );
            die(json_encode($data));
        } else {

            return $this->request_value('receipt') == null ? 1 : 2;
        }
    }

    public function ussdInit() {
        $soapRequest = '<?xml version="1.0" encoding="UTF-8"?>
<message>
<isomsg>
<field id="1" value="0200" />
<field id="2" value="1235485695231568" />
<field id="3" value="310000" />
<field id="11" value="456987" />
<field id="12" value="125000" />
<field id="13" value="0402" />
<field id="32" value="55555" />
<field id="37" value="785632159235" />
<field id="49" value="TZS" />
<field id="59" value="uat1,6624317309611186081" />
<field id="102" value="0152056574700" />
</isomsg>
</message>';
        $soapResponse = $sclient->channelRequest(array('request' => $soapRequest));
    }

    function getFeeNames($invoice_id) {
        $fees = Invoice_fee::where('invoice_id', $invoice_id)->get();
        $names = array();
        if (count($fees) > 0) {
            foreach ($fees as $fee) {
                array_push($names, $fee->item_name);
            }
        }
        $uq_names = array_unique($names);
        return implode(',', $uq_names);
    }

    private function get_remote_ip() {
        if (getenv('HTTP_CLIENT_IP')) {
            $ip = getenv('HTTP_CLIENT_IP');
        } elseif (getenv('HTTP_X_FORWARDED_FOR')) {
            $ip = getenv('HTTP_X_FORWARDED_FOR');
        } elseif (getenv('HTTP_X_FORWARDED')) {
            $ip = getenv('HTTP_X_FORWARDED');
        } elseif (getenv('HTTP_FORWARDED_FOR')) {
            $ip = getenv('HTTP_FORWARDED_FOR');
        } elseif (getenv('HTTP_FORWARDED')) {
            $ip = getenv('HTTP_FORWARDED');
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    /**
     * @return JSON OBJECT
     *
     */
    private function firstRequest() {
        $reference = $this->request_value('reference') == null ? $this->request_value('paymentReference') : $this->request_value('reference');
        $invoice = DB::table('invoice_view')->where(DB::raw('lower(number)'), strtolower($reference))->first();
        if (count($invoice) > 0) {
            //this invoice exist, so lets return it to a client with amount to process
            $data['request'] = 1;
            if ($invoice->status == 1) {
                // this invoice has already being paid
                if (strlen($this->request_value('checksum')) > 4) {
                    $data = array(
                        'status' => 203,
                        'reference' => $reference,
                        'statusDesc' => 'Already paid'
                    );
                } else {
                    $data = array(
                        'status' => 3,
                        'reference' => $reference,
                        'description' => 'Already paid'
                    );
                }
            } else {
                // this invoice is not yet being paid
                if (strlen($this->request_value('checksum')) > 4) {
                    $this->validateChecksum($reference);
                    $data = array(
                        "status" => 200,
                        "statusDesc" => "success",
                        "data" => array(
                            "payerName" => $invoice->name,
                            "amount" => $invoice->amount,
                            "amountType" => $invoice->amount_type,
                            "currency" => $invoice->currency,
                            "paymentReference" => $reference,
                            "paymentType" => $invoice->payment_type,
                            "paymentDesc" => "ERB AID2018",
                            "payerID" => $invoice->user_id
                        )
                    );
                } else {
                    $data = array(
                        'status' => 1,
                        'reference' => $reference,
                        'amount' => $invoice->amount,
                        'currency' => 'TZS',
                        'account' => '41515111',
                        'student_name' => $invoice->name,
                        'student_id' => $invoice->user_id,
                        'type' => 'ERB',
                        'code' => "10",
                        'callback_url' => "http://158.69.112.216:8011/api/init"
                    );
                }
            }
        } else {
            if (strlen($this->request_value('checksum')) > 4) {
                $data = array(
                    'status' => 204,
                    'reference' => $reference,
                    'statusDesc' => 'Unknown invoice number'
                );
            } else {
                $data = array(
                    'status' => 0,
                    'reference' => $reference,
                    'description' => 'Unknown invoice number'
                );
            }
        }
        echo json_encode($data);
    }

    public function validateChecksum($reference) {
        $checksum = $this->request_value('checksum');
        $valid = sha1($this->request_value('token') . '' . md5($reference));
        if ($checksum != $valid) {
            $data = array(
                'status' => 202,
                'reference' => $this->request_value('paymentReference'),
                "statusDesc" => "Invalid checksum",
            );
            die(json_encode($data));
        }
    }

    function checkEmptyKeys() {
        if (strlen($this->request_value('checksum')) > 4) {
            $mandatory_fields = array(
                'amount', 'transactionChannel', 'paymentReference', 'transactionRef'
            );
            $this->api_param = request()->all();
            foreach ($mandatory_fields as $key) {
                if (!isset($this->api_param[$key])) {
                    die($key . ' is empty. Mandatory fields includes ' . json_encode($mandatory_fields));
                }
            }
        } else {
            $mandatory_fields = array(
                'receipt', 'amount', 'account_number', 'channel', 'reference'
            );
            $this->api_param = request()->all();
            foreach ($mandatory_fields as $key) {
                if (!isset($this->api_param[$key])) {
                    die($key . ' is empty. Mandatory fields includes ' . json_encode($mandatory_fields));
                }
            }
        }
    }

    public function acceptPayment($amount, $invoice_id, $payment_method, $receipt, $mobile_transaction_id, $customer_name, $account_number, $timestamp, $token) {

        $financial_id = count($this->api_info) == 1 ? $this->api_info->financial_entity_id : \App\Model\Financial_entity::where('name', request('method'))->first()->id;

        $payment_array = array(
            "invoice_id" => $invoice_id,
            "amount" => $amount,
            "method" => $payment_method,
            "transaction_id" => $receipt,
            "mobile_transaction_id" => $mobile_transaction_id,
            'account_number' => $account_number,
            'note' => $customer_name,
            'transaction_time' => $timestamp,
            'token' => $token,
            'financial_entity_id' => $financial_id,
            //special case for CRDB payments only
            'checksum' => $this->request_value('checksum'),
            'payment_type' => $this->request_value('paymentType'),
            'amount_type' => $this->request_value('amountType'),
            'currency' => $this->request_value('currency')
        );

        $payment_id = DB::table('payments')->insertGetId($payment_array);
        $invoice_fee_ids = \App\Model\Invoice_fee::where('invoice_id', $invoice_id)->get();
        $status = 1;
        foreach ($invoice_fee_ids as $invoice_fee_data) {
            if ($invoice_fee_data->status <> 1 && $amount > 0) {
                if ($amount >= $invoice_fee_data->amount) {
                    $status = 1;
                    Invoice_fees_payment::create([
                        'invoice_fee_id' => $invoice_fee_data->id,
                        'payment_id' => $payment_id,
                        'paid_amount' => $invoice_fee_data->amount,
                        'status' => $status
                    ]);
                    $amount = $amount - $invoice_fee_data->amount;
                } else {
                    //amount is less than invoice paid amount
                    $status = 2;
                    Invoice_fees_payment::create([
                        'invoice_fee_id' => $invoice_fee_data->id,
                        'payment_id' => $payment_id,
                        'paid_amount' => $amount,
                        'status' => $status
                    ]);
                    $amount = $amount - $amount;
                }
            }
        }
        $invoice = Invoice::find($invoice_id);
        $invoice->update(['status' => $status]);
        $amount > 0 ? \App\Model\Wallet::create(['user_id' => $invoice->user_id, 'amount' => $amount, 'invoice_id' => $invoice_id, 'status' => 1]) : '';
        return $this->paymentBalance($payment_id, $status);
    }

    public function paymentBalance($payment_id, $status) {
        $code = 'ERB' . rand(43, 43434);
        DB::table('receipts')->insert(['payment_id' => $payment_id, 'code' => $code]);
        $invoice = Payment::find($payment_id)->invoice;
        $this->sendBarcodeForm($payment_id);
        (new PaymentController())->sendNotification($invoice);
        if ($status == 1) {
            //amount has been paid correctly to more than one id so the returned id should be changed.
            return json_encode(array('control' => 1, 'description' => 'Invoice fully paid', 'payment_id' => $payment_id));
        } else if ($status == 2) {
            return json_encode(array('control' => 2, 'description' => 'Invoice partially paid', 'payment_id' => $payment_id));
        } else {
            return json_encode(array('control' => 3, 'description' => 'Invoice is paid amount more than invoiced amount', 'payment_id' => $payment_id));
        }
    }

    /**
     * 
     * @param type $payment_id
     * @access : Via kernel background operation
     */
    function sendBarcodeForm($payment_id = null) {
        $payment = $this->data['payment'] = Payment::find($payment_id);
        $id = $payment->invoice->user_id;
        $content = 'Please Click the link below to download/print your barcode form'
                . '<br/>'
                . '<br/>'
                . '<a href="https://engineersday.co.tz/user/ticket/' . $id . '?auth=' . encrypt($id) . '" style="display: inline-block; margin-bottom: 0; font-weight: 40px; text-align: center;
    vertical-align: middle; cursor: pointer; background-image: none; border: 1px solid transparent; white-space: nowrap; padding: 12px 24px; font-size: 14px; line-height: 1.428571429; border-radius: 4px;color: #fff; background-color: #5cb85c; border-color: #4cae4c;">EVENT BARCODE</a>';
        $this->send_email($payment->invoice->user->email, 'ERB Payment Accepted - Event Barcode ticket', $content);
    }

    
    /**
     * @return JSON OBJECT
     */
    private function secondRequest() {

        $this->checkEmptyKeys();
        $reference = $this->request_value('reference') == null ? $this->request_value('paymentReference') : $this->request_value('reference');
        $receipt = $this->request_value('receipt') == null ? $this->request_value('transactionRef') : $this->request_value('receipt');
        $customer_name = $this->request_value('customer_name') == null ? $this->request_value('payerName') : $this->request_value('customer_name');
        $timestamp = $this->request_value('timestamp') == null ? $this->request_value('transactionDate') : $this->request_value('timestamp');
        $channel = $this->request_value('channel') == null ? $this->request_value('transactionChannel') : $this->request_value('channel');
        $invoice = Invoice::where(DB::raw("lower(number)"), strtolower($reference))->first();

        if (count($invoice) > 0) {
// This is when a bank return payment status to us
//save it in the database

            $payments = Payment::where('transaction_id', $receipt)->first();
            if (count($payments) > 0) {
                if (strlen($this->request_value('checksum')) > 4) {
                    $data = array(
                        'status' => 206,
                        'statusDesc' => 'Duplicate entry',
                        "data" => ["receipt" => $payments->receipt->number]
                    );
                    die(json_encode($data));
                } else {
                    $data = array(
                        'status' => 1,
                        'success' => 0,
                        'reference' => $invoice->number,
                        'description' => 'Transaction ID has been used already to commit transaction'
                    );
                    die(json_encode($data));
                }
            }
            $mobile_transaction_id = $this->request_value('mobile_transaction_id');

            $accept_payment = $this->acceptPayment($this->request_value('amount'), $invoice->id, $channel, $receipt, $mobile_transaction_id, $customer_name, $this->request_value('account_number'), $timestamp, $this->request_value('token'));
            $query = (object) json_decode($accept_payment);
//push this status to ERB system now, receipt will be sent by the system
            //$this->dispatch(new \App\Jobs\SyncPayment($query->payment_id));
            if (in_array($query->control, [1, 2, 3])) {
                $receipt = \App\Model\Receipt::where('payment_id', $query->payment_id)->first();
                //successful payment
                //return feedback to a bank to send SMS too
                if (strlen($this->request_value('checksum')) > 4) {
                    $data = [
                        "status" => 200,
                        "statusDesc" => "success",
                        "data" => [
                            "receipt" => $receipt->number]
                    ];
                } else {
                    $data = array(
                        'status' => $query->control,
                        'success' => 1,
                        'reference' => $invoice->number,
                        'receipt' => $receipt->number,
                        'description' => $query->description
                    );
                }
                echo json_encode($data);
            }
        } else {
//we don't have this invoice
            $data = array(
                'status' => 0,
                'success' => 0,
                'reference' => $reference,
                'description' => 'Unknown reference number'
            );
            echo json_encode($data);
        }
    }

    /**
     * @return JSON OBJECT
     */
    public function api() {
        /*         * -------------payment flow will be as follows-----------------

         * 1. validate api_key and api_secret by checking in the database
          2. if the request=1, take the invoice form the database and send it to client with
          request id, status,param (invoice, amount,currency,account)
          3. if request is 2, then we expect to receive payment information from a bank
          and the status if the payment is complete or not, if complete, we return
          to client request, status, param (invoice, receipt)
         */


        $this->logRequests();

        $request = $this->initApiRequest();

        switch ($request) {
            case 1:
                //this is the first request from a bank
                // fetch user request and send parameters to us
                $this->firstRequest();
                break;
            case 2:
                //this is the second request from a bank with the same invoice no
                $this->secondRequest();
                break;
            default:
                $data['request'] = 1;
                $data['status'] = 203;
                $data['param'] = array(
                    'invoice' => 'NILL'
                );
                echo json_encode($data);
                break;
        }
    }

    /**
     *
     * @param type $input string
     * @return type
     */
    function encrypt($input) {
        return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, hash('sha256', ENCRYPTION_KEY, TRUE), $input, MCRYPT_MODE_ECB, mcrypt_create_iv(32)));
    }

    /**
     * @used Generate payment receipt to a user
     * @depends payment_insertion
     * @var session_id=session from someone who log in
     * @var payment_id
     */
    function code($nc = 6, $a = 'ABCDEFGHIJKLMNPQRSTUVWXYZ123456789') {
        $l = strlen($a) - 1;
        $r = '';
        while ($nc-- > 0)
            $r .= $a{mt_rand(0, $l)};

        return $r;
    }

    /*
     * *  Function:   convert_number
     * *  Arguments:  int
     * *  Returns:    string
     * *  Description:
     * *      Converts a given integer (in range [0..1T-1], inclusive) into
     * *      alphabetical format ("one", "two", etc.).
     */

    public function convert_number($number) {
        if (($number < 0) || ($number > 999999999)) {
            return "$number";
        }

        $Gn = floor($number / 1000000);  /* Millions (giga) */
        $number -= $Gn * 1000000;
        $kn = floor($number / 1000);     /* Thousands (kilo) */
        $number -= $kn * 1000;
        $Hn = floor($number / 100);      /* Hundreds (hecto) */
        $number -= $Hn * 100;
        $Dn = floor($number / 10);       /* Tens (deca) */
        $n = $number % 10; /* Ones */

        $res = "";

        if ($Gn) {
            $res .= $this->convert_number($Gn) . " Million";
        }

        if ($kn) {
            $res .= (empty($res) ? "" : " ") .
                    $this->convert_number($kn) . " Thousand";
        }

        if ($Hn) {
            $res .= (empty($res) ? "" : " ") .
                    $this->convert_number($Hn) . " Hundred";
        }

        $ones = array("", "One", "Two", "Three", "Four", "Five", "Six",
            "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen",
            "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eightteen",
            "Nineteen");
        $tens = array("", "", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty",
            "Seventy", "Eigthy", "Ninety");

        if ($Dn || $n) {
            if (!empty($res)) {
                $res .= " and ";
            }

            if ($Dn < 2) {
                $res .= $ones[$Dn * 10 + $n];
            } else {
                $res .= $tens[$Dn];

                if ($n) {
                    $res .= "-" . $ones[$n];
                }
            }
        }

        if (empty($res)) {
            $res = "zero";
        }

        return $res;
    }

    /**
     * These fxns are specific for EGA integration
     */
    public function createControlNumber($user, $amount) {
        $setting=\App\Model\Setting::first();
        $obj = [
            'fullName' => $user->name,
            'email' => $user->email,
            'phone' => validate_phone_number($user->phone)[1],
            'dueDate' => str_replace('+','.',date('Y-m-d\TH:i:sO',strtotime($setting->payment_deadline))),
            'amount' => $amount,
            'currency' => 'TZS',
            'token' => md5($this->ega_secret),
            'checkSum' => sha1($this->ega_secret . md5($user->email))
        ];

        $return = (object) $this->curlServer($obj, $this->ega_submit_url,array('Content-Type:application/json'));

        $data = ['content' => json_encode($return),
            'remote_ip' => '',
            'remote_hostname' => ''
        ];
        DB::table('requests')->insert($data);
        return json_decode($return->scalar);
    }

}
