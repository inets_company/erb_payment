<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Model\User;
use \App\Model\Role;
use \App\Model\Fee;
use \App\Model\User_type;
use \App\Model\Event;
use \App\Model\Employer;
use \App\Model\Payment;
use \App\Model\Invoice;
use \App\Model\Profession;
use \App\Model\Financial_entity;
use \App\Model\Sms_template;
use DB;
use PDF;
use Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\PaymentController;

class UserController extends Controller {

    public function __construct() {
        if (request('auth') == NULL) {
            $this->middleware('auth');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //profile
    }

    public function profile($id) {
        $this->data['user'] = User::find($id);
        return view('user.profile', $this->data);
    }

    public function ticket($id = null) {
        $this->data['padding_ticket'] = 1;
        $this->data['token'] = request('auth');
        if (strlen(request('auth')) > 2) {
            $auth_token = decrypt($this->data['token']);
            if ($auth_token != $id) {
                die('Request is not valid. Please click the link as supplied in your email address');
            }
        }
        $this->data['id'] = $id;
        return view('user.ticket', $this->data);
    }

    public function nametag($id = null) {
        //    $this->data['user'] = User::find($id);
//        $this->data['event'] = Event::first();
//        $this->data['barcode'] = (new SettingController())->createBarCode($id);
//        $this->data['setting'] = \App\Model\Setting::first();
        return redirect('user/bulknametag/?single=1&ids=' . $id);
        //     return view('user.nametag', $this->data);
    }

    public function storePrintEvent($ids, $event) {
        $ip = $_SERVER['REMOTE_ADDR'] ?: ($_SERVER['HTTP_X_FORWARDED_FOR'] ?: $_SERVER['HTTP_CLIENT_IP']);
        foreach ($ids as $id) {
            \App\Model\Nametag_printlog::create([
                'user_id' => $id,
                'staff_id' => Auth::user()->id,
                'event_id' => $event->id,
                'user_agent' => json_encode(['printer'=>request('single')==1 ?'EPSON':'HP','IP'=>$ip])
            ]);
        }
    }

    public function bulkNameTag() {
        $ids = explode(',', trim(request('ids'), ','));
        $this->data['users'] = User::whereIn('id', array_filter($ids))->where('is_employer', '<>', 1)->get();
        $this->data['event'] = Event::first();
        $this->data['setting'] = \App\Model\Setting::first();
        $this->storePrintEvent($ids, $this->data['event']);
        if (request('single') == 1) {
            // return view('user.bulknametag', $this->data);
            PDF::setOptions(['dpi' => 10, 'defaultFont' => 'sans-serif']);
            $pdf = PDF::loadView('user.bulknametag', $this->data);
            $pdf->setPaper('A4', 'portrait');
            return $pdf->stream('pdf_nametag.pdf');
        }
        PDF::setOptions(['dpi' => -10, 'defaultFont' => 'sans-serif']);
        $pdf = PDF::loadView('user.bulknametag', $this->data);
        $pdf->setPaper('A4', 'landscape');
        return $pdf->stream('pdf_nametag.pdf');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        if ($request->user == 'entity') {
            Financial_entity::create($request->all());
        } else if ($request->user == 'user_type') {
            User_type::create($request->all());
        } else if ($request->user == 'role') {
            Role::create($request->all());
        } else if ($request->user == 'user') {
            $this->validate(request(), ['phone' => 'required|unique:users,phone',
                'email' => 'required|email|unique:users,email']);
            $role_id = request('role_id');
            $pass = rand(43434, 4343434);
            if ((int) $role_id > 0) {
                $message = 'Your Account has been created'
                        . '<br/>'
                        . 'Login email:' . $request->email
                        . '<br/>'
                        . 'Login password:' . $pass;
                $this->send_email($request->email, 'User Account', $message);
            }
            User::create(array_merge($request->all(), ['password' => bcrypt($pass)]));
        } else if ($request->user == 'fee') {
            Fee::create($request->all());
        } else if ($request->user == 'event') {
            Event::create($request->all());
        } else if ($request->user == 'employer') {
            Employer::create($request->all());
            User::create(array_merge($request->all(), ['password' => 123456789, 'is_employer' => 1]));
        } else if ($request->user == 'profession') {
            Profession::create($request->all());
        } else if ($request->user == 'sms_template') {
            Sms_template::create($request->all());
        } else if ($request->user == 'sms') {
            (new \App\Http\Controllers\InboxController())->sendSms();
        } else if ($request->user == 'schedule') {
            \App\Model\Schedule::create(array_merge($request->except('days', 'time'), ['days' => implode(',', $request->days), 'time' => date('Y-m-d h:i', strtotime($request->time))]));
        } else if ($request->user == 'payment') {
            return (new PaymentController())->store();
        }
        return redirect()->back()->with('success', 'success');
    }

    public function saveUser($basic_param, $other) {
        $user = User::orWhere($basic_param)->first();
        if (count($user) == 0) {
            $record = User::create(array_merge($basic_param, array('user_type_id' => $other['id'])));
        } else {
            $record = $user;
            $user->update(array_merge($basic_param, array('user_type_id' => $other['id'])));
        }
        return $record;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        if ($id == 'entity') {
            $this->data['entities'] = Financial_entity::all();
            return view('user.entity', $this->data);
        } else if ($id == 'applicants') {
            $type = request('user_type');
            $user_types = $type == null || $type == 0 ? User_type::get(['id']) : [$type];
            if (request('paid') == 1) {
                $this->data['applicants'] = User::whereNull('role_id')->where('user_type_id', 9)->whereIn('id', Invoice::whereIn('id', Payment::get(['invoice_id']))->get(['user_id']))->get();
            } else if ($type == null) {
                $this->data['applicants'] = [];
            } else if ($type == 0) {
                $this->data['applicants'] = User::whereNull('role_id')->paginate(40);
            } else {
                $this->data['applicants'] = $type == 120 ? User::where('is_employer', 1)->paginate(40) :
                        User::whereNull('role_id')->whereIn('user_type_id', $user_types)->paginate(40);
            }
            return view('user.applicants', $this->data);
        } elseif ($id == 'organizations') {
            $this->data['organizations'] = \App\Model\Employer::paginate(15);
            return view('user.organizations', $this->data);
        } else if ($id == 'bulknametag') {
            return $this->bulkNameTag();
        } else if ($id == 'getApplicants') {
            return $this->getApplicants();
        } else if ($id == 'attendance') {
            return $this->attendance();
        } else if ($id == 'addAttendance') {
            return $this->addAttendance();
        } else if ($id == 'addBulkAttendance') {
            return $this->AddBulkAttendance();
        } else if ($id == 'barcodeAttendance') {
            return $this->barcodeAttendance();
        } else if ($id == 'invite') {
            return $this->invite();
        } else {
            $this->data['users'] = User::whereNotNull('role_id')->get();
            return view('user.staff', $this->data);
        }
    }

    public function invite() {
        $this->data['applicants'] = User::whereNull('role_id')->where('user_type_id', 13)->get();
        return view('user.invite', $this->data);
    }

    public function barcodeAttendance() {
        $number = request('s');
        $user = User::where(DB::raw('lower(number)'), strtolower(trim($number)))->first();
        if (count($user) == 1) {
            $day = request('date');
            $add = $this->addSingleUserAttendance($user->id, $day, 1);
            echo $add == true ? ('<span class="label label-success">success</span>') :
                    '<span class="label label-info">updated</span>';
        } else {
            echo '<span class="label label-danger">Error: User not found</span>';
        }
    }

    public function addBulkAttendance() {
        $type = request('type');
        $user_types = $type == null || $type == 0 ? User_type::get(['id']) : [$type];
        if ($type == null || $type == 0) {
            $users = User::whereNull('role_id')->get();
        } else {
            $users = $type == 120 ? User::where('is_employer', 1)->get() :
                    User::whereNull('role_id')->whereIn('user_type_id', $user_types)->get();
        }
        foreach ($users as $user) {
            $present = request('status') == 'false' ? 0 : 1;
            $this->addSingleUserAttendance($user->id, request('date'), $present);
        }
        echo 'success';
    }

    function addAttendance() {
        $id = request('user_id');
        $day = request('date');
        if ((int) $id) {
            $present = request('status') == 'false' ? 0 : 1;
            $add = $this->addSingleUserAttendance($id, $day, $present);
            echo $add == true ? ('success') : 'updated';
        }
    }

    public function addSingleUserAttendance($user_id, $day, $present) {
        $where = ['user_id' => $user_id, 'date' => $day];
        $found = \App\Model\Attendance::where($where);
        if (count($found->first()) == 1) {
            //update              
            $data = array_merge($where, ['created_by' => Auth::user()->id,
                'present' => $present]);
            $found->update($data);
            return false;
        } else {
            \App\Model\Attendance::create(array_merge($where, ['created_by' => Auth::user()->id,
                'present' => $present]));
            return TRUE;
        }
    }

    public function attendance() {
        $type = request('user_type');
        $user_types = $type == null || $type == 0 ? User_type::get(['id']) : [$type];
        $this->data['event'] = \App\Model\Event::first();
        $this->data['applicants'] = User::where('is_employer', 0)->get();

        return view('user.attendance', $this->data);
    }

    public function getApplicants() {
        $type = request('type');
        $user_types = $type == null || $type == 0 ? User_type::get(['id']) : [$type];
        if ((int) request('employer_id') > 0) {
            $other_return = [];
            if ((int) request('user_id')) {
                $invoice = \App\Model\Invoice::where('user_id', request('user_id'))->first();
                $other_return = $invoice->invoiceFee()->get(['user_id as id']);
            }
            $obj = User::where('employer_id', request('employer_id'))->where('is_employer', '<>', 1)->get(['id']);
            $returns = $obj->merge($other_return);
        } else {
            $returns = ($type == null || $type == 0) ?
                    User::whereNull('role_id')->get(['id']) :
                    User::whereIn('user_type_id', $user_types)->get();
        }

        foreach ($returns as $return) {
            echo $return->id . ',';
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        User::find($id)->update($request->all());
        \App\Model\Email::where('user_id', $id)->update(['email' => request('email')]);
        \App\Model\Sms::where('user_id', $id)->update(['phone' => request('phone')]);
        return redirect()->back()->with('success', 'User Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (request('type') == 'user') {
            User::find($id)->delete();
        } else if (request('type') == 'user_type') {
            User_type::find($id)->delete();
        } else if (request('type') == 'role') {
            Role::find($id)->delete();
        } else if (request('type') == 'fee') {
            Fee::find($id)->delete();
        } else if (request('type') == 'event') {
            Event::find($id)->delete();
        } else if (request('type') == 'profession') {
            Profession::find($id)->delete();
        } else if (request('type') == 'sms_template') {
            Sms_template::find($id)->delete();
        } else if (request('type') == 'schedule') {
            \App\Model\Schedule::find($id)->delete();
        } else {
            Financial_entity::find($id)->delete();
        }
        return redirect()->back()->with('success', 'success');
    }

    public function password() {
        if ($_POST) {
            $current = request('current');
            $user = User::find(Auth::user()->id);
            if (Auth::attempt(['email' => $user->email, 'password' => $current])) {
                $new1 = request('new1');
                $new2 = request('new2');
                if ($new1 != $new2) {
                    return redirect()->back()->with('error', 'New password and confirmed one  do not matchs');
                }
                $user->update(['password' => Hash::make($new1)]);
                return redirect()->back()->with('success', 'Password changed successfully');
            } else {

                return redirect()->back()->with('error', 'Current Password is not valid');
            }
        }
        return view('auth.passwords.change', $this->data);
    }

}
