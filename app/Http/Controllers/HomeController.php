<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Model\Invoice;
use \App\Model\Payment;
use \App\Model\Invoice_fee;
use \App\Model\User;
use DB;

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    public function handle() {

        $payments = (array) \collect(DB::select('select a.number as booking_number,d.name, b.amount as amount_paid, d.phone as phone_number, b.method as description, b.transaction_id  as reference_number,  c.name as agent, \'dsdlksdslkjdlkssds\' as api_password FROM erb_payment.invoices a join erb_payment.payments b on b.invoice_id=a.id join erb_payment.financial_entity c on c.id=b.financial_entity_id join erb_payment.users d on d.id=a.user_id '))->first();
        // return $this->curlServer($payments, 'http://localhost/erb/erb_api.php');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $this->data['total_invoices'] = Invoice::count();
        $this->data['amount_tobe_collected'] = Invoice_fee::sum('amount');
        $this->data['total_money_collected'] = Payment::sum('amount');
        $this->data['total_users'] = User::count();
        $this->data['organizations'] = \App\Model\Employer::count();
        $this->data['applicants'] = User::whereNull('role_id')->count();
        $this->data['sms'] = \App\Model\Sms::count();
        $this->data['email'] = \App\Model\Email::count();
        $this->data['sms_pending'] = \App\Model\Sms::where('status', 0)->count();
        $this->data['email_pending'] = \App\Model\Email::where('status', 0)->count();
        $this->data['sms_status'] = json_decode(\karibusms::statistics());
        return view('home', $this->data);
    }

    public function search() {
        $q = request('s');
        if (strlen($q) > 1) { //prevent empty search which load all results
            $users = \App\Model\User::leftJoin('employers', 'employers.id', 'users.employer_id')->leftJoin('invoices', 'invoices.user_id', 'users.id')
                            ->where('employers.name', 'ilike', "%{$q}%")
                            ->orWhere('email', 'ilike', "%{$q}%")
                            ->orWhere('users.name', 'ilike', "%{$q}%")
                            ->orWhere('abbreviation', 'ilike', "%{$q}%")
                            //   ->orWhere('invoices.number', 'ilike', "%{$q}%")
                            ->orWhere('users.number', $q)
                            ->orWhere('phone', 'ilike', "%{$q}%")->select('users.*');
            
            $employers = \App\Model\Employer::where('name', 'ilike', "%{$q}%")
                    ->orWhere('abbreviation', 'ilike', "%{$q}%")
                    ->orWhere('location', 'ilike', "%{$q}%")->get();
                    
             strlen(request('user_id_tags')) > 1 ?
                    $users->orWhereIn('users.id', explode(',', trim(request('user_id_tags'),','))) : NULL;
           $this->data['users'] = $users->get();
            $this->data['employers'] = $employers;
            $invoices = \App\Model\Invoice::where('number', 'ilike', "%{$q}%")->get();
            $this->data['invoices'] = $invoices;
            return request('type') == 1 ?
                    view('layouts.search_focus', $this->data) : view('layouts.search', $this->data);
        }
    }

}
