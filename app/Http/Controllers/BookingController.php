<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Model\User;
use \App\Model\Invoice;
use \App\Model\Invoice_fee;
use \App\Model\Setting;
 use\App\Model\Event;
use PDF;
use Auth;
use DB;

class BookingController extends Controller {

    public $setting;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        parent::__construct();
        $this->setting = Setting::first();
    }

    public function index() {
        $this->data['landing'] = 1;
        $this->data['fee'] = \App\Model\Fee::find(1);
        $this->data['event'] = Event::first();
        return view('auth.booking', $this->data);
    }

    public function getFeeAmount() {
        $fee = \App\Model\Fee::find(1);
        $now = date('Y-m-d'); // or your date as well
        $your_date = date('Y-m-d', strtotime($fee->end_date));
        $date1 = strtotime($now);
        $date2 = strtotime($your_date);
        $days = $date1 - $date2;
        $amount = $days < 0 ?
                $fee->amount : $fee->amount + $fee->penalty_amount;
        return $amount;
    }

    public function landing() {
        if (Auth::check()) {
            return (new \App\Http\Controllers\HomeController())->index();
        }
        $this->data['fee'] = \App\Model\Fee::find(1);
        $this->data['event'] = Event::first();
        $this->data['landing'] = 1;
        $setting = Setting::first();
        if ($setting->site_mode == 3) {
            $page = 'certificate';
        } else if ($setting->site_mode == 2) {
            $page = 'inactive';
        } else {
            $page = 'landing';
        }
        return view('landing.' . $page, $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    public function validateBookig() {

        return $this->validate(request(), [
                    'name' => 'required',
                    'phone' => 'required|min:6|max:15',
                    'email' => 'required|email|unique:users,email',
                    'profession_id' => 'required',
                    'employer_id' => 'required'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * //create booking here
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validateBookig();
        $phone = validate_phone_number(request('phone'));
        if (count($phone) <> 2) {
            return redirect()->back()->with('error', 'Phone number ' . request('phone') . ' is not valid');
        }
        
        
        $amount = $this->getFeeAmount();
        $valid_phone = $phone[1];
      
       $check_phone=User::Where(['phone' => $valid_phone])->first();
      
        if (count($check_phone) > 0) {
            return redirect()->back()->with('error', 'Phone number ' . request('phone') . ' exists');
        } 
         
        $user_info = User::orWhere(['phone' => $valid_phone, 'email' => $request->email])->first();
        $this->setting = Setting::first();
        $user_type = \App\Model\User_type::where('name', 'non-sponsored')->first();

        if (count($user_info) < 1) {
            $users = array_merge($request->except('_token', 'phone'), ['password' => 123456789, 'phone' => $valid_phone, 'user_type_id' => count($user_type) == 1 ? $user_type->id : NULL]);
            $user = User::create($users);
        } else {
            $user = $user_info;

            $normal_invoice = $user->invoice()->orderBy('id', 'desc')->first();
            $sponsored_invoice = $user->invoiceFee()->first();
            if (count($normal_invoice) == 1) {
                $this->notifyUser($normal_invoice, $this->setting);
                return redirect(url('booking/' . $user->invoice()->orderBy('id', 'desc')->first()->id))->with('info', 'Invoice for this user has already being created');
            } else if (count($sponsored_invoice) == 1) {
                return redirect()->back()->with('success', 'Invoice for this user has already being created');
            }
        }

        $booking = array_merge($request->all(), ['user_id' => $user->id,
            'amount' => $amount,
            'username' => $this->setting->username,
            'password' => $this->setting->password
        ]);
        $fee = \App\Model\Fee::find(1);
        $return = $this->createBooking($booking, $fee->id, false);
        //$return = (new \App\Http\Controllers\InvoiceController())->storeInvoice($this->curlServer($booking, $setting->create_invoice_url), $user->id, $fee->id);
        if (count($return) == 1) {
            //invoice created successfully
            return redirect(url('booking/' . $return->invoice_id));
        } else {
            return redirect()->back()->with('error', 'Invoice Failed to be Created. Please try again later');
        }
    }

    public function createBooking($book, $fee_id, $is_bulk = false) {
        $booking = (object) $book;
        $user = \App\Model\User::find($booking->user_id);
       
        $invoice_number = Invoice::orderBy('id','desc')->first();
        $number=1;
        if(count($invoice_number)>0) {
       
          $number=(int) $invoice_number->id + 1;
        }
     $ega_invoice = $this->createInvoiceNo($user, $booking->amount);
      $invoice_param = [
            'number' => $number,
            'user_id' => $booking->user_id,
            'bil_id' => $ega_invoice->billId,
            'title' => request('title'),
            'optional_name' => request('optional_name'),
            'date' => 'now()',
            'type' => $is_bulk == FALSE ? 0 : 1,
            'year' => date('Y')];      
      
        $invoice = Invoice::create($invoice_param);
        $this->handle($invoice->id);
        $this->notifyUser($invoice, $this->setting);
        return $is_bulk == false ?
                Invoice_fee::create(['invoice_id' => $invoice->id, 'fee_id' => $fee_id, 'amount' => $booking->amount, 'item_name' => $booking->name, 'note' => 'Engineers  Registration Board (ERB), Engineers Day Event']) :
                (object) ['invoice_id' => $invoice->id, 'amount' => $booking->amount];
    }

    public function createInvoiceNo($user, $amount) {
        return $this->createControlNumber($user, $amount);
        // $client = Client::first();
        $invoiceNo = $this->setting->institution_code . rand(11, 9997);
        //  $invoiceNo = $client->institution_code . rand(1, 999) . substr(str_shuffle('ABCDEFGHJKLMNPQRSTUVWXYZ'), 0, 3) . rand(1, 999);
        $data = Invoice::where('number', $invoiceNo)->first();
        if (!empty($data)) {
            return $this->createInvoiceNo();
        } else {
            return $invoiceNo;
        }
    }

    public function createControlNumber($user, $amount) {
        $api = new \App\Http\Controllers\ApiController();
//        return (object) [
//                    "billId" => 36,
//                    "controlNumber" => null,
//                    "fullName" => "Othman Omary",
//                    "email" => "othman.omary@ega.go.tz",
//                    "phone" => "255710826932",
//                    "createDate" => "2019-07-18T14:49:17.261",
//                    "dueDate" => "2019-06-18T16:23:15.052",
//                    "checkSum" => "396884f5b5c67df630a307fdd66e2f800e7e667d"
//        ];
        return  $api->createControlNumber($user, $amount);
    }

    public function handle($invoice_id = null) {
        $invoices = $invoice_id == null ?
                DB::select('select * from erb_new_payment.invoice_view where sync <>1 limit 8') :
                DB::select('select * from erb_new_payment.invoice_view where invoice_id=' . $invoice_id);
        $this->setting = Setting::first();
        if (count($invoices) > 0) {
            foreach ($invoices as $invoice) {
                $token = $this->getToken();
                if (strlen($token) > 4) {
                    $fields = array(
                        "reference" => $invoice->number,
                        "student_name" => $invoice->name,
                        "student_id" => $invoice->user_id,
                        "amount" => $invoice->amount,
                        "type" => 'ERB',
                        "code" => "10",
                        "callback_url" => "http://158.69.112.216:8010/api/init",
                        "token" => $token
                    );
                    $push_status = $invoice->status == 2 ? 'invoice_update' : 'invoice_submission';
                    //live invoice

                    $url = 'https://api.mpayafrica.co.tz/v2/' . $push_status;

                    $curl = $this->curlServer($fields, $url);
                    $result = json_decode($curl);
                    if (($result->status == 1 && strtolower($result->description) == 'success') || $result->description == 'Duplicate Invoice Number') {
//update invoice no
                        DB::table('invoices')
                                ->where('number', $invoice->number)->update(['sync' => 1, 'return_message' => $curl, 'push_status' => $push_status]);
                    } else {
                        DB::table('requests')->insert(['content' => $curl . ', Failed Token, request=' . json_encode($fields)]);
                    }
                } else {
                    DB::table('requests')->insert(['content' => $token . ', request=' . json_encode($invoice) . ' invoice=' . $invoice->number]);
                }
            }
        }
    }

    /**
     *
     * @param type $schema
     * @return type
     *             $user = '107M17S666D381';
      $pass = 'rWh$abB!P5&$MWvj$!DTe29F#vAu2tmct!2';
     *
      Username: 109M17SA01DINET
      Password : LuHa6bAjKV5g5vyaRaRZJy*x5@%!yBBBTVy  , mother of mercy
     */
    public function getToken() {
        //later on this invoice will have a client id, thus we will select specific client but for now, lets use ERB
        //live invoice
        $url = 'https://api.mpayafrica.co.tz/v2/auth';
        // }
        $user = $this->setting->username;
        $pass = $this->setting->password;
        $curl = [
            'username' => "$user",
            'password' => "$pass"
        ];
        $request = $this->curlServer($curl, $url);
        $obj = json_decode($request);
        DB::table('requests')->insert(['content' => $request . ' request=' . $request . '*cred=' . json_encode($curl)]);
        if (isset($obj) && is_object($obj) && isset($obj->status) && $obj->status == 1) {
            return $obj->token;
        }
    }

    function notifyUser($normal_invoice, $setting) {
        $message = 'Your invoice has been created. Use the following information to make payments'        
                . '<li>Payment Methods:'
                . '     <p>   <br/><b>Via Mobile Network Operators (MNO)</b><br/>
                         Enter to the respective USSD Menu of MNO,Select 4 (Make Payments) for Tigopesa and Mpesa and Select 5 for AirtelMoney,Select 5 (Government Payments),Enter control number,Enter password</p>
                                                               
                                                                
<p>   <br/><b>Kupitia Mitandao ya Simu</b><br/>
                         Enter to the respective USSD Menu of MNO,Select 4 (Make Payments) for Tigopesa and Mpesa and Select 5 for AirtelMoney,Select 5 (Government Payments),Enter control number,Enter password</p>
                                                                <p>
                                                                
<b>Kupitia Bank</b>
                                                            <br/>
  Fika tawi lolote au wakala wa benki ya NMB, CDRB, NBC ukiwa na namba yako ya  kumbukumbu kutoka Kwenye mfumo <br/>

</p> 

                                                         <p>
                                                                
<b>FOR BANKS</b>
                                                            <br/>
       Visit any branch or bank agent of NMB, CRDB, NBC with your control number obtained from the system
<br/>
<b>(You are advised to print this invoice and submit it to the bank along with the appreciate amount)</b>


</p>

</li></ul>'
                . '<p>All payments without this reference number will not be processed</p><br/>Thank You';
        $this->send_email($normal_invoice->user->email, 'ERB Invoice Number Reminder', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $this->data['event'] = Event::first();
        $this->data['invoice'] = Invoice::find($id);
        $this->data['fee'] = \App\Model\Fee::first();
        return view('invoice.show', $this->data);
    }

    public function downloadPdf($id) {
        $this->data['event'] = Event::first();
        $this->data['invoice'] = Invoice::find($id);
        $this->data['fee'] = \App\Model\Fee::first();
        //return view('invoice.download', $this->data);
        $pdf = PDF::loadView('invoice.download', $this->data);
        return $pdf->download('invoice.pdf');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function search() {
        $phone = validate_phone_number(request('tag'))[1];
        $user_base_records = ['email' => trim(strtolower(request('tag'))), 'phone' => $phone == null ? 0 : $phone];
        $user_info = User::orWhere($user_base_records)->first();

        if (count($user_info) == 0) {
            echo json_encode(['message' => 'Sorry: Information does not exists', 'alert_status' => 'alert-danger']);
        } else {
            $no_such_invoice = json_encode([
                'message' => 'Sorry: No invoice has been created under such email or phone',
                'alert_status' => 'alert-danger']);

            if ($user_info->role_id == NULL) {
                if ((int) $user_info->is_employer == 1) {
                    $user_bulk_invoice = Invoice::where('user_id', $user_info->id)->first();
                    if (count($user_bulk_invoice) == 1) {
                        $setting = Setting::first();
                        $this->notifyUser($user_bulk_invoice, $setting);
                        echo json_encode(['message' => 'Success: Your Invoice/Reference number is ' . $user_bulk_invoice->number . '. <a href="' . url('booking/' . $user_bulk_invoice->id) . '" id="link" class="badge badge-success">Click here to view your invoice</a> ', 'alert_status' => 'alert-success']);
                    } else {
                        echo $no_such_invoice;
                    }
                } else {
                    $user_invoice = Invoice::where('user_id', $user_info->id)->first();
                    $invoice_in_bulk = Invoice_fee::where('user_id', $user_info->id)->first();
                    if (count($user_invoice) == 1) {
                        echo json_encode(['message' => 'Success: Your Invoice/Reference number is ' . $user_invoice->number . '. <a href="' . url('booking/' . $user_invoice->id) . '" id="link" class="badge badge-success">Click here to view your invoice</a> ', 'alert_status' => 'alert-success']);
                    } else if (count($invoice_in_bulk) == 1) {
                        echo json_encode(['message' => 'Success: Your invoice has been created under sponsorship of ' . $invoice_in_bulk->invoice->user->name, 'alert_status' => 'alert-success']);
                    } else {
                        echo $no_such_invoice;
                    }
                }
            } else {
                echo $no_such_invoice;
            }
        }
    }

}
