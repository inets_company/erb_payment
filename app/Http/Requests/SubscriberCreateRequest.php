<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubscriberCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'        => 'required',
            'last_name'         => 'required',
            'gender'            => 'required',
            'email'             => 'required|email|unique:subscribers',
            'phone_mobile'      => 'required|unique:subscribers',
            'address_street'    => 'required',
            'pobox_region'      => 'required_with:pobox_number',
            'domestic_use_tv_count' => 'required_if:domestic_use,==,yes',
            'address_region'    => 'required',
        ];
    }
}
