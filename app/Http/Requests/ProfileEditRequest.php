<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfileEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'    => 'required',
            'last_name'     => 'required',
            'email'         => 'required|email|exists:users',
            'phone'         => 'required|exists:users',
            'gender'        => Rule::in(['male', 'female']),
            'birthday'      => [
                'required',
                'date',
                'before:'. Carbon::now()->subYears(18),
            ],
            'role'          => 'required',
        ];
    }
}
