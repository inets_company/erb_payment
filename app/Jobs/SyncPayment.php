<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SyncPayment implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  
    protected $payment_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($payment_id) {
        //
        $this->payment_id = $payment_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        // we will add url for client to know where to push payment status
        'agent
reference_number
phone_number
name
booking_number
amount_paid
description
api_password
';
        $payments=\collect(DB::select('select a.number as booking_number,d.name, b.amount as amount_paid, d.phone as phone_number, b.method as description, b.transaction_id  as reference_number,  c.name as agent, \'dsdlksdslkjdlkssds\' as api_password FROM erb_payment.invoices a join erb_payment.payments b on b.invoice_id=a.invoice_id join erb_payment.financial_entity c on c.id=b.financial_entity_id join erb_payment.users d on d.id=a.user_id '))->first()->toArray();
        return $this->curlServer($payments,'http://localhost/erb/erb_api.php');
    }

     /**
     * 
     * @param type $fields
     */
    private function curlServer($fields, $url) {
// Open connection
        $ch = curl_init();
// Set the url, number of POST vars, POST data

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'application/x-www-form-urlencoded'
        ));

        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}
