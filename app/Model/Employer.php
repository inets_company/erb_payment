<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Employer extends Model
{
      protected $fillable = [
        'name', 'location', 'abbreviation','tin','box'
    ];
}
