<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Receipt extends Model
{
    public function payment() {
        return $this->belongsTo('\App\Model\Payment');
    }
}
