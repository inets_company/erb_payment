<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
      protected $fillable = [
        'type', 'days', 'time', 'sms_template_id','name'
    ];

    public function smsTemplate() {
        return $this->belongsTo('\App\Model\Sms_template')->withDefault(['name'=>'No name']);
    }
}
