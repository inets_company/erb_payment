<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Fee extends Model
{
      protected $fillable = [
        'name', 'amount', 'start_date','end_date','penalty_amount'
    ];
}
