<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'user_type_id', 'role_id', 'number', 'phone', 'profession_id', 'is_employer', 'employer_id', 'created_by'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role() {
        return $this->belongsTo('\App\Model\Role')->withDefault(['name' => 'NO role']);
    }

    public function userType() {
        return $this->belongsTo('\App\Model\User_type')->withDefault(['name' => 'unknown']);
    }

    public function invoice() {
        return $this->hasMany('\App\Model\Invoice');
    }

    public function invoiceFee() {
        return $this->hasMany('\App\Model\Invoice_fee');
    }

    public function profession() {
        return $this->belongsTo('\App\Model\Profession')->withDefault(['name' => 'unknown']);
    }

    public function employer() {
        return $this->belongsTo('\App\Model\Employer')->withDefault(['name' => 'unknown']);
    }

    public function attendance() {
        return $this->hasMany('\App\Model\Attendance');
    }

    public function payment() {
        return $this->hasManyThrough('\App\Model\Payment', '\App\Model\Invoice');
    }

    public function nametagPrintlog() {
        return $this->hasMany('App\Model\Nametag_printlog');
    }

}
