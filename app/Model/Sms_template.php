<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Sms_template extends Model
{
    protected $fillable = [
        'name', 'message', 'created_by','phone_numbers'
    ];
}
