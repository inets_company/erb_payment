<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Api extends Model
{
    public $table='api';
    
    public function financialEntity() {
       return $this->belongsTo('\App\Model\Financial_entity'); 
    }
}
