<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model {

    protected $guarded = ['id'];

    public function invoice() {
        return $this->belongsTo('\App\Model\Invoice');
    }

    public function financial_entity() {
        return $this->belongsTo('\App\Model\Financial_entity')->withDefault(['name' => 'unknown']);
    }

    public function receipt() {
        return $this->hasOne('\App\Model\Receipt');
    }

}
