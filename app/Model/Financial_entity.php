<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Financial_entity extends Model {

    public $table = 'financial_entity';
    protected $fillable = [
        'name', 'email', 'location', 'phone'
    ];

}
