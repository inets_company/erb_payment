<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Email extends Model {

     protected $fillable = [
        'body', 'email', 'user_id', 'status', 'subject'
    ];
    public function user() {
        return $this->belongsTo('\App\Model\User')->withDefault(['name'=>'']);
    }

}
