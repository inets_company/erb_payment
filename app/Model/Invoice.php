<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model {

    protected $guarded = ['push_status'];

    public function user() {
        return $this->belongsTo('\App\Model\User');
    }

    public function invoiceFee() {
        return $this->hasMany('App\Model\Invoice_fee');
    }

    public function invoiceFeesPayment() {
        return $this->hasManyThrough('App\Model\Invoice_fees_payment', 'App\Model\Invoice_fee');
    }
    public function getAmount() {
        return $this->invoiceFee()->sum('amount');
    }
    
    public function payment() {
         return $this->hasMany('App\Model\Payment');
    }

}
