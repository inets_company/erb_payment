<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Invoice_fee extends Model {

    protected $guarded = ['paymenttype'];

    public function invoice() {
        return $this->belongsTo('\App\Model\Invoice');
    }
    
     public function invoiceFeesPayment() {
        return $this->hasMany('App\Model\Invoice_fees_payment');
    }

    public function user() {
        return $this->belongsTo('\App\Model\User');
    }
}
