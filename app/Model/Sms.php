<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Sms extends Model {

    public $table = 'sms';
    protected $fillable = [
        'body', 'phone', 'user_id', 'status', 'type','return_code'
    ];

    public function user() {
        return $this->belongsTo('\App\Model\User')->withDefault(['name'=>'No name']);
    }

}
