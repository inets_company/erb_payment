@extends('layouts.app')

@section('content')
<!-- page start-->

<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                Email

            </header>

            <div class="panel-body">
                <section id="unseen">
                    <table class="table table-bordered table-striped table-condensed dataTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th class="numeric">Email</th>
                                <th class="numeric">Subject</th>
                                <th class="numeric">Message</th>

                                <th class="numeric">status</th>
                                <th class="numeric">Sent time</th>
                                <th class="numeric col-md-2">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            $barcodes=\App\Model\Payment::all();
                            ?>
                            @foreach($barcodes as $barcode)
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$barcode->invoice->user->name}}</td>
                                <td class="numeric">{{$barcode->invoice->user->email}}</td>
                                <td class="numeric">Event Barcode</td>
                                <td class="numeric">Attachment</td>
                                <td data-title="">
                                    <?=
                                    $barcode->receipt_sent == 1 ? '<b class="badge bg-success">sent</b>' :
                                            '<b class="badge bg-primary">pending</b>'
                                    ?>
                                </td>

                                <td data-title="">
                                    {{$barcode->created_at}}
                                </td>

                                <td class="numeric">
                                     <?php if (can_access('resend_emails')) { ?>
                                    <a href="#" id="resend<?= $barcode->id ?>" onclick="return false" onmousedown="resend('<?= $barcode->id ?>')" class="btn btn-xs btn-success">resend</a>
                                     <?php }?>
                         
                                </td>
                            </tr>
                            <?php $i++; ?>
                            @endforeach
                        </tbody>
                    </table>
                </section>
            </div>
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <form class="cmxform form-horizontal " id="commentForm" method="post" action="<?= url('user') ?>">

                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title">Add New User Type</h4>
                            </div>
                            <div class="modal-body">
                                <div class="panel-body">

                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">To (required)</label>
                                        <div class="col-lg-6">
                                            <select  class=" form-control" name="to" id="user_type_check">
                                                <option value="0">All</option> 
                                                <?php $user_types = \App\Model\User_type::all(); ?>
                                                @foreach ($user_types as $user_type)
                                                <option value="{{$user_type->id}}">{{$user_type->name}}</option>                                                  @endforeach;
                                                <option value="write">Custom Number</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group " id="phones" style="display: none">
                                        <label for="phones" class="control-label col-lg-3">Phones</label>
                                        <div class="col-lg-6">
                                            <input type="text" name="phone" class="form-control"/>
                                            <span>Write numbers separated by comma</span>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Template</label>
                                        <div class="col-lg-6">
                                            <select  class=" form-control" id="template" name="template">
                                                <option value=""></option> 
                                                <?php
                                                $templates = \App\Model\Sms_template::all();
                                                ?>
                                                @foreach($templates as $template)
                                                <option value="{{$template->id}}">{{$template->name}}</option>    @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Message (required)</label>
                                        <div class="col-lg-6">
                                            <textarea class=" form-control" id="message" name="message" minlength="2" type="text" required=""></textarea>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <?= csrf_field() ?>
                                <input type="hidden" name="created_by" value="<?= Auth::user()->id ?>"/>
                                <input type="hidden" name="user" value="sms"/>
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                <button class="btn btn-success" type="submit">Save changes</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
</div>
<!-- page end-->
<script type="text/javascript">
    user_type_check = function () {
        $('#user_type_check').change(function () {
            var type = $(this).val();
            if (type == 'write') {
                $('#phones').show();
            } else {
                $('#phones').hide();
            }
        });
    }
    function resend(a) {
        $.ajax({
            type: 'GET',
            url: "<?= url('inbox/resend_barcode') ?>",
            data: {id: a, type: 'email'},
            dataType: "html",
            success: function (data) {
                $('#resend' + a).html('sent');
                $('#resend' + a).attr('disabled', 'disabled');
            }
        });
    }
    template = function () {
        $('#template').change(function () {
            var template = $(this).val();
            $.ajax({
                type: 'GET',
                url: "<?= url('inbox/getTemplate') ?>",
                data: {template: template},
                dataType: "html",
                success: function (data) {
                    $('#message').html(data);
                }
            });
        });
    }
    $(document).ready(template);
    $(document).ready(user_type_check);
</script>
@endsection