@extends('layouts.app')

@section('content')
<!-- page start-->

<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                Schedule SMS and Emails

            </header>
             <?php if (can_access('add_sms')) { ?>
            <p><br/>&nbsp;&nbsp;&nbsp;<a class="btn btn-success" data-toggle="modal" href="#myModal">
                    Create Schedule
                </a></p>
             <?php }?>
            <?php $types = [1 => 'SMS', 2 => 'Email', 0 => 'Both SMS and Email'] ?>
            <div class="panel-body">
                <section id="unseen">
                    <table class="table table-bordered table-striped table-condensed dataTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th class="numeric">Type</th>
                                <th class="numeric">Template Name</th>

                                <th class="numeric">Days</th>
                                <th class="numeric">Sent time</th>
                                <th class="numeric">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            ?>
                            @foreach($schedules as $schedule)
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$schedule->name}}</td>
                                <td class="numeric">{{isset($types[$schedule->type]) ?$types[$schedule->type]:'SMS' }}</td>
                                <td class="numeric">{{$schedule->smsTemplate->name}}</td>
                                <td data-title="">
                                    <?= $schedule->days ?>
                                </td>

                                <td data-title="">
                                    {{date('H:i',strtotime($schedule->time))}} H
                                </td>
                                <td class="numeric">
                                     <?=can_access('delete_sms')? btn_delete('user/' . $schedule->id, 'schedule') :''?>
                         

                                </td>
                            </tr>
                            <?php $i++; ?>
                            @endforeach
                        </tbody>
                    </table>
                </section>
            </div>
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <form class="cmxform form-horizontal " id="commentForm" method="post" action="<?= url('user') ?>">

                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title">Add New Schedule</h4>
                            </div>
                            <div class="modal-body">
                                <div class="panel-body">
                                    <div class="form-group " id="phones">
                                        <label for="phones" class="control-label col-lg-3">Name</label>
                                        <div class="col-lg-6">
                                            <input type="text" name="name" class="form-control"/>
                                            <span>Write schedule name</span>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Template</label>
                                        <div class="col-lg-6">
                                            <select  class=" form-control" id="template" name="sms_template_id">
                                                <option value=""></option> 
                                                <?php
                                                $templates = \App\Model\Sms_template::all();
                                                ?>
                                                @foreach($templates as $template)
                                                <option value="{{$template->id}}">{{$template->name}}</option>    @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Type</label>
                                        <div class="col-lg-6">
                                            <select disabled=""  class=" form-control" id="template" name="type">

                                                <option value="1" selected="">SMS</option> 
                                                @foreach ($types as $key=>$type)
                                                <option value="{{$key}}">{{$type}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group " id="phones">
                                        <label for="phones" class="control-label col-lg-3">Days</label>
                                        <div class="col-lg-6">
                                            <select class="form-control select2_multiple select2" multiple="" name="days[]">
                                                <option value=""></option>                                     
                                                <?php $days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] ?>
                                                @foreach ($days as $day)
                                                <option value="{{$day}}">{{$day}}</option>                                                  @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group " id="phones">
                                        <label for="phones" class="control-label col-lg-3">Time</label>

                                        <div class="col-lg-6">
                                            <input type="time" name="time" class="form-control"/>
                                        </div>
                                    </div>


                                </div>

                            </div>
                            <div class="modal-footer">
                                <?= csrf_field() ?>
                                <input type="hidden" name="created_by" value="<?= Auth::user()->id ?>"/>
                                <input type="hidden" name="user" value="schedule"/>
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                <button class="btn btn-success" type="submit">Save changes</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
</div>
<!-- page end-->
@endsection