@extends('layouts.app')

@section('content')

<!-- page start-->
<link href="{{ asset('public/css/bootstrap-tagsinput.css')}}" rel="stylesheet">
<script src="{{ asset('public/js/bootstrap-tagsinput.js') }}"></script>
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                Templates
              
            </header>
              <?php if (can_access('add_sms')) { ?>
            <p><br/>&nbsp;&nbsp;&nbsp;<a class="btn btn-success" data-toggle="modal" href="#myModal">
                    Add  New Template
                </a></p>
              <?php }?>
            <div class="panel-body">
                <section id="unseen">
                    <table class="table table-bordered table-striped table-condensed dataTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th class="numeric">Message</th>
                                <th class="numeric">Phones</th>
                                <th class="numeric">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            ?>
                            @foreach($templates as $template)
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$template->name}}</td>
                                <td class="numeric">{{$template->message}}</td>
                                <td class="text">{{$template->phone_numbers}}</td>
                                <td class="numeric">
                                    <?= can_access('delete_sms')? btn_delete('user/' . $template->id, 'sms_template') :''?>
                                  
                                      <?php if (can_access('edit_sms')) { ?>
                                    <a data-toggle="modal" href="#myModal" onmousedown="open_edit_model('<?= $template->id ?>', '<?= url('setting/' . $template->id . '/edit') ?>')" class="btn btn-xs btn-info">Edit</a>  
                                      <?php }?>
                                </td>
                            </tr>
                            <?php $i++; ?>
                            @endforeach
                        </tbody>
                    </table>
                </section>
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <form class="cmxform form-horizontal " id="commentForm" method="post" action="<?= url('user') ?>">

                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title">Add New User Type</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="panel-body">

                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-3">Name (required)</label>
                                            <div class="col-lg-6">
                                                <input class=" form-control" id="name" name="name" minlength="2" type="text" required="">
                                            </div>
                                        </div>


                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-3">Message (required)</label>
                                            <div class="col-lg-6">
                                                <textarea class=" form-control" id="message" name="message" minlength="2" type="text" required=""></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-3">Phone (s)</label>
                                            <div class="col-lg-6">
                                                <input class="form-control" id="phone_numbers" value="" name="phone_numbers" type="text" required="" >

                                            </div>
                                            <span class="hints">Separate by comma for more than one</span>

                                        </div>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <?= csrf_field() ?>
                                    <input type="hidden" name="created_by" value="<?= Auth::user()->id ?>"/>
                                    <input type="hidden" name="user" value="sms_template"/>
                                    <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                    <button class="btn btn-success" type="submit">Save changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<!-- page end-->
<style>
    .tag{color:blue; background: green}
</style>
<script type="text/javascript">
  
    open_edit_model = function (a, b) {
        $.ajax({
            type: 'POST',
            url: "<?= url('setting/getedit') ?>",
            data: {
                "id": a,
                "table": "sms_template"
            },
            dataType: "json",
            success: function (data) {
                $('#title_page').html('Edit Template');
                $('#commentForm').attr('action', b);
                $("#commentForm").attr("method", "get");
                $.each(data, function (i, item) {
                 
                    $('#' + i).val(item);
                });
            }
        });
    }
    reset_form = function () {
        $('#title_page').html('Add New Template');
        $('#commentForm').attr('action', '<?= url('user') ?>');
        $("#commentForm").attr("method", "post");
        $("input:not(:hidden)").val('');
        $('.delete').html('Delete');
    }
</script>
@endsection