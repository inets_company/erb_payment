<!DOCTYPE html>
<html>
    <head>
        <title>ERB Barcode Ticket</title>
        <link href='https://fonts.googleapis.com/css?family=Lobster|Kreon:400,700' rel='stylesheet' type='text/css'>

        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    </head>

    <style>
<?php if (isset($padding_ticket)) { ?>
            body {
                margin: 0;
                color: #494140;
                font-weight: 400;
                font-size: 15px;
            }

            .container {
                width: 780px;
                margin: 0 auto;
            }
            .table_ticket{text-align: center;}
            table tr,th,td{text-align: center !important;}
<?php } ?>
        .verticalTableHeader {
            text-align:center;
            transform: rotate(-90deg);

        }
        .verticalTableHeader p {
            display:inline-block;
        }
        .verticalTableHeader p:before{
            vertical-align:middle;
        }
      
    </style>

    <body style="font-family:'Open Sans',sans-serif; min-height: 200px;" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">
        <div class="container">
            <table class="table_ticket" height="100%" cellspacing="0" cellpadding="0" border="1"  bordercolor="#ccccc"  style="text-align: center; margin-right: 5%; border-color: #cccccc"  >
                <thead>

                </thead>
                <tbody>
                    <tr>
                        <td width="20%">
                            <img class="verticalTableHeader" src="data:image/png;base64,<?= $barcode ?>" />
                        </td>
                        <td width="60%"> <table class="table-td-wrap"  width="100%" height="100%" cellspacing="0" cellpadding="0" border="1" bordercolor="#ccccc" style="text-align: center; border-color: #cccccc" >
                                <thead>
                                    <tr>
                                        <th>
                                            <p>Event Barcode:
                                            <h1>
                                                <?= $event->name ?>
                                            </h1>
                                            </p>

                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><b>Date:</b>
                                            <?= $event->description ?>
                                            <?= $event->location ?>
                                            <p><b>Theme: </b>
                                                <?= $event->theme ?>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Order Information</b>
                                            <?php
                                            if(isset($payment) && count($payment)){ ?>
                                            <p>Paid Amount: Tsh <?=$payment->amount?>/=, Reference Number: <?=$payment->invoice->number?>, Payment Date: <?=date('Y M d', strtotime($payment->created_at))?>, by: <?=$payment->method?></p>   
                                          <?php   }
                                            ?>
                                         
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td width="20%">
                            <table  width="100%" height="100%" cellspacing="0" cellpadding="0" style="text-align: center; border-color: #cccccc" >
                                <thead>
                                    <tr>
                                        <th>    <br/>  <img src="<?= url('public/images/erb.png') ?>" height="80" width="80" align="center">
                                            <br/>
                                            <p><?= $user->name ?></p>
                                            <b>{{$user->employer->name}}</b></th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <tr>
                                        <td><br/>
                                            Status: <b style="color:red">PAID</b></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
            <section>
                <h2>Barcode Notes / Instruction</h2> 
                <div>
                    Please come with this Barcode ticket on the event.
                </div>
            </section>
        </div>

    </body>

</html>
