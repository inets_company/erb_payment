<tbody><tr>
        <td colspan="2" style="background:#fff;border-radius:8px">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody><tr>
                        <td style="font-family:'Helvetica Neue',Helvetica,Arial,sans-serif">

                        </td></tr><tr>
                        <td class="m_-6472649085327934178grid__col" style="font-family:'Benton Sans',-apple-system,BlinkMacSystemFont,Roboto,'Helvetica neue',Helvetica,Tahoma,Arial,sans-serif;padding:32px 40px;border-radius:6px 6px 0 0" align="">



                            <h2 style="color:#404040;font-weight:300;margin:0 0 12px 0;font-size:24px;line-height:30px;font-family:'Benton Sans',-apple-system,BlinkMacSystemFont,Roboto,'Helvetica neue',Helvetica,Tahoma,Arial,sans-serif">

                                This is your order confirmation for <a href="http://engineersday.co.tz" style="text-decoration:none;color:#1090ba;font-weight:normal" target="_blank">Annual Engineers Day 2018</a>

                            </h2>






                            <div style="color:#666666;font-weight:400;font-size:15px;line-height:21px;font-family:'Benton Sans',-apple-system,BlinkMacSystemFont,Roboto,'Helvetica neue',Helvetica,Tahoma,Arial,sans-serif;font-weight:300" class="m_-6472649085327934178organized_by">


                                Organized by <a href="http://erb.go.tz" style="color:#999" target="_blank">Engineers Registration Board</a>


                            </div>





                        </td>
                    </tr>

                    <tr>
                        <td style="padding:0 40px">

                            <table cellspacing="0" cellpadding="0" width="130%" style="width:136%;min-width:100%">
                                <tbody><tr>
                                        <td style="background-color:#dedede;width:120%;min-width:100%;font-size:1px;height:1px;line-height:1px">&nbsp;
                                        </td>
                                    </tr>
                                </tbody></table>

                        </td>
                    </tr>


                    <tr>
                        <td class="m_-6472649085327934178grid__col" style="font-family:'Benton Sans',-apple-system,BlinkMacSystemFont,Roboto,'Helvetica neue',Helvetica,Tahoma,Arial,sans-serif;padding:28px 30px" align="">





                            <h2 style="color:#404040;font-weight:300;margin:0 0 12px 0;font-size:24px;line-height:30px;font-family:'Benton Sans',-apple-system,BlinkMacSystemFont,Roboto,'Helvetica neue',Helvetica,Tahoma,Arial,sans-serif;margin-bottom:18px">

                                Here is your ticket

                            </h2>


                            <?php
                           
                            $barcode = (new \App\Http\Controllers\SettingController())->createBarCode($id);
                            $user = \App\Model\User::find($id);
                            ?>
                            @include('email.ticket_attachment')



                            <table cellpadding="0" cellspacing="0" border="0" align="left" style="width:215px;text-align:center;overflow:hidden" class="m_-6472649085327934178small_full_width">
                                <tbody><tr>
                                        <td>
                                            <table cellspacing="0" cellpadding="0" width="100%" align="">


                                                <tbody><tr>
                                                        <td width="" height="" style="text-align:left;padding-left:2px" align="" valign="" colspan="1">
                                                            <table width="100%" cellspacing="0" cellpadding="0">
                                                                <tbody><tr width="100%">
                                                                        <td width="100%" style="overflow:hidden;float:left;display:none;font-size:0;height:0;line-height:0;max-height:0;width:0;margin:0;padding:0" class="m_-6472649085327934178passbook">


                                                                            <table cellspacing="0" cellpadding="0" width="" align="" class="m_-6472649085327934178no_text_resize">


                                                                                <tbody><tr>


                                                                                        <td width="22" height="22" align="" valign="" colspan="1">



                                                                                        </td>


                                                                                        <td width="" height="" align="" valign="" colspan="1">



                                                                                        </td>


                                                                                    </tr>


                                                                                </tbody></table>


                                                                        </td>
                                                                    </tr>
                                                                </tbody></table>


                                                        </td>


                                                    </tr>


                                                </tbody></table>


                                        </td>
                                    </tr>
                                </tbody></table>


                            <table cellpadding="0" cellspacing="0" border="0" align="right" style="width:215px;text-align:center;overflow:hidden" class="m_-6472649085327934178small_full_width">
                                <tbody><tr>
                                        <td>





                                            <div style="color:#666666;font-weight:400;font-size:15px;line-height:21px;font-family:'Benton Sans',-apple-system,BlinkMacSystemFont,Roboto,'Helvetica neue',Helvetica,Tahoma,Arial,sans-serif;margin-top:0;margin-bottom:0; float:right;">


                                                <a style="text-decoration:none;color:#0f90ba;font-family:'Benton Sans',-apple-system,BlinkMacSystemFont,Roboto,'Helvetica neue',Helvetica,Tahoma,Arial,sans-serif" href="#99footer">download attachment below</a>

                                            </div>


                                        </td>
                                    </tr>
                                </tbody></table>




                        </td>
                    </tr>




                    <tr>
                        <td style="padding:0 0px">

                            <table cellspacing="0" cellpadding="0" width="150%" style="width:126%; min-width:100%">
                                <tbody><tr>
                                        <td style="background-color:#dedede;width:150%;min-width:100%;font-size:1px;height:1px;line-height:1px">&nbsp;
                                        </td>
                                    </tr>
                                </tbody></table>

                        </td>
                    </tr>

                    <tr width='120%'>
                        <td class="m_-6472649085327934178grid__col" style="font-family:'Benton Sans',-apple-system,BlinkMacSystemFont,Roboto,'Helvetica neue',Helvetica,Tahoma,Arial,sans-serif;padding:32px 40px;border-radius:0 0 6px 6px" align="left">



                            <table cellpadding="0" cellspacing="0" border="0" align="left" style="width:100%;line-height:1.67;font-size:13px" class="m_-6472649085327934178small_full_width">
                                <tbody><tr>
                                        <td>



                                            <h2 style="color:#404040;font-weight:300;margin:0 0 12px 0;font-size:24px;line-height:30px;font-family:'Benton Sans',-apple-system,BlinkMacSystemFont,Roboto,'Helvetica neue',Helvetica,Tahoma,Arial,sans-serif">
                                                About this event
                                            </h2>


                                            <table cellspacing="0" cellpadding="0" width="140%" align="">



                                                <tbody>
                                                    <tr>


                                                        <td width="30" height="" style="vertical-align:top;padding-right:10px" align="" valign="" colspan="1">



                                                            <img src="https://ci5.googleusercontent.com/proxy/K5NOKd2qRftFOiSFcKbzG2xIEoT5TsuEhqxZYpsPovzNv3ftZUWB5HHbrXY3_H8siBt0HFXxBev2YpiWh2yQpTzHuHF2GEi_JnCi2AwuMB8Fe65DQGb14F8c9B62BEa86Xil1zU2sPDX=s0-d-e1-ft#https://cdn.evbstatic.com/s3-s3/marketing/emails/order_confirmation/date-iconx2.png" title="date" alt="date" style="width:20px;height:20px;vertical-align:-2px" border="0" width="20" height="20" class="CToWUd">



                                                        </td>


                                                        <td width="" height="" align="" valign="" colspan="1">



                                                            <div style="color:#666666;font-weight:400;font-size:15px;line-height:21px;font-family:'Benton Sans',-apple-system,BlinkMacSystemFont,Roboto,'Helvetica neue',Helvetica,Tahoma,Arial,sans-serif">
                                                                <?= $event->description ?>

                                                            </div>


                                                        </td>


                                                    </tr>




                                                    <tr>


                                                        <td width="30" height="" style="vertical-align:top;padding-right:10px" align="" valign="" colspan="1">



                                                            <img src="https://ci6.googleusercontent.com/proxy/0EyFIZsXl2MqZQQ6aFvuUJTNcprk97pCJebIvMsVIElYkmuBmk9PRurQ4lOmde9CUGy42OT7YGXF3MiKkQ2YswIbCn83g7Iv_c9WOgsIqXOdtkPOxFNWhNQDAVR0lcjQaYTiSwysz2Mn2SFvYXphsWo=s0-d-e1-ft#https://cdn.evbstatic.com/s3-s3/marketing/emails/order_confirmation/location-pin-iconx2.png" title="date" alt="date" style="width:20px;height:20px;vertical-align:-4px" border="0" width="20" height="20" class="CToWUd">



                                                        </td>


                                                        <td width="" height="" style="padding-bottom:10px" align="" valign="" colspan="1">

                                                            <?= $event->location ?>
                                                            <br/>
                                                            <br/>
                                                            <p><b>Theme: </b>
                                                                <?= $event->theme ?>
                                                            </p>
                                                        </td>


                                                    </tr>




                                                    <tr class="m_-6472649085327934178hide-for-small">


                                                        <td width="30" height="" style="vertical-align:top;padding-right:10px;padding-top:10px" align="" valign="" class="m_-6472649085327934178hide-for-small" colspan="1">



                                                            <img src="https://ci6.googleusercontent.com/proxy/mxXBYATunu3EOSIMILty3zgVnkAdbkhR1uBIk4u59BqOpUc7VIemDqDtoNvbUr89m2Qj8t_Uyx_FFfGoFiVEzxOr-WI0-M3gA-Z2llNdJEN1ykRHw7u9d3UJd4zS8Q_PSOE225Tlj9-_GRwsSemEpd4=s0-d-e1-ft#https://cdn.evbstatic.com/s3-s3/marketing/emails/order_confirmation/add-calendar-iconx2.png" title="date" alt="date" style="width:20px;height:20px;vertical-align:-2px" border="0" width="20" height="20" class="CToWUd">



                                                        </td>


                                                        <td width="" height="" style="padding-top:10px" align="" valign="" class="m_-6472649085327934178hide-for-small" colspan="1">



                                                            <div style="color:#666666;font-weight:400;font-size:15px;line-height:21px;font-family:'Benton Sans',-apple-system,BlinkMacSystemFont,Roboto,'Helvetica neue',Helvetica,Tahoma,Arial,sans-serif">

                                                                Add to my calendar:

                                                            </div>



                                                            <div style="color:#666666;font-weight:400;font-size:15px;line-height:21px;font-family:'Benton Sans',-apple-system,BlinkMacSystemFont,Roboto,'Helvetica neue',Helvetica,Tahoma,Arial,sans-serif;margin-top:6px;margin-bottom:0" class="m_-6472649085327934178no_text_resize">
                                                                <?php
                                                                $day = $event->date;

// add 1 days to the date above
                                                                $NewDate = date('Y-m-d', strtotime($day . " +1 days"));
                                                                ?>
                                                                <div>


                                                                    <a style="text-decoration:none;color:#0f90ba;font-family:'Benton Sans',-apple-system,BlinkMacSystemFont,Roboto,'Helvetica neue',Helvetica,Tahoma,Arial,sans-serif"  href="https://www.google.com/calendar/render?action=TEMPLATE&text=<?= $event->name ?>&dates=<?= date('Ymd', strtotime($event->date)) ?>T224000Z/<?= date('Ymd', strtotime($NewDate)) ?>T221500Z&details=<?= $event->theme ?>.. For+more+details,+click+here:+http://engineersday.co.tz&location=<?= $event->location ?>&sf=true&output=xml">Google</a>


                                                                </div>


                                                        </td>


                                                    </tr>


                                                </tbody>
                                            </table>


                                        </td>

                                    </tr>

                                </tbody>
                            </table>


                        </td>
        
                    </tr>
                </tbody></table>
            <p id="99footer"></p>