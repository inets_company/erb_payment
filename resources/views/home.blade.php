@extends('layouts.app')

@section('content')

<?php if (can_access('view_dashboard')) { ?>
<!--main content start-->
<!--mini statistics start-->
<div class="row">
    <div class="col-md-3">
        <div class="mini-stat clearfix">
            <span class="mini-stat-icon orange"><i class="fa fa-users"></i></span>
            <div class="mini-stat-info">
                <span><?= $total_users ?></span>
                Total Users
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="mini-stat clearfix">
            <span class="mini-stat-icon tar"><i class="fa fa-tag"></i></span>
            <div class="mini-stat-info">
                <span><?= $organizations ?></span>
                Organizations
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="mini-stat clearfix">
            <span class="mini-stat-icon pink"><i class="fa fa-money"></i></span>
            <div class="mini-stat-info">
                <span><?= $applicants ?></span>
                Applicants 
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="mini-stat clearfix">
            <span class="mini-stat-icon green"><i class="fa fa-users"></i></span>
            <div class="mini-stat-info">
                <span><?= $total_users - $applicants ?></span>
                ERB staff
            </div>
        </div>
    </div>
</div>
<!--mini statistics end-->

<!--mini statistics start-->
<div class="row">
    <div class="col-md-3">
        <section class="panel">
            <div class="panel-body">
                <div class="top-stats-panel">
                    <div class="daily-visit">
                        <h4 class="widget-h">Total Invoices</h4>
                        <div id="daily-visit-chart" style="width:100%; height: 100px; display: block">
                            <br/><br/>
                        {{$total_invoices}}
                    </div>
                       <ul class="chart-meta clearfix">
                            <li class="pull-left visit-chart-value"></li>
                            <li class="pull-right visit-chart-title"><i class="fa fa-arrow-up"></i><a href="{{url('invoice')}}">View</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="col-md-3">
        <section class="panel">
            <div class="panel-body">
                <div class="top-stats-panel">
                    <div class="daily-visit">
                        <h4 class="widget-h">
                            Amount to be Collected</h4>
                        <div id="daily-visit-chart" style="width:100%; height: 100px; display: block">
                            <br/><br/>
                          Tsh  {{number_format($amount_tobe_collected)}}
                        </div>
                        <ul class="chart-meta clearfix">
                            <li class="pull-left visit-chart-value"></li>
                            <li class="pull-right visit-chart-title"><i class="fa fa-arrow-up"></i><a href="{{url('invoice')}}">View</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="col-md-3">
        <section class="panel">
            <div class="panel-body">
                <div class="top-stats-panel">
                    <div class="daily-visit">
                    <h4 class="widget-h">Amount Collected</h4>
                    <div id="daily-visit-chart" style="width:100%; height: 100px; display: block">
                        <br/><br/>
                       Tsh {{number_format($total_money_collected)}}
                    </div>
                    <ul class="chart-meta clearfix">
                        <li class="pull-left visit-chart-value"></li>
                        <li class="pull-right visit-chart-title"><i class="fa fa-arrow-up"></i><a href="{{url('payment')}}">View</a></li>
                    </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="col-md-3">
        <section class="panel">
            <div class="panel-body">
                <div class="top-stats-panel">
                    <h4 class="widget-h">Pending & Tasks</h4>
                    <div class="bar-stats">
                        <h4>Pending</h4>
                        <ul class="bar-legend">
                            <li><span class="bar-legend-pointer pink"></span> SMS : {{$sms_pending}}</li>
                            <li><span class="bar-legend-pointer green"></span> Emails : {{$email_pending}}</li>
                       </ul>
                        <br/>
                        <div class="daily-visit">
                        <div class="daily-sales-info">
                            <span class="sales-count">{{$sms-$sms_pending}} </span> 
                            <span class="sales-label">SMS Sent,</span>
                            <span class="sales-label"><span class="sales-count">
                                    {{isset($sms_status->sms_remain) ?$sms_status->sms_remain:''}}</span> SMS Remains</span>
                        </div>
                            <br/>
                            <div class="daily-sales-info">
                            <span class="sales-count">{{$email-$email_pending}} </span> 
                            <span class="sales-label">Emails Sent</span>
                        </div>
                       </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<!--right sidebar end-->
<?php }?>
@endsection
