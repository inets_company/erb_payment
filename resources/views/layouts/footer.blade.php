<!--<footer class="footer-section">
    <div class="text-center">
<?= date('Y') ?> © <a href="http://inetstz.com" style="color:white" target="_blank">Inets Co LTD</a>
        <a href="#" class="go-top">
            <i class="fa fa-angle-up"></i>
        </a>
    </div>
</footer>-->
<!--Core js-->


<script src="{{ asset('public/js/jquery-ui/jquery-ui-1.10.1.custom.min.js') }}"></script>

<script src="{{ asset('public/js/jquery.dcjqaccordion.2.7.js') }}"></script>
<script src="{{ asset('public/js/jquery.scrollTo.min.js') }}"></script>
<script src="{{ asset('public/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('public/js/jquery.nicescroll.js') }}"></script>
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="{{ asset('public/js/flot-chart/excanvas.min.js') }}"></script><![endif]-->
<script src="{{ asset('public/js/skycons/skycons.js') }}"></script>
<script src="{{ asset('public/js/jquery.scrollTo/jquery.scrollTo.js') }}"></script>
<script src="{{ asset('public/js/easing.js') }}"></script>
<script src="{{ asset('public/js/underscore.js') }}"></script>
<script src="{{ asset('public/select2/select2.js') }}"></script>
<script type="text/javascript">
$(".select2").select2({placeholder: "Select", maximumSelectionSize: 30});
$("button[data-select2-open]").click(function () {
    $("#" + $(this).data("select2-open")).select2("open");
});

</script>
<!-- Jquery datatable js -->
<script type="text/javascript" src="{{ asset('public/datatables/jquery.dataTables.min.js')}}"></script>
<!-- Datatable js -->
<script type="text/javascript" src="{{ asset('public/datatables/dataTables.bootstrap.js')}}"></script>

<?php if (isset($landing)) { ?>
    <script src="{{ asset('public/js/jvector-map/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <script src="{{ asset('public/js/jvector-map/jquery-jvectormap-us-lcc-en.js') }}"></script>
    <!--clock init-->
    <script src="{{ asset('public/js/css3clock/js/css3clock.js') }}"></script>
    <!--Easy Pie Chart-->
    <script src="{{ asset('public/js/easypiechart/jquery.easypiechart.js') }}"></script>
    <!--Sparkline Chart-->
    <script src="{{ asset('public/js/sparkline/jquery.sparkline.js') }}"></script>

<?php } ?>

<script src="{{ asset('public/js/jquery.customSelect.min.js') }}" ></script>
<!--common script init for all pages-->
<script src="{{ asset('public/js/scripts.js') }}"></script>
<!--Jquery Advanced Datatables-->
<script type="text/javascript" src="{{ asset('public/datatables/dataTables.buttons.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/datatables/buttons.print.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/datatables/buttons.flash.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/datatables/jszip.min.js')}}"></script>

<script type="text/javascript" src="{{ asset('public/datatables/pdfmake.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/datatables/vfs_fonts.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/datatables/buttons.html5.min.js')}}"></script>


<link href="{{ asset('public/datatables/buttons.dataTables.min.css')}}" rel="stylesheet">
<script type="text/javascript">

$(document).ready(function () {
    $('.dataTable').DataTable({
        dom: 'Bfrtip',
        responsive: true,
        buttons: [
            {
                text: 'PDF',
                extend: 'pdfHtml5',
                message: '',
                orientation: 'landscape',
                exportOptions: {
                    columns: ':visible'
                },
                customize: function (doc) {
                    doc.pageMargins = [10, 10, 10, 10];
                    doc.defaultStyle.fontSize = 7;
                    doc.styles.tableHeader.fontSize = 7;
                    doc.styles.title.fontSize = 9;
                    // Remove spaces around page title
                    doc.content[0].text = doc.content[0].text.trim();
                    // Create a footer
                    doc['footer'] = (function (page, pages) {
                        return {
                            columns: [
                                'www.shulesoft.com',
                                {
                                    // This is the right column
                                    alignment: 'right',
                                    text: ['page ', {text: page.toString()}, ' of ', {text: pages.toString()}]
                                }
                            ],
                            margin: [10, 0]
                        }
                    });
                    // Styling the table: create style object
                    var objLayout = {};
                    // Horizontal line thickness
                    objLayout['hLineWidth'] = function (i) {
                        return .5;
                    };
                    // Vertikal line thickness
                    objLayout['vLineWidth'] = function (i) {
                        return .5;
                    };
                    // Horizontal line color
                    objLayout['hLineColor'] = function (i) {
                        return '#aaa';
                    };
                    // Vertical line color
                    objLayout['vLineColor'] = function (i) {
                        return '#aaa';
                    };
                    // Left padding of the cell
                    objLayout['paddingLeft'] = function (i) {
                        return 4;
                    };
                    // Right padding of the cell
                    objLayout['paddingRight'] = function (i) {
                        return 4;
                    };
                    // Inject the object in the document
                    doc.content[1].layout = objLayout;
                }
            },
            {extend: 'copyHtml5', footer: true},
            {extend: 'excelHtml5', footer: true},
            {extend: 'csvHtml5', customize: function (csv) {
                    return "ShuleSoft" + csv + "ShuleSoft";
                }},
            {extend: 'print', footer: true}

        ]
    });
});


</script>
