@extends('layouts.app')

@section('content')
<div class="col-sm-12">
    <section class="panel">
        <header class="panel-heading">
            Search Results
        </header>
        <div class="panel-body">
            <div class="col-sm-12">
                <div class="row"  id="search_checked_table" style="display:none">
<!--                    <a type="button" target="_blank" tags='' id="nametag_link" href="" value="" name="" class="btn btn-sm btn-danger"><i class="fa fa-cloud"></i> Print NameTag</a>-->
                    
                     <a type="button" target="_blank" tags='' id="nametag_link" href="" value="" name="" class="btn btn-sm btn-danger link"><i class="fa fa-cloud"></i> Print NameTag (HP Printer)</a>
                    <a type="button" target="_blank" tags='' id="nametag_link2" href="" value="" name="" class="btn btn-sm btn-primary link"><i class="fa fa-cloud"></i> Print single NameTag (EPSON Printer) </a>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Number</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr  id="search_checked"></tr>
                        </tbody>
                    </table>
                </div>
                <section class="panel">
                    <header class="panel-heading">
                        Users
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-cog"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <table class="table table-striped dataTable">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Employer</th>
                                    <th>Type</th>
                                     <th class="numeric col-sm-2">Print Count</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($users as $user) {
                                    ?>
                                    <tr  id="row<?= $user->id ?>">
                                        <td>{{$i}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->phone}}</td>
                                        <td>{{$user->employer->name}}</td>
                                        <td>{{$user->userType->name}}</td>
                                         <td>{{$user->nametagPrintlog()->count()}}</td>
                                        <td>                      <a href="<?= url('user/profile/' . $user->id) ?>" class="btn btn-xs btn-success">View</a>
                                            
                                    <?php if ($user->is_employer <> 1 && $user->payment()->count() > 0) { ?>
                                        <a href="<?= url('user/ticket/' . $user->id) ?>" class="btn btn-xs btn-warning">Barcode</a> &nbsp; &nbsp;
                                                          <a href="<?= url('user/nametag/' . $user->id) ?>" class="btn btn-xs btn-default">Tag</a>
 <?php } ?>
                      
                                         <?php if (can_access('edit_users')) { ?>
                                        <a data-toggle="modal" href="#myModal" onmousedown="open_edit_model('<?= $user->id ?>', '<?= url('setting/' . $user->id . '/edit') ?>')" class="btn btn-xs btn-info">Edit</a> 
                                    <?php } ?>
                                          <?= can_access('delete_users') ? btn_delete('user/' . $user->id, 'user') : '' ?>
                                   
                                            &nbsp; <input type="checkbox" class="check" name="select[]" value="<?= $user->id ?>"/>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </section>
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <form class="cmxform form-horizontal " id="commentForm" method="post" action="<?= url('invoice') ?>">

                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="title_page">Add New User</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="panel-body">
                                        <div class="form-group ">
                                            <label for="type" class="control-label col-lg-3">Employer</label>
                                            <div class="col-lg-6">
                                                <select class="form-control"  name="employer_id" id="employer_id">
                                                    <option value=""></option> 
                                                    <?php $userype = \App\Model\Employer::orderBy('name')->get() ?>
                                                    @foreach ($userype as $type)
                                                    <option value="{{$type->id}}">{{$type->name}}</option>                                                  @endforeach;
                                                </select>

                                            </div>
                                            <?php echo form_error($errors, 'employer_id'); ?>
                                        </div>
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-3">Name (required)</label>
                                            <div class="col-lg-6">
                                                <input class=" form-control" id="name" name="name" minlength="2" type="text" required="" value="{{old('name')}}" pattern="[a-zA-Z\. ]{5,}"  onblur="this.value = this.value.toUpperCase()">
                                            </div>
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group ">
                                            <label for="cemail" class="control-label col-lg-3">E-Mail (required)</label>
                                            <div class="col-lg-6">
                                                <input class="form-control " id="email" type="email" name="email" required=""  value="<?= old('email') ?>"  onblur="this.value = this.value.toLowerCase()">
                                            </div>
                                            @if ($errors->has('email'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group ">
                                            <label for="phone" class="control-label col-lg-3">Phone Number</label>
                                            <div class="col-lg-6">
                                                <input class="form-control " id="phone" type="text" name="phone"  value="<?= old('phone') ?>">
                                            </div>
                                            @if ($errors->has('phone'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group ">
                                            <label for="number" class="control-label col-lg-3">Specialization (Required)</label>
                                            <div class="col-lg-6">
                                                <select class="form-control" name="profession_id" id="profession_id">
                                                    <option value=""></option>                                     
                                                    <?php $professions = App\Model\Profession::where('invitee', 0)->orderBy('name')->get(); ?>
                                                    @foreach ($professions as $profession)
                                                    <option value="{{$profession->id}}">{{$profession->name}}</option>                                                  @endforeach;
                                                </select>

                                            </div>
                                            <?php echo form_error($errors, 'profession_id'); ?>
                                            <!--<a data-toggle="modal" href="#myModal">Or Add new</a>-->
                                        </div>




                                    </div>


                                </div>
                                <div class="modal-footer">
                                    <?= csrf_field() ?>
                                    <input type="hidden" name="user" value="user"/>
                                    <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                    <button class="btn btn-success" type="submit">Save changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <br/><hr/>
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        Invoices
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-cog"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Number</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($invoices as $invoice) {
                                    ?>
                                    <tr>
                                        <td>{{$i}}</td>
                                        <th>{{$invoice->number}}</th>
                                        <td>{{$invoice->user->name}}</td>
                                        <td>{{$invoice->user->email}}</td>
                                        <td>{{$invoice->user->phone}}</td>
                                        <td>                      <a href="<?= url('invoice/' . $invoice->id) ?>" class="btn btn-xs btn-success">View</a></td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </section>
            </div>

            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        Employers
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-cog"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Abbreviation</th>
                                    <th>Name</th>
                                    <th>Location</th>
                                    <th>Tin</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($employers as $employer) {
                                    ?>
                                    <tr>
                                        <td>{{$i}}</td>
                                        <th>{{$employer->abbreviation}}</th>
                                        <td>{{$employer->name}}</td>
                                        <td>{{$employer->location}}</td>
                                        <td>{{$employer->tin}}</td>
                                        <td>  </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </section>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    
    search_checked = function () {
        $('.check').click(function () {
            var value = $(this).val();
            var status = $(this).is(':checked');
            if (status === true) {
                var text = $('#row' + value).html();
                $('#search_checked_table').show();
                $('#search_checked').after('<tr id="s_table' + value + '">' + text + '</tr>');
                var ex = $('.link').attr('tags');
                var url = '<?= url('user/bulknametag?ids=') ?>';
                var url2 = '<?= url('user/bulknametag?single=1&ids=') ?>';
                var param = ex.split(",");
                param.push(value);
                $('#nametag_link').attr('tags', param.join(","));
                $('#nametag_link').attr('href', url + param.join(","));

                $('#nametag_link2').attr('tags', param.join(","));
                $('#nametag_link2').attr('href', url2 + param.join(","));
                console.log(param);

            } else {
                var ex = $('.link').attr('tags');
                var url = '<?= url('user/bulknametag?ids=') ?>';
                var url2 = '<?= url('user/bulknametag?single=1&ids=') ?>';
                var param = ex.split(",");
                param = jQuery.grep(param, function (val) {
                    return val != value;
                });
                var arr = param;

                var result = arr.filter(function (elem) {
                    return elem != value;
                });
                console.log(result);

                $('#nametag_link').attr('tags', result.join(","));
                $('#nametag_link').attr('href', url + result.join(","));


                $('#nametag_link2').attr('tags', param.join(","));
                $('#nametag_link2').attr('href', url2 + param.join(","));

                $('#s_table' + value).remove();
            }
        });
    }
      open_edit_model = function (a, b) {
        $.ajax({
            type: 'POST',
            url: "<?= url('setting/getedit') ?>",
            data: {
                "id": a,
                "table": "user"
            },
            dataType: "json",
            success: function (data) {
                $('#title_page').html('Edit user type');
                $('#commentForm').attr('action', b);
                $("#commentForm").attr("method", "get");
                $.each(data, function (i, item) {
                    $('#' + i).val(item);
                });
            }
        });
    }
    reset_form = function () {
        $('#title_page').html('Add New User Type');
        $('#commentForm').attr('action', '<?= url('user') ?>');
        $("#commentForm").attr("method", "post");
        $("input:not(:hidden)").val('');
        $('.delete').html('Delete');
    }
    $(document).ready(search_checked);
</script>
@endsection