<aside>
    <div id="sidebar" class="nav-collapse">
        <!-- sidebar menu start-->

        <div class="leftside-navigation">
            <ul class="sidebar-menu" id="nav-accordion">
                <li>
                    <a class="active" href="{{url('/')}}">
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <?php if (can_access('view_invoices')) { ?>
                    <li class="sub-menu">
                        <a href="javascript:;"  class="<?= is_active('invoice') ?>">
                            <i class="fa fa-book"></i>
                            <span>Invoices</span>
                        </a>
                        <ul class="sub">
                            <li><a href="{{url('invoice')}}">User Invoices</a></li>
                            <li><a href="{{url('invoice/bulk')}}">Sponsored Invoices</a></li>

                        </ul>
                    </li>
                <?php } ?>


                <?php if (can_access('view_payments')) { ?>
                    <li class="sub-menu">
                        <a href="javascript:;"  class="<?= is_active('payment') ?>">
                            <i class="fa fa-th"></i>
                            <span>Payments</span>
                        </a>
                        <ul class="sub">
                            <!--<li><a href="{{url('payment/api')}}">API Requests</a></li>-->
                            <li><a href="{{url('payment')}}">Payments</a></li>
                            <!--<li><a href="{{url('payment/summary')}}">Summary</a></li>-->
                            <li><a href="{{url('payment/receipts')}}">Receipts</a></li>
                        </ul>
                    </li>
                <?php } ?>

                <?php if (can_access('view_users')) { ?>
                    <li class="sub-menu">
                        <a href="javascript:;"  class="<?= is_active('applicants') ?>">
                            <i class="fa fa-tasks"></i>
                            <span>Applicants</span>
                        </a>
                        <ul class="sub">
                            <li><a href="{{url('user/applicants?user_type=9')}}">Non Sponsored</a></li>
                            <li><a href="{{url('user/applicants?user_type=7')}}">Sponsored</a></li>
                            <li><a href="{{url('user/applicants?user_type=120')}}">Employers</a></li>
                            <li><a href="{{url('user/invite')}}">Invitee</a></li>
                        </ul>
                    </li>
                <?php } ?>

                <?php if (can_access('view_users')) {
                    $has_type=in_array('applicants', explode('/', url()->current())) && in_array('user', explode('/', url()->current())) ?'none':'user';
                    ?>
                    <li class="sub-menu">
                        <a href="javascript:;"  class="<?= is_active($has_type) ?>">
                            <i class="fa fa-tasks"></i>
                            <span>Users</span>
                        </a>
                        <ul class="sub">
                            <li><a href="{{url('user/staff')}}">ERB Staff</a></li>
                            <!--<li><a href="{{url('user/applicants')}}">Applicants</a></li>-->
                            <li><a href="{{url('user/applicants?paid=1')}}">Paid Applicants</a></li>
                            <li><a href="{{url('user/entity')}}">Financial Entity</a></li>
                            <li><a href="{{url('user/organizations')}}">Organizations</a></li>
                            <li><a href="{{url('certificate')}}">Certificate</a></li>
                            <li><a href="{{url('user/attendance')}}">Attendance</a></li>
                        </ul>
                    </li>
                <?php } ?>

                <?php if (can_access('view_emails') || can_access('view_sms')) { ?>
                    <li class="sub-menu">
                        <a href="javascript:;"  class="<?= is_active('inbox') ?>">
                            <i class="fa fa-envelope"></i>
                            <span>Mail & SMS </span>
                        </a>
                        <ul class="sub">
                            <li><a href="{{url('inbox/template')}}">SMS Templates</a></li>
                            <li><a href="{{url('inbox')}}">SMS</a></li>
                            <li><a href="{{url('inbox/email')}}">Email</a></li>
                            <li><a href="{{url('inbox/attachment')}}">Attachments</a></li>
                            <li><a href="{{url('inbox/schedule')}}">Schedule</a></li>
                        </ul>
                    </li>
                    <?php
                }
                ?>
                <?php if (can_access('view_settings')) { ?>
                    <li class="sub-menu">
                        <a href="javascript:;" class="<?= is_active('setting') ?>">
                            <i class=" fa fa-bar-chart-o"></i>
                            <span>Settings</span>
                        </a>
                        <ul class="sub">

                            <li><a href="{{url('setting')}}">Configurations</a></li>
                            <li><a href="{{url('setting/user')}}">User Types</a></li>
                            <li><a href="{{url('setting/role')}}">User Roles</a></li>
                            <li><a href="{{url('setting/permission')}}">User Permission</a></li>
                            <li><a href="{{url('setting/event')}}">Events</a></li>
                            <li><a href="{{url('setting/fee')}}">Fee Definition</a></li>
                            <li><a href="{{url('setting/profession')}}">Specialization</a></li>
                        </ul>
                    </li>
                <?php } ?>
                <li>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">
                        <i class="fa fa-user"></i>
                        {{ __('Logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>          
        </div>

    </div>
</aside>