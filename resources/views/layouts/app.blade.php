<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="SHORTCUT ICON" rel="icon" href="{{ url('public/images/erb.png')}}">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="ERB">
        <link rel="shortcut icon">
        <!--Core CSS -->
        <link href="{{ asset('public/bs3/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('public/js/jquery-ui/jquery-ui-1.10.1.custom.min.css') }}" rel="stylesheet">
        <link href="{{ asset('public/css/bootstrap-reset.css') }}" rel="stylesheet">
        <link href="{{ asset('public/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
        <link href="{{ asset('public/js/jvector-map/jquery-jvectormap-1.2.2.css') }}" rel="stylesheet">
        <link href="{{ asset('public/css/clndr.css') }}?v=1" rel="stylesheet">
        <!--clock css-->
        <link href="{{ asset('public/js/css3clock/css/style.css') }}?v=1" rel="stylesheet">
        <!--Morris Chart CSS -->
        <link rel="stylesheet" href="{{ asset('public/js/morris-chart/morris.css') }}?v=1">
        <!-- Custom styles for this template -->
        <link href="{{ asset('public/css/style.css') }}?v=2" rel="stylesheet">
        <link href="{{ asset('public/css/style-responsive.css') }}?v=1" rel="stylesheet"/>
        <link href="{{ asset('public/select2/css/select2.css') }}?v=1" rel="stylesheet">
        <link href="{{ asset('public/select2/css/select2-bootstrap.css') }}?v=1" rel="stylesheet">
        <link href="{{ asset('public/select2/css/gh-pages.css') }}?v=1" rel="stylesheet">
        <link href="{{ asset('public/datatables/dataTables.bootstrap.css')}}" rel="stylesheet">
        <link href="{{ asset('public/css/bootstrap-tour.min.css')}}" rel="stylesheet">

        <!-- Just for debugging purposes. Don't actually copy this line! -->
        <!--[if lt IE 9]>
        <script src="js/ie8-responsive-file-warning.js"></script><![endif]-->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <title>{{ config('app.name', 'ERB') }}</title>

        <!-- Scripts -->

        <!-- Fonts -->
        <link rel="dns-prefetch" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            .invalid-feedback{color:red}
            .form-control{color:black !important}
        </style>
        <script src="{{ asset('public/js/jquery.js') }}"></script>
        <script src="{{ asset('public/bs3/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('public/js/bootstrap-tour.min.js') }}"></script>

        <script type="text/javascript">
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
var root_url = "<?= url('/'); ?>";

        </script>
        <script src="{{ asset('public/js/function.js') }}"></script>
    </head>
    <body>
        <div id="app">


            <main class="py-4">

                <section id="container">
                    <!--header start-->
                    <header class="header fixed-top clearfix">
                        <!--logo start-->
                        <div class="brand">

                            <a href="{{url('/')}}" class=                                    "logo">
                                <img src="{{ url('public/images/erb.png')}}" width="40" height="40" alt=                                        "">
                                <span style="color:#fff">ERB </span></a> 
                            <div class="sidebar-toggle-box">
                                <div class="fa fa-bars"></div>
                            </div>
                        </div>
                        <!--logo end-->
                        @guest
                        <div class="top-nav clearfix">
                            <!--search & user info start-->
                            <ul class="nav navbar-nav pull-right">

                                <li class="dropdown">
                                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">HELP <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a  data-toggle="modal" href="#how">How it Works</a></li>
                                        <li><a data-toggle="modal" href="#pay">How to make Payments</a></li>
                                        <li><a data-toggle="modal" href="#invoice">What is Reference/Invoice Number</a></li> <li><a data-toggle="modal" href="#penalty">Penalties</a></li>
                                    </ul>
                                </li>
                                <li><a href="{{url('/login')}}">ERB STAFF LOGIN</a></li>
                            </ul>

                        </div>

                        @else

                        <div class="top-nav clearfix">
                            <!--search & user info start-->
                            <ul class="nav pull-right top-menu">
                                <?php if (Auth::user()->role_id != null) { ?>

                                    <li>
                                        <form method="get" action="<?= url('find') ?>" id="search">
                                            <input type="text" name="s" class="form-control search" placeholder=" Search" id="search_input">
                                        </form>
                                        <div id="search_loader"></div>
                                        <script type="text/javascript">
    ajax_search_results = function () {
    $('#search_input ').keyup(function (e) {
        if (e.keycode == 13) {
            $('#search').submit()
        }
    });
    }

    $(document).ready(ajax_search_results);
                                        </script>
                                    </li>
                                <?php } ?>
                                <!-- user login dropdown start-->
                                <li class="dropdown">
                                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                        <img alt="" src="{{ asset('public/images/country.jpg')}}">
                                        <span class="username">
                                            {{ Auth::user()->name }}</span>
                                        <b class="caret"></b>
                                    </a>
                                    <ul class="dropdown-menu extended logout">
                                        <li><a href="{{ url('user/profile/'.Auth::user()->id) }}">
                                                <i class="fa fa-suitcase"></i>Profile</a></li>

                                        <li>
                                        <li><a href="{{ url('user/password/') }}"><i class=" fa fa-cog"></i>Change Password</a></li>

                                        <li>
                                            <a class="dropdown-item" href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                                       document.getElementById('logout-form').submit();"><i class="fa fa-key"></i>
                                                {{ __('Logout') }}
                                            </a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                            </ul>                                                               
                            <!--search & user info end-->
                        </div>

                        @endguest
                    </header>

                    @guest
                    @else
                    <?php if (Auth::user()->role_id != null) { ?>
                        @include('layouts.nav')
                    <?php } ?>
                    @endguest
                    <!--sidebar end-->
                    <section id="{{Auth::check() ?'main-content':'' }}">
                        <section class="wrapper">
                            @include('layouts.hints')
                            <div id="search_page_result"></div>
                            @guest
                            @yield('content')
                            @else
                            <?php if (Auth::user()->role_id != null) { ?>
                                @yield('content')
                            <?php } ?>
                            @endguest


                        </section>
                    </section>
                </section>
            </main>
        </div>

        @include('layouts.footer')
    </body>
</html>
