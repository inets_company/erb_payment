@extends('layouts.app')

@section('content')

<div class="row">
    <section class="panel">
        <header class="panel-heading option">
            NameTag
        </header>
        <div class="panel-body option">
            <button type="button" class="btn btn-default" onclick="print_tag()"><i class="fa fa-print"></i>Print</button>
        </div>
        <div class="">
            <div class="col-md-4" >
                <!--widget start-->
                <aside class="profile-nav alt" style="border: 1px solid #ccc">
                    <section class="panel">
                        <div class="user-heading alt">
                            <h5 style="color: black; text-align: center">The United Republic of Tanzania<br/>
                                <?= strtoupper($setting->name) ?></h5>

                            <img style="float: left" src="{{ url('public/images/country.jpg')}}" width="50" height="50">

                            <img style="float: right" src="{{ url('public/images/erb.png')}}" width="50" height="50" alt=                                        "">
                            <div style="float: clear"></div>

                        </div>

                        <ul class="nav nav-pills nav-stacked">
                            <li class="text-center"><a href="javascript:;"> <i class="fa fa-user"></i> <b style="font-size: 180%"><?= $user->name ?></b></a></li>
                            <li class="text-center"><a href="javascript:;"> <i class="fa fa-tasks"></i><?php
                                    if ($user->userType->id == 13) {
                                       echo   $user->profession->name; 
                                    } else {
                                      echo   $user->employer->name ;
                                    }
                                 
                                    ?></a></li>
                            <li><a href="javascript:;" style="text-align: center"><img class="" src="data:image/png;base64,<?= $barcode ?>" /></a></li>
                            <li><a href="javascript:;" style="text-align: center"> <i class="fa fa-marker"></i> <?= $event->description . '<br/>' . $event->location ?> <span class="badge label-warning pull-right r-activity"></span></a></li>
                        </ul>

                    </section>
                </aside>
                <!--widget end-->

            </div>
        </div>
    </section>

</div>
<script type="text/javascript">
    function print_tag() {
        $('.header, .pdf_btn, .print_btn,.option').hide();
        window.print();
        $('.header, .pdf_btn, .print_btn, .option').show();
    }
</script>
@endsection