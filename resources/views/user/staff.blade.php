@extends('layouts.app')

@section('content')

<!-- page start-->

<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                Users
             
            </header>
             <?php if (can_access('add_users')) { ?>
            <p><br/>&nbsp;&nbsp;&nbsp;<a class="btn btn-success" data-toggle="modal" href="#myModal">
                    Add  New User
                </a></p>
             <?php }?>
            <div class="panel-body">
                <section id="unseen">
                    <table class="table table-bordered table-striped table-condensed dataTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th class="numeric">Phone</th>
                                <th class="numeric">Email</th>
                                <th class="numeric">Registration No</th>
                                <th class="numeric">Role</th>
                                <th class="numeric">User Type</th>
                                <th class="numeric">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $total_amount = 0;
                            $total_paid = 0;
                            $total_unpaid = 0;
                            $i = 1;
                            ?>
                            @foreach($users as $user)
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$user->name}}</td>
                                <td class="numeric">{{$user->phone}}</td>
                                <td class="numeric">{{$user->email}}</td>
                                <td data-title="">
                                    {{$user->number}}
                                </td>
                                <td class="numeric">{{$user->role->name}}</td>

                                <td data-title="">
                                    {{$user->userType->name}}
                                </td>

                                <td class="numeric">
                                     <?=can_access('delete_users')? btn_delete('user/' . $user->id, 'user'):'' ?>
                             
                                    <?php if (can_access('edit_users')) { ?>
                                    <a data-toggle="modal" href="#myModal" onmousedown="open_edit_model('<?= $user->id ?>', '<?= url('setting/' . $user->id . '/edit') ?>')" class="btn btn-xs btn-info">Edit</a>  
                                    <?php }?>
                                </td>
                            </tr>
                            <?php $i++;?>
                            @endforeach
                        </tbody>
                    </table>
                </section>
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <form class="cmxform form-horizontal " id="commentForm" method="post" action="<?= url('user') ?>">

                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title">Add New User</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="panel-body">
                                        <div class=" form">
                                            <div class="form-group ">
                                                <label for="cname" class="control-label col-lg-3">Name (required)</label>
                                                <div class="col-lg-6">
                                                    <input class=" form-control" id="name" name="name" minlength="2" type="text" required="" onblur="this.value = this.value.toUpperCase()"  pattern="[a-zA-Z\. ]{5,}">
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label for="cemail" class="control-label col-lg-3">E-Mail (required)</label>
                                                <div class="col-lg-6">
                                                    <input class="form-control " id="email" type="email" name="email" required="" onblur="this.value = this.value.toLowerCase()">
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label for="phone" class="control-label col-lg-3">Phone (required)</label>
                                                <div class="col-lg-6">
                                                    <input class="form-control " id="phone" type="text" name="phone">
                                                </div>
                                            </div>
<!--                                             <div class="form-group ">
                                                <label for="user_type_id" class="control-label col-lg-3">User Type (required)</label>
                                                <div class="col-lg-6">
                                                    <select name="user_type_id" id="user_type_id" class="form-control m-bot15">
                                                        <?php $user_types = \App\Model\User_type::all();
                                                        foreach ($user_types as $user_type) {
                                                            ?>
                                                        <option value="<?=$user_type->id?>"><?=$user_type->name?></option>
                                                        <?php }   ?>
                                                        
                                                    </select>
                                                </div>
                                            </div>-->
                                            <div class="form-group ">
                                                <label for="role_id" class="control-label col-lg-3">Role(required)</label>
                                                <div class="col-lg-6">
                                                    <select name="role_id" id="role_id"  class="form-control m-bot15">
                                                        <?php $roles = \App\Model\Role::all();
                                                        foreach ($roles as $role) {
                                                            ?>
                                                        <option value="<?=$role->id?>"><?=$role->name?></option>
                                                        <?php }   ?>
                                                        
                                                    </select>
                                                </div>
                                            </div>
                                           
                                     

                                        </div>

                                    </div>


                                </div>
                                <div class="modal-footer">
<?= csrf_field() ?>
                                    <input type="hidden" name="user" value="user"/>
                                    <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                    <button class="btn btn-success" type="submit">Save changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<!-- page end-->
<script type="text/javascript">
    open_edit_model = function (a, b) {
        $.ajax({
            type: 'POST',
            url: "<?= url('setting/getedit') ?>",
            data: {
                "id": a,
                "table": "user"
            },
            dataType: "json",
            success: function (data) {
                $('#title_page').html('Edit user type');
                $('#commentForm').attr('action', b);
                $("#commentForm").attr("method", "get");
                $.each(data, function (i, item) {
                    $('#' + i).val(item);
                });
            }
        });
    }
    reset_form = function () {
        $('#title_page').html('Add New User Type');
        $('#commentForm').attr('action', '<?= url('user') ?>');
        $("#commentForm").attr("method", "post");
        $("input:not(:hidden)").val('');
        $('.delete').html('Delete');
    }
</script>
@endsection