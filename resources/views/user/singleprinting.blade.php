<style type="text/css">

    .nametag{

        background: #ffffff;
        text-align: center;
        page-break-after: always;
        font-size: 285% !important;
        max-width: 96%;
        margin-left: 2%;
        border: 8px solid #000000; 
        height: 22em;
     
        
    }
    .username{
        border-bottom: 1px solid #ccc;
        margin-bottom: 2em;
    }
    .tag{

    }
    body{
        margin: 0; padding: 0;
    }
    table{
        text-align: center;
    }
</style> 
    <?php
$i = 1;
foreach ($users as $user) {
    $barcode = (new \App\Http\Controllers\SettingController())->createBarCode($user->id);
    ?>
<div class="nametag">
    <table  style="">
        <thead>
            <tr>
                <th style="padding:3px; color: black; font-weight: bolder; text-align: center">   
                    The United Republic of Tanzania<br/>
                    <?= strtoupper($setting->name) ?>
                    <img style="float: left" src="{{ url('public/images/country.jpg')}}" width="180" height="180"> <img style="float: right" src="{{ url('public/images/erb.png')}}" width="180" height="180" alt="">

                </th>
            </tr>
           
        </thead>
        <tbody>
            <tr>
                <td class="username">   <i class="fa fa-user"></i> <h4 style="color: #0000FF !important;font-weight: bolder;    font-size: 65%;"><br/><?= strtoupper(strtoupper($user->name)) ?></h4></td>
            </tr>
            <tr>
                <td  class="username"><b style="color: #FF0000 !important;
                                         font-weight: bolder"> <i class="fa fa-tasks"></i> <?php
                                    if ($user->userType->id == 13) {
                                       echo   strtoupper($user->profession->name); 
                                    } else {
                                      echo   strtoupper($user->employer->name);
                                    }
                                 
                                    ?></b></td>
            </tr>
            <tr>
                <td  class="username" > <b style="text-align: center; padding-top: 5%"><img class=""  src="data:image/png;base64,<?= $barcode ?>" width="50%"  /></b></td>
            </tr>
            <tr>
                <td>  <i class="fa fa-marker"></i> <?= $event->description . '<br/>' . $event->location ?> <span class="badge label-warning pull-right r-activity"></span></td>
            </tr>
        </tbody>
    </table>
</div>
    <?php
    $i++;
}
?>