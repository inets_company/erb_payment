@extends('layouts.app')

@section('content')
<!-- page start-->

<div class="row">
    <div class="col-sm-12">

        <section class="panel">

            <header class="panel-heading">
                Applicants

            </header>
            <?php
            // dd(md5('ERB-MIS'));
            if (can_access('add_invoices')) {
                $type='';
                if (request('user_type') == 120) {
                    
                } else if (request('user_type') == 7) {
                    $type = '<a href="' . url('invoice/create/?bulk=1') . '" class="btn btn-primary">Create Sponsored Invoice</a>';
                } else if (request('user_type') == 9) {
                    $type = '<a href="' . url('invoice/create/?bulk=1') . '" class="btn btn-primary">Create Non-Sponsored Invoice </a>';
                }
                ?>
                <p>
                    <br/>
                    &nbsp; 
                    <?=$type?>
                </p>
            <?php } ?>
            <div class="row">
                <div class="col-md-3">
                    <div class="mini-stat clearfix">
                        <span class="mini-stat-icon orange"><i class="fa fa-user"></i></span>
                        <div class="mini-stat-info">
                            <span><?= \App\Model\User::count() ?></span>
                            Total Participants
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="mini-stat clearfix">
                        <span class="mini-stat-icon tar"><i class="fa fa-money"></i></span>
                        <div class="mini-stat-info">
                            <span><?= \App\Model\Payment::count() ?></span>
                            Total Payments
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="mini-stat clearfix">
                        <span class="">Search By Barcode</span>
                        <div class="mini-stat-info">
                            <input type="text" autofocus="true" class="form-control" id="search_input_tag"/>

                        </div>
                        <span id="attendance_search_loader"></span>
                    </div>
                </div>

            </div>
            <div class="panel-body">
                <div class="row"  id="search_checked_table" style="display:none">
                    <a type="button" target="_blank" tags='' id="nametag_link" href="" value="" name="" class="btn btn-sm btn-danger link"><i class="fa fa-cloud"></i> Print NameTag (HP Printer)</a>
                    <a type="button" target="_blank" tags='' id="nametag_link2" href="" value="" name="" class="btn btn-sm btn-primary link"><i class="fa fa-cloud"></i> Print single NameTag (EPSON Printer) </a>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Number</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr  id="search_checked"></tr>
                        </tbody>
                    </table>
                </div>
                <?php if ((int) request('paid') != 1) { ?>
                    <div class="row">  
                        <div class="form-group ">
                            <label for="number" class="control-label col-lg-3 text-right">Sort</label>
                            <div class="col-lg-6">
                                <select class="form-control" id="sort_user" name="user_type">
                                    <option value=""></option> 
                                    <option value="0">All</option> 
                                    <option value="120">Employers</option> 
                                    <?php $user_types = \App\Model\User_type::all(); ?>
                                    @foreach ($user_types as $user_type)
                                    <option value="{{$user_type->id}}">{{$user_type->name}}</option>                                                  @endforeach;
                                </select>

                            </div>
                        </div>
                    </div>
                <?php } ?>
                <br/>
                <section id="unseen">
                    <table class="table table-bordered table-striped table-condensed dataTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th class="numeric">Phone</th>
                                <th class="numeric">Email</th>
                                <th class="numeric">Type</th>
                                <th class="numeric">Employer</th>
                                <th class="numeric">Specialization</th>
                                <th class="numeric col-sm-4">Action</th>
                                <th class="numeric col-sm-2">Print Count</th>
                                <th><input type="checkbox" name="all" id="toggle_all"/></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            ?>
                            @foreach($applicants as $applicant)
                            <tr  id="row<?= $applicant->id ?>">
                                <td>{{$i}}</td>
                                <td>{{$applicant->name}}</td>
                                <td class="numeric">{{$applicant->phone}}</td>
                                <td class="numeric">{{$applicant->email}}</td>
                                <td>{{$applicant->userType->name}}</td>
                                <td>{{$applicant->employer->name}}</td>
                                <td>{{$applicant->profession->name}}</td>
                                <td> 
                                    <?= can_access('delete_users') ? btn_delete('user/' . $applicant->id, 'user') : '' ?>
                                    <a href="<?= url('user/profile/' . $applicant->id) ?>" class="btn btn-xs btn-primary">View</a> &nbsp; &nbsp;
                                    <?php if (can_access('edit_users')) { ?>
                                        <a data-toggle="modal" href="#myModal" onmousedown="open_edit_model('<?= $applicant->id ?>', '<?= url('setting/' . $applicant->id . '/edit') ?>')" class="btn btn-xs btn-info">Edit</a> 
                                    <?php } ?>

                                    <?php if ($applicant->is_employer <> 1 && $applicant->payment()->count() > 0) { ?>
                                        <a href="<?= url('user/ticket/' . $applicant->id) ?>" class="btn btn-xs btn-warning">Barcode</a> &nbsp; &nbsp;

                                        <a href="<?= url('user/nametag/' . $applicant->id) ?>" class="btn btn-xs btn-default">Tag</a>
                                    <?php } ?>
                                </td>
                                <td>{{$applicant->nametagPrintlog()->count()}}</td>
                                <td><input type="checkbox" class="check" name="select[]" value="<?= $applicant->id ?>"/></td>
                            </tr>
                            <?php $i++; ?>
                            @endforeach
                        </tbody>
                    </table>
                    <?php if (count($applicants) && (int) request('paid') != 1) { ?>
                    <?php } ?>
                </section>
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <form class="cmxform form-horizontal " id="commentForm" method="post" action="<?= url('invoice') ?>">

                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="title_page">Add New User</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="panel-body">
                                        <div class="form-group ">
                                            <label for="type" class="control-label col-lg-3">Employer</label>
                                            <div class="col-lg-6">
                                                <select class="form-control"  name="employer_id" id="employer_id">
                                                    <option value=""></option> 
                                                    <?php $userype = \App\Model\Employer::orderBy('name')->get() ?>
                                                    @foreach ($userype as $type)
                                                    <option value="{{$type->id}}">{{$type->name}}</option>                                                  @endforeach;
                                                </select>

                                            </div>
                                            <?php echo form_error($errors, 'employer_id'); ?>
                                        </div>
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-3">Name (required)</label>
                                            <div class="col-lg-6">
                                                <input class=" form-control" id="name" name="name" minlength="2" type="text" required="" value="{{old('name')}}" pattern="[a-zA-Z\. ]{5,}"  onblur="this.value = this.value.toUpperCase()">
                                            </div>
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group ">
                                            <label for="cemail" class="control-label col-lg-3">E-Mail (required)</label>
                                            <div class="col-lg-6">
                                                <input class="form-control " id="email" type="email" name="email" required=""  value="<?= old('email') ?>"  onblur="this.value = this.value.toLowerCase()">
                                            </div>
                                            @if ($errors->has('email'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group ">
                                            <label for="phone" class="control-label col-lg-3">Phone Number</label>
                                            <div class="col-lg-6">
                                                <input class="form-control " id="phone" type="text" name="phone"  value="<?= old('phone') ?>">
                                            </div>
                                            @if ($errors->has('phone'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group ">
                                            <label for="number" class="control-label col-lg-3">Specialization (Required)</label>
                                            <div class="col-lg-6">
                                                <select class="form-control" name="profession_id" id="profession_id">
                                                    <option value=""></option>                                     
                                                    <?php $professions = App\Model\Profession::where('invitee', 0)->orderBy('name')->get(); ?>
                                                    @foreach ($professions as $profession)
                                                    <option value="{{$profession->id}}">{{$profession->name}}</option>                                                  @endforeach;
                                                </select>

                                            </div>
                                            <?php echo form_error($errors, 'profession_id'); ?>
                                            <!--<a data-toggle="modal" href="#myModal">Or Add new</a>-->
                                        </div>




                                    </div>


                                </div>
                                <div class="modal-footer">
                                    <?= csrf_field() ?>
                                    <input type="hidden" name="user" value="user"/>
                                    <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                    <button class="btn btn-success" type="submit">Save changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<!-- page end-->
<script type="text/javascript">
    ajax_barcode_search = function () {
        document.addEventListener('keydown', function (event) {
            if (event.keyCode == 17 || event.keyCode == 74)
                event.preventDefault();
        });
        $('#search_input_tag').keyup(function (e) {
            if (e.keyCode == 13) {
                var pasteData = $(this).val();
                $.ajax({
                    type: 'GET',
                    url: "<?= url('find') ?>",
                    data: {
                        "s": pasteData,
                        'type': 1,
                        user_id_tags: $('#user_id_tags').attr('content')
                    },
                    dataType: "html ",
                    beforeSend: function (xhr) {
                        $('#search_loader').html('<a href="#/refresh"><i class="fa fa-spinner"></i> </a>');
                    },
                    complete: function (xhr, status) {
                        $('#search_input_tag').val('');
                        $('#search_loader').html('');
                    },
                    success: function (data) {
                        var val = $('#search_page_result').text();

                        if (val == '') {
                            $('#search_page_result').html(data);
                        } else {
                            $('#search_page_result').html(data);
                        }
                    }});
            }
        });
    }

    $(document).ready(ajax_barcode_search);
    open_edit_model = function (a, b) {
        $.ajax({
            type: 'POST',
            url: "<?= url('setting/getedit') ?>",
            data: {
                "id": a,
                "table": "user"
            },
            dataType: "json",
            success: function (data) {
                $('#title_page').html('Edit user type');
                $('#commentForm').attr('action', b);
                $("#commentForm").attr("method", "get");
                $.each(data, function (i, item) {
                    $('#' + i).val(item);
                });
            }
        });
    }
    reset_form = function () {
        $('#title_page').html('Add New User Type');
        $('#commentForm').attr('action', '<?= url('user') ?>');
        $("#commentForm").attr("method", "post");
        $("input:not(:hidden)").val('');
        $('.delete').html('Delete');
    }
    sort_user = function () {
        $('#sort_user').change(function () {
            var type = $(this).val();
            window.location.href = '<?= url()->current() ?>/?user_type=' + type;
        });
    }

    search_checked = function () {
        $('.check').click(function () {
            var value = $(this).val();
            var status = $(this).is(':checked');
            if (status === true) {
                var text = $('#row' + value).html();
                $('#search_checked_table').show();
                $('#search_checked').after('<tr id="s_table' + value + '">' + text + '</tr>');
                var ex = $('.link').attr('tags');
                var url = '<?= url('user/bulknametag?ids=') ?>';
                var url2 = '<?= url('user/bulknametag?single=1&ids=') ?>';
                var param = ex.split(",");
                param.push(value);
                $('#nametag_link').attr('tags', param.join(","));
                $('#nametag_link').attr('href', url + param.join(","));

                $('#nametag_link2').attr('tags', param.join(","));
                $('#nametag_link2').attr('href', url2 + param.join(","));
                console.log(param);

            } else {
                var ex = $('.link').attr('tags');
                var url = '<?= url('user/bulknametag?ids=') ?>';
                var url2 = '<?= url('user/bulknametag?single=1&ids=') ?>';
                var param = ex.split(",");
                param = jQuery.grep(param, function (val) {
                    return val != value;
                });
                var arr = param;

                var result = arr.filter(function (elem) {
                    return elem != value;
                });
                console.log(result);

                $('#nametag_link').attr('tags', result.join(","));
                $('#nametag_link').attr('href', url + result.join(","));


                $('#nametag_link2').attr('tags', param.join(","));
                $('#nametag_link2').attr('href', url2 + param.join(","));

                $('#s_table' + value).remove();
            }
        });
    }
    toggle_all = function () {
        $('#toggle_all').click(function () {
            var status = $(this).is(':checked');
            if ($("#toggle_all").prop('checked')) {
                $('.check').prop("checked", true);
            } else {
                $('.check').prop("checked", false);
            }
            if (status === true) {
                //select all

                $.ajax({
                    type: 'GET',
                    url: "<?= url('user/getApplicants') ?>",
                    data: {
                        "type": '<?= request('type') ?>',
                    },
                    dataType: "html",
                    success: function (data) {
                        console.log(data);
                        $('#search_checked_table').show();
                        var ex = data;
                        var url = '<?= url('user/bulknametag?ids=') ?>';
                        var url2 = '<?= url('user/bulknametag?single=1&ids=') ?>';
                        var param = ex.split(",");
                        $('#nametag_link').attr('tags', param.join(","));
                        $('#nametag_link').attr('href', url + param.join(","));

                        $('#nametag_link2').attr('tags', param.join(","));
                        $('#nametag_link2').attr('href', url2 + param.join(","));
                        console.log(param);

                    }
                });

            } else {
                //diselect all
                $('#nametag_link').attr('tags', '');
                $('#nametag_link').attr('href', '');

                $('#nametag_link2').attr('tags', '');
                $('#nametag_link2').attr('href', '');
                $('#search_checked_table').hide();
            }
        });
    };
//    $('#search_input_tag2').on('paste', function (e) {
//        var pasteData = e.originalEvent.clipboardData.getData('text');
//        $.ajax({
//            type: 'GET',
//            url: "<?= url('find') ?>",
//            data: {
//                "s": pasteData,
//                'type': 1,
//                user_id_tags: $('#user_id_tags').attr('content')
//            },
//            dataType: "html ",
//            beforeSend: function (xhr) {
//                $('#search_loader').html('<a href="#/refresh"><i class="fa fa-spinner"></i> </a>');
//            },
//            complete: function (xhr, status) {
//                $('#search_input_tag').val('');
//                $('#search_loader').html('');
//            },
//            success: function (data) {
//                var val = $('#search_page_result').text();
//                if (val == '') {
//                    $('#search_page_result').html(data);
//                } else {
//                    $('#search_page_result').html(data);
//                }
//            }});
//    });
    $(document).ready(toggle_all);
    $(document).ready(search_checked);
    $(document).ready(sort_user);
</script>
@endsection