@extends('layouts.app')

@section('content')

<div class="row">
<div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    Event Barcode
                  
                </header>
                <div class="panel-body">
                    <span class="option"> Options</span>
                    <button type="button" class="btn btn-primary print_btn" id="" onclick="print_ticket()"><i class="fa fa-print"></i> Print</button>
                    <a href="<?=url('myticket/'.$id.'/?auth='.$token)?>" class="btn btn-success pdf_btn" id=""><i class="fa fa-download"></i> PDF Download</a>
                    <div class="myticket">
<?php

$setting=(new \App\Http\Controllers\SettingController());
echo $setting->getUserTicket($id);?>
</div>
                </div>
            </section>
        </div>
</div>

  <style>
        body {
            color: #494140;
            font-weight: 400;
            font-size: 15px;
            font-family:'Open Sans',sans-serif !important; 
            min-height: 200px;
        }
        .myticket{
            background: #fff;
            margin-top: 4%;
        }

        .verticalTableHeader {
            text-align:center;
            margin-top: 15% !important;
            transform: rotate(-90deg);

        }
        .verticalTableHeader p {
            display:inline-block;
        }
        .verticalTableHeader p:before{
            vertical-align:middle;
        }
        .table_ticket{text-align: center;}
    </style>
    <script type="text/javascript">
        function print_ticket(){
            $('.header, aside, .pdf_btn, .print_btn,.option').hide();
            $('#sidebar').hide();
            window.print();
             $('.header, aside, #sidebar,.pdf_btn, .print_btn, .option').show();
             $('#sidebar').show();
        }
        </script>
@endsection