@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12">
        <section class="panel">
            <div class="panel-body profile-information">
                <div class="col-md-3">
                    <div class="profile-pic text-center">
                        <img src="{{ asset('public/images/icon.png')}}" alt="">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="profile-desk">
                        <h1><?= $user->name ?></h1>
                        <h3><span class="text-muted">Type: <?= $user->userType->name ?></span></h3>
                        <p>
                            Employer:  <?= $user->employer->name ?>
                        </p>
                        <br/>
                        <p>
                            Phone:  <?= $user->phone ?>
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="profile-statistics">
                        <h1><?= $user->userType->name ?></h1>
                        <p>User Type</p>
                        <h1><?= $user->role->name ?></h1>
                        <p>User Role</p>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="col-md-12">
        <section class="panel">
            <header class="panel-heading tab-bg-dark-navy-blue">
                <ul class="nav nav-tabs nav-justified ">
                    <?php
                    if ($user->is_employer == 1) {
                        ?>
                        <li class="active">
                            <a data-toggle="tab" href="#members">
                                Members Information
                            </a>
                        </li>
                    <?php } else { ?>
                        <li class="active">
                            <a data-toggle="tab" href="#overview">
                                Invoice Information
                            </a>
                        </li>
                    <?php } ?>
                    <li class="">
                        <a data-toggle="tab" href="#job-history">
                            User Activity
                        </a>
                    </li>
                    <!--                    <li class="">
                                            <a data-toggle="tab" href="#settings">
                                                Settings
                                            </a>
                                        </li>-->
                </ul>
            </header>
            <div class="panel-body">
                <div class="tab-content tasi-tab">
                    <?php
                    if ($user->is_employer == 1) {
                        ?>
                        <div id="members" class="tab-pane active">
                            <div class="row">
                                <div class="col-md-12">
                                    <h1>Members</h1>
                                    <div class="panel-body">
                                        <div class="row"  id="search_checked_table" style="display:none">
                                            <a type="button" target="_blank" tags='' id="nametag_link" href="" value="" name="" class="btn btn-sm btn-danger"><i class="fa fa-cloud"></i> Print NameTag</a>
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Number</th>
                                                        <th>Name</th>
                                                        <th>Email</th>
                                                        <th>Phone</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr  id="search_checked"></tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <section id="unseen">
                                            <table class="table table-bordered table-striped table-condensed dataTable">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Name</th>
                                                        <th class="numeric">Phone</th>
                                                        <th class="numeric">Email</th>
                                                        <th class="numeric">Type</th>
                                                        <th class="numeric">Employer</th>
                                                        <th class="numeric">Specialization</th>
                                                        <th class="numeric col-sm-4">Action</th>
                                                        <th><input type="checkbox" name="all" id="toggle_all"/></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $i = 1;
                                                    // $applicants = \App\Model\User::where('employer_id', $user->employer_id)->get();
                                                    $invoice = \App\Model\Invoice::where('user_id', $user->id)->first();
                                                    //$applicants = $user->invoice->invoiceFee()->get();
                                                    if (count($invoice) > 0) {
                                                        $applicants = $invoice->invoiceFee()->get();
                                                    } else {
                                                        $applicants = [];
                                                    }
                                                    $user_ids = [];
                                                    if (count($applicants) > 0) {
                                                        ?>
                                                        @foreach($applicants as $applicant)
                                                        <tr  id="row<?= $applicant->user->id ?>">
                                                            <td>{{$i}}</td>
                                                            <td>{{$applicant->user->name}}</td>
                                                            <td class="numeric">{{$applicant->user->phone}}</td>
                                                            <td class="numeric">{{$applicant->user->email}}</td>
                                                            <td>{{$applicant->user->userType->name}}</td>
                                                            <td>{{$applicant->user->employer->name}}</td>
                                                            <td>{{$applicant->user->profession->name}}</td>
                                                            <td> 
                                                                <?= btn_delete('user/' . $applicant->user->id, 'user') ?>

                                                                <a href="<?= url('user/profile/' . $applicant->user->id) ?>" class="btn btn-xs btn-primary">View</a> &nbsp; &nbsp;<a data-toggle="modal" href="#myModal" onmousedown="open_edit_model('<?= $applicant->user->id ?>', '<?= url('setting/' . $applicant->user->id . '/edit') ?>')" class="btn btn-xs btn-info">Edit</a> 

                                                                <a href="<?= url('user/ticket/' . $applicant->user->id) ?>" class="btn btn-xs btn-warning">Barcode</a> &nbsp; &nbsp;
                                                                <a href="<?= url('user/nametag/' . $applicant->user->id) ?>" class="btn btn-xs btn-default">Tag</a>

                                                            </td>
                                                            <td><input type="checkbox" class="check" name="select[]" value="<?= $applicant->user->id ?>"/></td>
                                                        </tr>
                                                        <?php
                                                        $i++;
                                                        array_push($user_ids, $applicant->user->id);
                                                        ?>
                                                        @endforeach

                                                        <?php
                                                    }
                                                    if (isset($user->employer_id) && $user->employer_id != null) {
                                                        $other_applicants = \App\Model\User::where('employer_id', $user->employer_id)->where('is_employer', 0)->whereNotIn('id', $user_ids)->get();
                                                    } else {
                                                        $other_applicants = array();
                                                    }

                                                    if (count($other_applicants) > 0) {
                                                        ?>
                                                        @foreach($other_applicants as $user)
                                                        <tr  id="row<?= $user->id ?>">
                                                            <td>{{$i}}</td>
                                                            <td>{{$user->name}}</td>
                                                            <td class="numeric">{{$user->phone}}</td>
                                                            <td class="numeric">{{$user->email}}</td>
                                                            <td>{{$user->userType->name}}</td>
                                                            <td>{{$user->employer->name}}</td>
                                                            <td>{{$user->profession->name}}</td>
                                                            <td> 
                                                                <?= btn_delete('user/' . $user->id, 'user') ?>

                                                                <a href="<?= url('user/profile/' . $user->id) ?>" class="btn btn-xs btn-primary">View</a> &nbsp; &nbsp;<a data-toggle="modal" href="#myModal" onmousedown="open_edit_model('<?= $user->id ?>', '<?= url('setting/' . $user->id . '/edit') ?>')" class="btn btn-xs btn-info">Edit</a> 

                                                                <a href="<?= url('user/ticket/' . $user->id) ?>" class="btn btn-xs btn-warning">Barcode</a> &nbsp; &nbsp;
                                                                <a href="<?= url('user/nametag/' . $user->id) ?>" class="btn btn-xs btn-default">Tag</a>

                                                            </td>
                                                            <td><input type="checkbox" class="check" name="select[]" value="<?= $user->id ?>"/></td>
                                                        </tr>
                                                        <?php $i++; ?>
                                                        @endforeach

                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </section>

                                    </div>


                                </div>

                            </div>
                        </div>
                    <?php } else { ?>
                        <div id="overview" class="tab-pane active">
                            <div class="row">
                                <div class="col-md-12">
                                    <h1>Invoice Records</h1>
                                    <div class="panel-body">
                                        <section id="unseen">
                                            <table class="table table-bordered table-striped table-condensed">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Payer Name</th>
                                                        <th class="numeric">Invoice Number</th>
                                                        <th class="numeric">Date</th>
                                                        <th class="numeric">Amount</th>
                                                        <th class="numeric">Payment For</th>
                                                        <th class="numeric">Paid Amount</th>
                                                        <th class="numeric">UnPaid Amount</th>
                                                        <th class="numeric">Payment Status</th>
                                                        <th class="numeric">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $i = 1;

                                                    $invoices = $user->invoice()->get();


                                                    if (count($invoices) > 0) {
                                                        ?>

                                                        @foreach($invoices as $invoice)
                                                        <tr>
                                                            <td>{{$i}}</td>
                                                            <td>{{$invoice->user->name}}</td>
                                                            <td class="numeric">{{$invoice->number}}</td>
                                                            <td class="numeric">{{$invoice->date}}</td>
                                                            <td data-title="">
                                                                <?php
                                                                $am = $invoice->invoiceFee()->sum('amount');

                                                                echo money($am);
                                                                ?>
                                                            </td>
                                                            <td class="numeric">{{$invoice->title}}</td>

                                                            <td data-title="">
                                                                <?php
                                                                $paid = $invoice->invoiceFeesPayment()->sum('paid_amount');
                                                                echo money($paid);
                                                                ?>
                                                            </td>
                                                            <td data-title="">
                                                                <?php
                                                                $unpaid = $am - $paid;
                                                                echo money($unpaid);
                                                                ?>
                                                            </td>
                                                            <td class="numeric"><?php
                                                        if ($invoice->status == 1) {
                                                            echo '<span class="label label-success">Paid</span>';
                                                        } else if ($invoice->status == 2) {
                                                            echo '<span class="label label-warning">Partially Paid</span>';
                                                        } else {
                                                            echo '<span class="label label-danger">Not Paid</span>';
                                                        }
                                                        $i++;
                                                                ?></td>

                                                            <td class="numeric">
                                                                <a href="<?= url('invoice/' . $invoice->id) ?>" class="btn btn-xs btn-success">View</a>
                                                                <?= btn_delete('invoice/' . $invoice->id, '') ?>



                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                        <?php
                                                    } else {
                                                        if (count($user) == 0) {
                                                            $invoices = [];
                                                        } else {
                                                            $invoices = $user->invoiceFee()->get();
                                                        }
                                                        if (count($invoices) > 0) {
                                                            ?>
                                                            @foreach($invoices as $invoice_fee)
                                                            <tr>
                                                                <td>{{$i}}</td>
                                                                <td>{{$user->name.', Sponsored Organization: '.$invoice_fee->invoice->user->name}}</td>
                                                                <td class="numeric">{{$invoice_fee->invoice->number}}</td>
                                                                <td class="numeric">{{$invoice_fee->invoice->date}}</td>
                                                                <td data-title="">
                                                                    <?php
                                                                    $am = $invoice_fee->invoice->invoiceFee->sum('amount');

                                                                    echo money($am);
                                                                    ?>
                                                                </td>
                                                                <td class="numeric">{{$invoice_fee->invoice->title}}</td>

                                                                <td data-title="">
                                                                    <?php
                                                                    $paid = $invoice_fee->invoice->invoiceFeesPayment()->sum('paid_amount');
                                                                    echo money($paid);
                                                                    ?>
                                                                </td>
                                                                <td data-title="">
                                                                    <?php
                                                                    $unpaid = $am - $paid;
                                                                    echo money($unpaid);
                                                                    ?>
                                                                </td>
                                                                <td class="numeric"><?php
                                                        if ($invoice_fee->invoice->status == 1) {
                                                            echo '<span class="label label-success">Paid</span>';
                                                        } else if ($invoice_fee->invoice->status == 2) {
                                                            echo '<span class="label label-warning">Partially Paid</span>';
                                                        } else {
                                                            echo '<span class="label label-danger">Not Paid</span>';
                                                        }
                                                        $i++;
                                                                    ?></td>

                                                                <td class="numeric">
                                                                    <a href="<?= url('invoice/' . $invoice_fee->invoice->id) ?>" class="btn btn-xs btn-success">View</a>
                                                                    <?= btn_delete('invoice/' . $invoice_fee->invoice->id, '') ?>


                                                                </td>
                                                            </tr>
                                                            @endforeach  
                                                        <?php
                                                        }
                                                    }
                                                    ?>
                                                </tbody>

                                            </table>
                                        </section>
                                    </div>


                                </div>

                            </div>
                        </div>
                    <?php } ?>
                    <div id="job-history" class="tab-pane">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="timeline-messages">
                                    <h3>Take a Tour</h3>


                                    <div class="recent-act">

                                        <h4>User Activity</h4>
                                        <div class="x_content">

                                            <div class="flex">
                                                <table class="data table table-striped no-margin">
                                                    <thead>
                                                        <tr>
                                                            <th>On Date</th>
                                                            <th>Page visit action</th>
                                                            <th>Device</th>
                                                            <th class="hidden-phone">Browser</th>
                                                            <th>Location</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $logs = \App\Model\Log::where('user_id', $user->id)->paginate(30);
                                                        if (isset($logs)) {

                                                            foreach ($logs as $log) {
                                                                //check to identify action
                                                                $ar = explode('/', $log->url);
                                                                $controller = isset($ar[1]) ? $ar[1] : '';
                                                                $method = isset($ar[2]) ? $ar[2] : '';
                                                                ?>
                                                                <tr>
                                                                    <td><?= date('d M Y', strtotime($log->created_at)) . ' at ' . date('h:i A', strtotime($log->created_at)) ?></td>
                                                                    <td><?php
                                                                        if (preg_match('/index/', $method) || preg_match('/view/', $method) || preg_match('/show/', $method)) {
                                                                            $action = 'View';
                                                                        } else if (preg_match('/add/', $method)) {
                                                                            $action = 'Add ';
                                                                        } else if (preg_match('/edit/', $method)) {
                                                                            $action = 'Edit';
                                                                        } else if (preg_match('/delete/', $method)) {
                                                                            $action = 'Delete';
                                                                        } else {
                                                                            $action = 'View';
                                                                        }
                                                                        echo $action . ' ' . $controller;
                                                                        ?></td>
                                                                    <td><?= $log->platform ?></td>
                                                                    <td class="hidden-phone"><?= $log->user_agent ?></td>
                                                                    <td class="vertical-align-mid">
                                                                        <?= $log->city ?>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>

                                            </div>

                                        </div>

                                        <div><?php if (isset($logs)) { ?>
                
                                            <?php } ?>
                                        </div>



                                    </div>
                                    <!-- /comment -->
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="settings" class="tab-pane ">
                        <div class="position-center">
                            <div class="prf-contacts sttng">
                                <h2>  User Information</h2>
                            </div>
                            <form role="form" class="form-horizontal"  id="commentForm" method="POST" action="<?= url('user/' . $user->id) ?>">

                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Name</label>
                                    <div class="col-lg-6">
                                        <input type="text" placeholder=" " id="c-name" value="<?= $user->name ?>" name="name" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Reg Number</label>
                                    <div class="col-lg-6">
                                        <input type="text"  value="<?= $user->number ?>" placeholder=" " name="number" id="location" class="form-control">
                                    </div>
                                </div>



                                <div class="prf-contacts sttng">
                                    <h2>Contact</h2>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Phone</label>
                                    <div class="col-lg-6">
                                        <input type="text" placeholder=" "  value="<?= $user->phone ?>" name="phone" id="phone" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Email</label>
                                    <div class="col-lg-6">
                                        <input type="text" placeholder=" " value="<?= $user->email ?>" name="email" id="email" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label for="role_id" class="control-label col-lg-2">Role(required)</label>
                                    <div class="col-lg-6">
                                        <select name="role_id"  class="form-control m-bot15">
                                            <?php
                                            $roles = \App\Model\Role::all();
                                            foreach ($roles as $role) {
                                                ?>
                                                <option value="<?= $role->id ?>" <?php
                                                if ($user->role_id == $role->id) {
                                                    echo 'selected';
                                                }
                                                ?>><?= $role->name ?></option>
                                                    <?php } ?>

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="user_type_id" class="control-label col-lg-2">User Type (required)</label>
                                    <div class="col-lg-6">
                                        <select name="user_type_id" class="form-control m-bot15">
                                            <?php
                                            $user_types = \App\Model\User_type::all();
                                            foreach ($user_types as $user_type) {
                                                ?>
                                                <option value="<?= $user_type->id ?>"  <?php
                                                if ($user->user_type_id == $user_type->id) {
                                                    echo 'selected';
                                                }
                                                ?>><?= $user_type->name ?></option>
                                                    <?php } ?>

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <?= csrf_field() ?>
                                        {{ Form::open(array('url' => 'user/' . $user->id, 'class' => '')) }}
                                        {{ Form::hidden('_method', 'PUT') }}
                                        {{ Form::close() }}

                                        <button class="btn btn-primary" type="submit">Save</button>
                                        <button class="btn btn-default" type="button">Cancel</button>
                                    </div>
                                </div>

                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<script type="text/javascript">
    search_checked = function () {
        $('.check').click(function () {
            var value = $(this).val();
            var status = $(this).is(':checked');
            if (status === true) {
                var text = $('#row' + value).html();
                $('#search_checked_table').show();
                $('#search_checked').after('<tr id="s_table' + value + '">' + text + '</tr>');
                var ex = $('#nametag_link').attr('tags');
                var url = '<?= url('user/bulknametag?ids=') ?>';
                var param = ex.split(",");
                param.push(value);
                $('#nametag_link').attr('tags', param.join(","));
                $('#nametag_link').attr('href', url + param.join(","));
                console.log(param);

            } else {
                var ex = $('#nametag_link').attr('tags');
                var url = '<?= url('user/bulknametag?ids=') ?>';
                var param = ex.split(",");
                param = jQuery.grep(param, function (val) {
                    return val != value;
                });
                var arr = param;

                var result = arr.filter(function (elem) {
                    return elem != value;
                });
                console.log(result);

                $('#nametag_link').attr('tags', result.join(","));
                $('#nametag_link').attr('href', url + result.join(","));
                $('#s_table' + value).remove();
            }
        });
    }
    toggle_all = function () {
        $('#toggle_all').click(function () {
            var status = $(this).is(':checked');
            if ($("#toggle_all").prop('checked')) {
                $('.check').prop("checked", true);
            } else {
                $('.check').prop("checked", false);
            }
            if (status === true) {
                //select all

                $.ajax({
                    type: 'GET',
                    url: "<?= url('user/getApplicants') ?>",
                    data: {
                        "type": '<?= request('type') ?>',
                        employer_id:<?= $user->employer_id ?>,
                        'user_id':<?= $user->id ?>
                    },
                    dataType: "html",
                    success: function (data) {
                        console.log(data);
                        $('#search_checked_table').show();
                        var ex = data;
                        var url = '<?= url('user/bulknametag?ids=') ?>';
                        var param = ex.split(",");
                        $('#nametag_link').attr('tags', param.join(","));
                        $('#nametag_link').attr('href', url + param.join(","));
                        console.log(param);

                    }
                });

            } else {
                //diselect all
                $('#search_checked_table').hide();
            }
        });
    };
    $(document).ready(toggle_all);
    $(document).ready(search_checked);
</script>
@endsection