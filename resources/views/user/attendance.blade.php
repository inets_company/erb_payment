@extends('layouts.app')

@section('content')
<!-- page start-->
<?php
$date = request('date') == null ? date('Y-m-d') : request('date');
?>
<div class="row">
    <div class="col-sm-12">

        <section class="panel">

            <header class="panel-heading">
                Attendances

            </header>
            <div class="row">
                <div class="col-md-3">
                    <div class="mini-stat clearfix">
                        <span class="mini-stat-icon orange"><i class="fa fa-gavel"></i></span>
                        <div class="mini-stat-info">
                            <span><?= \App\Model\User::count() ?></span>
                            Total Participants
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="mini-stat clearfix">
                        <span class="mini-stat-icon tar"><i class="fa fa-tag"></i></span>
                        <div class="mini-stat-info">
                            <span id="att_count"><?= \App\Model\Attendance::where('date', $date)->where('present', 1)->count() ?></span>
                            <?= date('Y M d', strtotime($date)) ?> Attendance
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="mini-stat clearfix">
                        <span class="">Barcode Attendance</span>
                        <div class="mini-stat-info">
                            <input type="text" autofocus="true" class="form-control" id="add_attendance"/>

                        </div>
                        <span id="attendance_search_loader"></span>
                    </div>
                </div>

            </div>
            <div class="panel-body">
<?php
 $dates = DB::select('select distinct created_at::date from attendances');
?>
                <div class="row">  
                    <div class="form-group ">
                        <label for="number" class="control-label col-lg-3 text-right">Dates</label>
                        <div class="col-lg-6">
                            <select class="form-control" id="sort_user" name="date">
                                <option value="<?= date('Y-m-d') ?>" <?= date('Y-m-d') == $date ? 'selected' : '' ?>>Today</option> 

                                <?php
                               
                                foreach ($dates as $att) {
                                    $at_date = date('Y-m-d', (strtotime($att->created_at)));
                                    ?>
                                    <option value="<?= $at_date ?>" <?= $at_date == $date ? 'selected' : '' ?>><?= $at_date?></option>
                                <?php } ?>

                            </select>

                        </div>
                    </div>
                </div>
                <br/>
                <section class="panel">
                    <header class="panel-heading tab-bg-dark-navy-blue ">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a data-toggle="tab" href="#home">Users</a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" href="#about">Attendance</a>
                            </li>
                            </li>
                        </ul>
                    </header>
                    <div class="panel-body">
                        <div class="tab-content">
                            <div id="home" class="tab-pane active">
                                <section id="unseen">
                                    <table class="table table-bordered table-striped table-condensed dataTable">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th class="numeric">Phone</th>
                                                <th class="numeric">Email</th>
                                                <th class="numeric">Type</th>
                                                <th class="numeric">Employer</th>
                                                <th class="numeric">Specialization</th>
                                                <th class="numeric col-sm-1">Action</th>
                                                <th><input type="checkbox" name="all" id="toggle_all"/><span id="search_status_loader"></span></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i = 1;
                                            ?>
                                            @foreach($applicants as $applicant)
                                            <tr  id="row<?= $applicant->id ?>">
                                                <td>{{$i}}</td>
                                                <td>{{$applicant->name}}</td>
                                                <td class="numeric">{{$applicant->phone}}</td>
                                                <td class="numeric">{{$applicant->email}}</td>
                                                <td>{{$applicant->userType->name}}</td>
                                                <td>{{$applicant->employer->name}}</td>
                                                <td>{{$applicant->profession->name}}</td>
                                                <td> 
                                                    <a href="<?= url('user/profile/' . $applicant->id) ?>" class="btn btn-xs btn-primary">View</a> &nbsp; &nbsp;


                                                </td>
                                                <td>
                                                    <?php
                                                    $method = "";
                                                    $att = $applicant->attendance()->where('date', $date)->first();
                                                    if (count($att) == 1 && $att->present == 1) {
                                                        $method = "checked";
                                                    }
                                                    ?>
                                                    <input type="checkbox" <?= $method ?> class="check" name="select[]" value="<?= $applicant->id ?>"/>
                                                    <span id="search_status_loader<?= $applicant->id ?>"></span>
                                                </td>
                                            </tr>
                                            <?php $i++; ?>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <?php //$applicants->appends(Illuminate\Support\Facades\Input::except('page'))->links() ?>
                                </section>
                            </div>
                            <div id="about" class="tab-pane">

                                <section id="unseen">
                                    <table class="table table-bordered table-striped table-condensed dataTable">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th class="numeric">Phone</th>
                                                <th class="numeric">Email</th>
                                                <th class="numeric">Type</th>
                                                <th class="numeric">Employer</th>
                                                <th class="numeric">Specialization</th>
                                                <th class="numeric col-sm-1">Action</th>
                                                <th><input type="checkbox" name="all" id="toggle_all"/><span id="search_status_loader"></span></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i = 1;
                                            ?>
                                            @foreach($applicants as $applicant)

                                            <?php
                                            $method = "";
                                            $attend = $applicant->attendance()->where('date', $date)->first();
                                            if (count($attend) == 1 && $attend->present == 1) {
                                                $method = "checked";
                                                ?>
                                                <tr  id="row<?= $applicant->id ?>">
                                                    <td>{{$i}}</td>
                                                    <td>{{$applicant->name}}</td>
                                                    <td class="numeric">{{$applicant->phone}}</td>
                                                    <td class="numeric">{{$applicant->email}}</td>
                                                    <td>{{$applicant->userType->name}}</td>
                                                    <td>{{$applicant->employer->name}}</td>
                                                    <td>{{$applicant->profession->name}}</td>
                                                    <td> 
                                                        <a href="<?= url('user/profile/' . $applicant->id) ?>" class="btn btn-xs btn-primary">View</a> &nbsp; &nbsp;


                                                    </td>
                                                    <td>

                                                        <input type="checkbox" <?= $method ?> class="check" name="select[]" value="<?= $applicant->id ?>"/>
                                                        <span id="search_status_loader<?= $applicant->id ?>"></span>
                                                    </td>
                                                </tr>
                                                <?php $i++;
                                            }
                                            ?>
                                            @endforeach
                                        </tbody>
                                    </table>
<?php //$applicants->appends(Illuminate\Support\Facades\Input::except('page'))->links()  ?>
                                </section>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </section>
    </div>
</div>
<!-- page end-->
<script type="text/javascript">
    let data = ''

    window.onload = function () {
        window.document.body.addEventListener('keydown', function (event) {
            if (event.keyCode == 13 || event.keyCode == 16 || event.keyCode == 17) {
                event.preventDefault();
                return;
            }

            if (event.ctrlKey) {
                event.preventDefault();
                return;
            }

            data += event.key;
            console.log(data);
        });
    }

    $('#add_attendance').keyup(function (e) {
        // e.preventDefault();
        if (e.which == 17 || e.which == 74 || e.which == 16) {
            e.preventDefault();
        } else {
            console.log(e.which);
        }
        if (e.keyCode == 13 || e.which == 16) {
            var pasteData = $(this).val();

            var att_count = parseInt($('#att_count').text());
            $.ajax({
                type: 'GET',
                url: "<?= url('user/barcodeAttendance') ?>",
                data: {
                    "s": pasteData,
                    'date': '<?= $date ?>'
                },
                dataType: "html ",
                beforeSend: function (xhr) {
                    $('#attendance_search_loader').html('<a href="#/refresh"><i class="fa fa-spinner"></i> </a>');
                },
                complete: function (xhr, status) {
                    // $('#attendance_search_loader').html('<span class="label label-success">' + status + '</span>');
                    $('#add_attendance').val('');
                },
                success: function (data) {
                    if (data == '<span class="label label-success">success</span>') {
                        $('#att_count').html(att_count + 1);
                    }
                    $('#attendance_search_loader').html('' + data + '');
                }
            });
        }
    });
    sort_user = function () {
        $('#sort_user').change(function () {
            var type = $(this).val();
            window.location.href = '<?= url()->current() ?>/?date=' + type;
        });
    }

    search_checked = function () {
        $('.check').click(function () {
            var value = $(this).val();
            var status = $(this).is(':checked');
            $.ajax({
                type: 'GET',
                url: "<?= url('user/addAttendance') ?>",
                data: {
                    "user_id": value, 'status': status, 'user': 'attendance', 'date': '<?= $date ?>'
                },
                dataType: "html",
                beforeSend: function (xhr) {
                    $('#search_status_loader' + value).html('<a href="#/refresh"><i class="fa fa-spinner"></i> </a>');
                },
                complete: function (xhr, status) {
                    $('#search_status_loader' + value).html('<span class="label label-success">' + status + '</span>');
                },
                success: function (data) {
                    //$('#search_status').html(data.message).removeClass().addClass('alert ' + data.alert_status);
                }
            });
        });
    }
    toggle_all = function () {
        $('#toggle_all').click(function () {
            var status = $(this).is(':checked');
            if ($("#toggle_all").prop('checked')) {
                $('.check').prop("checked", true);
            } else {
                $('.check').prop("checked", false);
            }
            $.ajax({
                type: 'GET',
                url: "<?= url('user/addBulkAttendance') ?>",
                data: {
                    "type": '<?= request('type') ?>', 'status': status, 'user': 'attendance', 'date': '<?= $date ?>'
                },
                dataType: "html",
                beforeSend: function (xhr) {
                    $('#search_status_loader').html('<a href="#/refresh"><i class="fa fa-spinner"></i> </a>');
                },
                complete: function (xhr, status) {
                    $('#search_status_loader').html('<span class="label label-success">' + status + '</span>');
                },
                success: function (data) {

                }
            });
        });
    };
    $(document).ready(toggle_all);
    $(document).ready(search_checked);
    $(document).ready(sort_user);
</script>
@endsection