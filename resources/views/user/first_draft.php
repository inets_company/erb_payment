<style type="text/css" media="print">
    .page
    {
        -webkit-transform: rotate(-90deg); 
        -moz-transform:rotate(-90deg);
        filter:progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
    }
</style>  <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="ERB">
<link rel="shortcut icon">
<!--Core CSS -->
<link href="{{ asset('public/bs3/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('public/js/jquery-ui/jquery-ui-1.10.1.custom.min.css') }}" rel="stylesheet">
<link href="{{ asset('public/css/bootstrap-reset.css') }}" rel="stylesheet">
<link href="{{ asset('public/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
<link href="{{ asset('public/js/jvector-map/jquery-jvectormap-1.2.2.css') }}" rel="stylesheet">
<link href="{{ asset('public/css/clndr.css') }}?v=1" rel="stylesheet">
<!--clock css-->
<link href="{{ asset('public/js/css3clock/css/style.css') }}?v=1" rel="stylesheet">
<!--Morris Chart CSS -->
<link rel="stylesheet" href="{{ asset('public/js/morris-chart/morris.css') }}?v=1">
<!-- Custom styles for this template -->
<link href="{{ asset('public/css/style.css') }}?v=2" rel="stylesheet">
<link href="{{ asset('public/css/style-responsive.css') }}?v=1" rel="stylesheet"/>

<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]>
<script src="js/ie8-responsive-file-warning.js"></script><![endif]-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
<title>{{ config('app.name', 'ERB') }}</title>

<!-- Scripts -->

<!-- Fonts -->
<link rel="dns-prefetch" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
<script src="{{ asset('public/js/jquery.js') }}"></script>
<style>
    h4{
        font-size: 1.2vw; color: black
    }
</style>
<div class="row">
    <div class="">

        <!--widget start-->
        <?php
        foreach ($users as $user) {
            $barcode = (new \App\Http\Controllers\SettingController())->createBarCode($user->id);
            ?>
            <div class="col-md-3 col-lg-3" style="max-height: 38em; min-height: 38em; max-width: 30em; overflow: hidden; margin-bottom: 1em; margin-left:10px;"> 
                <aside class="profile-nav alt" style="border: 4px solid #000000; font-size: 100%">
                    <section class="panel">
                        <div class="user-heading alt">
                            <h5 style="color: black; font-weight: bolder; text-align: center">The United Republic of Tanzania<br/>
                                <?= strtoupper($setting->name) ?></h5>

                            <img style="float: left" src="{{ url('public/images/country.jpg')}}" width="50" height="50">

                            <img style="float: right" src="{{ url('public/images/erb.png')}}" width="50" height="50" alt=                                        "">
                            <div style="float: clear"></div>

                        </div>

                        <ul class="nav nav-pills nav-stacked" id="inner_text" >
                            <li class="text-center" style="border: 1px solid #000 !important;"><a href="javascript:;"> <i class="fa fa-user"></i> <b style="font-size: 180%; color: #0000FF !important; font-weight: bolder"><h4 style="z-index: 1;color: #0000FF !important;font-weight: bolder;"><?= strtoupper($user->name) ?></h4></b></a></li>
                            <li class="text-center"><a href="javascript:;" style="color: #FF0000 !important;
    font-weight: bolder"> <i class="fa fa-tasks"></i> {{$user->employer->name}}</a></li>
                            <li><a href="javascript:;" style="text-align: center"><img class="" src="data:image/png;base64,<?= $barcode ?>" /></a></li>
                            <li><a href="javascript:;" style="text-align: center; color: #000000; font-weight: bolder; font-size: 12px"> <i class="fa fa-marker"></i> <?= $event->description . '<br/>' . $event->location ?> <span class="badge label-warning pull-right r-activity"></span></a></li>
                        </ul>

                    </section>
                </aside>
                <!--widget end-->
            </div>
        <?php } ?>

    </div>

</div>
<script>
    causeRepaintsOn = $("h4");

$(window).resize(function() {
  causeRepaintsOn.css("z-index", 1);
});
window.print();
</script>

