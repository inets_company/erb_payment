<style type="text/css">

    .nametag{

        background: #ffffff;
        text-align: center;


    }
    .username{
        border-bottom: 1px solid #ccc;
        margin-bottom: 2em;
    }
    .tag{
        border: 4px solid #000000; 
        width: 15em !important;
    }
    body{
        margin: 0; padding: 0;
    }
</style> 
<table>
    <tbody>

        <?php
        $i = 0;
        foreach ($users as $user) {
            $barcode = (new \App\Http\Controllers\SettingController())->createBarCode($user->id);
            if ($i % 4 == 0 || $i == 0) {
                echo '<tr id="' . $i . '">';
            }
            ?>

        <td class="tag" id="<?= $i ?>">
            <table class="nametag" style="">
                <thead>
                    <tr>
                        <th style="padding:3px; color: black; font-weight: bolder; text-align: center">   
                            The United Republic of Tanzania<br/>
                            <?= strtoupper($setting->name) ?>
                        </th>
                    </tr>
                    <tr>
                        <th style="padding: 1em"> <img style="float: left" src="{{ url('public/images/country.jpg')}}" width="50" height="50"> <img style="float: right" src="{{ url('public/images/erb.png')}}" width="50" height="50" alt=                                        ""></th>

                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="username">   <i class="fa fa-user"></i> <b style="font-size: 180%; color: #0000FF !important; font-weight: bolder"><h4 style="z-index: 1;color: #0000FF !important;font-weight: bolder;    font-size: 65%;"><br/><?= strtoupper($user->name) ?></h4></b></td>
                    </tr>
                    <tr>
                        <td  class="username"><b style="color: #FF0000 !important;
                                                 font-weight: bolder"> <i class="fa fa-tasks"></i>  <?php
                                    if ($user->userType->id == 13) {
                                       echo   strtoupper($user->profession->name); 
                                    } else {
                                      echo   strtoupper($user->employer->name) ;
                                    }
                                 
                                    ?></b></td>
                    </tr>
                    <tr>
                        <td  class="username"> <b style="text-align: center"><img class="" src="data:image/png;base64,<?= $barcode ?>" width="60%" height="60%" /></b></td>
                    </tr>
                    <tr>
                        <td>  <i class="fa fa-marker"></i> <?= $event->description . '<br/>' . $event->location ?> <span class="badge label-warning pull-right r-activity"></span></td>
                    </tr>
                </tbody>
            </table>

        </td>

        <?php
        $i++;
    }
    $p = 0;
    if ($i == 1) {
        $p = 3;
    } else if ($i == 2) {
        //2
        $p = 2;
    } else if ($i == 3) {
        $p = 1;
    }
    for ($x = 1; $x <= $p; $x++) {
        ?>
        <td class="tag" id=""><table class="nametag" style=""></table></td>
        <?php } ?>
</tbody>
</table>



