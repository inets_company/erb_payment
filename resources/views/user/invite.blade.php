@extends('layouts.app')

@section('content')
<!-- page start-->

<div class="row">
    <div class="col-sm-12">

        <section class="panel">

            <header class="panel-heading">
                Invitee

            </header>
                <?php if (can_access('add_users')) { ?>
            <p><br/>&nbsp;&nbsp;&nbsp;<a class="btn btn-success" data-toggle="modal" href="#myModal">
                    Add  New User
                </a></p>
             <?php }?>
            <div class="panel-body">
                <div class="row"  id="search_checked_table" style="display:none">
                    <a type="button" target="_blank" tags='' id="nametag_link" href="" value="" name="" class="btn btn-sm btn-danger"><i class="fa fa-cloud"></i> Print NameTag</a>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Number</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr  id="search_checked"></tr>
                        </tbody>
                    </table>
                </div>
         
                <br/>
                <section id="unseen">
                    <table class="table table-bordered table-striped table-condensed dataTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th class="numeric">Phone</th>
                                <th class="numeric">Email</th>
                                <th class="numeric">Type</th>
                                <th class="numeric">Employer</th>
                                <th class="numeric">Specialization</th>
                                <th class="numeric col-sm-4">Action</th>
                                <th><input type="checkbox" name="all" id="toggle_all"/></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            ?>
                            @foreach($applicants as $applicant)
                            <tr  id="row<?= $applicant->id ?>">
                                <td>{{$i}}</td>
                                <td>{{$applicant->name}}</td>
                                <td class="numeric">{{$applicant->phone}}</td>
                                <td class="numeric">{{$applicant->email}}</td>
                                <td>{{$applicant->userType->name}}</td>
                                <td>{{$applicant->employer->name}}</td>
                                <td>{{$applicant->profession->name}}</td>
                                <td> 
                                    <?=can_access('delete_users')? btn_delete('user/' . $applicant->id, 'user'):'' ?>
                                    <a href="<?= url('user/profile/' . $applicant->id) ?>" class="btn btn-xs btn-primary">View</a> &nbsp; &nbsp;
                                     <?php if (can_access('edit_users')) { ?>
                                    <a data-toggle="modal" href="#myModal" onmousedown="open_edit_model('<?= $applicant->id ?>', '<?= url('setting/' . $applicant->id . '/edit') ?>')" class="btn btn-xs btn-info">Edit</a> 
                                     <?php }?>
                                    
                                    <?php if ($applicant->is_employer <> 1) { ?>
                                        <a href="<?= url('user/ticket/' . $applicant->id) ?>" class="btn btn-xs btn-warning">Barcode</a> &nbsp; &nbsp;

                                        <a href="<?= url('user/nametag/' . $applicant->id) ?>" class="btn btn-xs btn-default">Tag</a>
                                    <?php } ?>
                                </td>
                                <td><input type="checkbox" class="check" name="select[]" value="<?= $applicant->id ?>"/></td>
                            </tr>
                            <?php $i++; ?>
                            @endforeach
                        </tbody>
                    </table>
                    <?php //$applicants->appends(Illuminate\Support\Facades\Input::except('page'))->links() ?>
                </section>
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <form class="cmxform form-horizontal " id="commentForm" method="post" action="<?= url('user') ?>">

                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="title_page">Add New User</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="panel-body">
                                        <div class="form-group ">
                                            <label for="type" class="control-label col-lg-3">Employer</label>
                                            <div class="col-lg-6">
                                                <select class="form-control"  name="employer_id" id="employer_id">
                                                    <option value=""></option> 
                                                    <?php $userype = \App\Model\Employer::orderBy('name')->get() ?>
                                                    @foreach ($userype as $type)
                                                    <option value="{{$type->id}}">{{$type->name}}</option>                                                  @endforeach;
                                                </select>

                                            </div>
                                            <?php echo form_error($errors, 'employer_id'); ?>
                                        </div>
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-3">Name (required)</label>
                                            <div class="col-lg-6">
                                                <input class=" form-control" id="name" name="name" minlength="2" type="text" required="" value="{{old('name')}}" pattern="[a-zA-Z\. ]{5,}"  onblur="this.value = this.value.toUpperCase()">
                                            </div>
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group " style="display:none">
                                            <label for="cemail" class="control-label col-lg-3">E-Mail</label>
                                            <div class="col-lg-6">
                                                <input class="form-control " id="email" type="email" name="email" required=""  value="<?= time().'@erb.go.tz'?>"  onblur="this.value = this.value.toLowerCase()">
                                            </div>
                                            @if ($errors->has('email'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group " style="display:none">
                                            <label for="phone" class="control-label col-lg-3">Phone Number</label>
                                            <div class="col-lg-6">
                                                <input class="form-control " id="phone" type="text" name="phone"  value="<?= time()?>">
                                            </div>
                                            @if ($errors->has('phone'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group ">
                                            <label for="number" class="control-label col-lg-3">Category (Required)</label>
                                            <div class="col-lg-6">
                                                <select class="form-control" name="profession_id" id="profession_id">
                                                    <option value=""></option>                                     
                                                    <?php $professions = App\Model\Profession::where('invitee',1)->orderBy('name')->get(); ?>
                                                    @foreach ($professions as $profession)
                                                    <option value="{{$profession->id}}">{{$profession->name}}</option>                                                  @endforeach;
                                                </select>

                                            </div>
                                            <?php echo form_error($errors, 'profession_id'); ?>
                                            <!--<a data-toggle="modal" href="#myModal">Or Add new</a>-->
                                        </div>




                                    </div>


                                </div>
                                <div class="modal-footer">
                                    <?= csrf_field() ?>
                                    <input type="hidden" name="user" value="user"/>
                                    <input type="hidden" name="user_type_id" value="13"/>
                                    <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                    <button class="btn btn-success" type="submit">Save changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<!-- page end-->
<script type="text/javascript">

    open_edit_model = function (a, b) {
        $.ajax({
            type: 'POST',
            url: "<?= url('setting/getedit') ?>",
            data: {
                "id": a,
                "table": "user"
            },
            dataType: "json",
            success: function (data) {
                $('#title_page').html('Edit user type');
                $('#commentForm').attr('action', b);
                $("#commentForm").attr("method", "get");
                $.each(data, function (i, item) {
                    $('#' + i).val(item);
                });
            }
        });
    }
    reset_form = function () {
        $('#title_page').html('Add New User Type');
        $('#commentForm').attr('action', '<?= url('user') ?>');
        $("#commentForm").attr("method", "post");
        $("input:not(:hidden)").val('');
        $('.delete').html('Delete');
    }
    sort_user = function () {
        $('#sort_user').change(function () {
            var type = $(this).val();
            window.location.href = '<?= url()->current() ?>/?user_type=' + type;
        });
    }

    search_checked = function () {
        $('.check').click(function () {
            var value = $(this).val();
            var status = $(this).is(':checked');
            if (status === true) {
                var text = $('#row' + value).html();
                $('#search_checked_table').show();
                $('#search_checked').after('<tr id="s_table' + value + '">' + text + '</tr>');
                var ex = $('#nametag_link').attr('tags');
                var url = '<?= url('user/bulknametag?ids=') ?>';
                var param = ex.split(",");
                param.push(value);
                $('#nametag_link').attr('tags', param.join(","));
                $('#nametag_link').attr('href', url + param.join(","));
                console.log(param);

            } else {
                var ex = $('#nametag_link').attr('tags');
                var url = '<?= url('user/bulknametag?ids=') ?>';
                var param = ex.split(",");
                param = jQuery.grep(param, function (val) {
                    return val != value;
                });
                var arr = param;

                var result = arr.filter(function (elem) {
                    return elem != value;
                });
                console.log(result);

                $('#nametag_link').attr('tags', result.join(","));
                $('#nametag_link').attr('href', url + result.join(","));
                $('#s_table' + value).remove();
            }
        });
    }
    toggle_all = function () {
        $('#toggle_all').click(function () {
            var status = $(this).is(':checked');
            if ($("#toggle_all").prop('checked')) {
                $('.check').prop("checked", true);
            } else {
                $('.check').prop("checked", false);
            }
            if (status === true) {
                //select all

                $.ajax({
                    type: 'GET',
                    url: "<?= url('user/getApplicants') ?>",
                    data: {
                        "type": 13,
                    },
                    dataType: "html",
                    success: function (data) {
                        console.log(data);
                        $('#search_checked_table').show();
                        var ex = data;
                        var url = '<?= url('user/bulknametag?ids=') ?>';
                        var param = ex.split(",");
                        $('#nametag_link').attr('tags', param.join(","));
                        $('#nametag_link').attr('href', url + param.join(","));
                        console.log(param);

                    }
                });

            } else {
                //diselect all
                $('#search_checked_table').hide();
            }
        });
    };
    $(document).ready(toggle_all);
    $(document).ready(search_checked);
    $(document).ready(sort_user);
</script>
@endsection