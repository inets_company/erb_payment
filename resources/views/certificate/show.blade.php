@extends('layouts.app')

@section('content')
<!-- page start-->
<div class="row">
    <section class="panel">
        <header class="panel-heading option">
            NameTag
        </header>
        <div class="panel-body option">
            <button type="button" class="btn btn-xs btn-default" onclick="print_tag()"><i class="fa fa-print"></i>Print</button>
            <a type="button" href="<?= url('certificate/printall') ?>" class="btn btn-xs btn-primary"><i class="fa fa-print"></i>Print All</a>
        
            <p align="right"> 
                <button type="button" class="btn btn-xs btn-info"><i class="fa fa-gear"></i>Settings</button></p>
        </div>
        <div class="col-lg-offset-2">
            <div style="width:800px; height:600px; padding:20px; text-align:center; border: 10px solid #787878">
                <div style="width:750px; height:550px; padding:20px; text-align:center; border: 5px solid #787878">
                    <span style="font-size:50px; font-weight:bold">Certificate of Attendance</span>
                    <br><br>
                    <span style="font-size:25px"><i>This is to certify that</i></span>
                    <br><br>
                    <span style="font-size:30px"><b>{{$user->name}}</b></span><br/><br/>
                    <span style="font-size:25px"><i>Participated in </i></span> <br/><br/>
                    <span style="font-size:30px"><?=$event->name?></span> <br/><br/>
                    <span style="font-size:20px">and earned  points  <b>3</b></span> <br/><br/><br/><br/>
                    <span style="font-size:25px"><i>Dated</i></span><br>
                    
                    <span style="font-size:30px"><?=$event->description?></span>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    function print_tag() {
        $('.header, .pdf_btn, .print_btn,.option').hide();
        window.print();
        $('.header, .pdf_btn, .print_btn, .option').show();
    }
</script>
@endsection