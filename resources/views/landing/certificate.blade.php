<?php
$root = 'public/front/';
$event = \App\Model\Event::first();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- ========== Meta Tags ========== -->
        <meta charset="UTF-8">
        <meta name="description" content="ERB, Engineers Registration Board, Engineers Day, INETS Company Limited">
        <meta name="keywords" content="ERB, Engineers Registration Board, Engineers Day, INETS Company Limited">
        <meta name="author" content="INETS Company Limited">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- ========== Title ========== -->
        <title> ERB - Engineers Day</title>
        <!-- ========== Favicon Ico ========== -->
        <!--<link rel="icon" href="fav.ico">-->
        <!-- ========== STYLESHEETS ========== -->
        <!-- Bootstrap CSS -->
        <link href="<?= $root ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <!-- Fonts Icon CSS -->
        <link href="<?= $root ?>/assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?= $root ?>/assets/css/et-line.css" rel="stylesheet">
        <link href="<?= $root ?>/assets/css/ionicons.min.css" rel="stylesheet">
        <!-- Carousel CSS -->
        <link href="<?= $root ?>/assets/css/owl.carousel.min.css" rel="stylesheet">
        <link href="<?= $root ?>/assets/css/owl.theme.default.min.css" rel="stylesheet">
        <!-- Animate CSS -->
        <link rel="stylesheet" href="<?= $root ?>/assets/css/animate.min.css">
        <!-- Custom styles for this template -->
        <link href="<?= $root ?>/assets/css/main.css" rel="stylesheet">
        <link href="{{ asset('public/css/bootstrap-tour.min.css')}}" rel="stylesheet">
    </head>
    <body>
        <div class="loader">
            <div class="loader-outter"></div>
            <div class="loader-inner"></div>
        </div>

        <!--header starts here -->
        <header class="header navbar fixed-top navbar-expand-md">
            <div class="container">
                <a class="navbar-brand logo" href="#">
                    <img src="<?= $root ?>/assets/img/logo.png" alt="Evento"/>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#headernav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="lnr lnr-text-align-right"></span>
                </button>
                <div class="collapse navbar-collapse flex-sm-row-reverse" id="headernav create_booking">
                    <ul class=" nav navbar-nav menu">
                        <li class="nav-item">
                            <a class="nav-link active" href="#">Home</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link btn btn-primary " href="<?= url('login') ?>">Staff Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        <!--header end here-->


        <!--event Instructions and Information-->
        <div id="payment" class="pb100">

        </div>
        <section class="pb100" >
            <div class="container">
                <div class="table-responsive">
                    <table class="event_calender table">

                        <thead class="event_title">
                            <tr>
                                <th>
                                    <span>Event Certificate</span>
                                </th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>

                                <td class="event_date">
                                    <h4 class="modal-title">Thanks for attending 2018 AED Event</h4>
                                </td>
                                <td>
                                    <div class="event_place">
                                        <p>Enter your phone number or email below to get your certificate of attendance</p>
                                    </div>
                                </td>


                            </tr>

                        </tbody>
                    </table>
                </div>
                <div class="alert clearfix" style="border: 1px solid #cccccc">
                    <span class="alert-icon"><i class="fa fa-search"></i></span>
                    <div class="notification-info">

                        <p>
                            Just enter your email or phone below and we will find it
                        </p>
                    </div>
                  
                    <div class="col-lg-12" id="search_box">
                        <form role="form" action="<?= url('search') ?>">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <input type="text" name="tag" required="required" class="form-control col-lg-6" id="exampleInputEmail1" placeholder="Enter email or phone"></div>
                                    <div class="col-lg-4">
                                        @csrf
                                        <button type="button" id="search_button" class="btn btn-search">Search</button>
                                    </div>
                                </div>
                            </div>


                        </form>
                         <span id="search_status_loader"></span> <div id="search_status"></div>
                    </div>
                </div>


            </div>

        </section>
        <!--event Instructions and Information end -->
        <div class="copyright_footer">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-6 col-12">
                        <p>
                            Copyright &copy; <script>document.write(new Date().getFullYear());</script> All rights reserved | By <a href="http://inetstz.com" target="_blank">INETS</a>
                        </p>
                    </div>
                    <div class="col-12 col-md-6 ">
                        <ul class="footer_menu">
                            <li>
                                <a href="#">Home</a>
                            </li>

                 

                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--footer end -->

        <!-- jquery -->
        <script src="<?= $root ?>/assets/js/jquery.min.js"></script>
        <!-- bootstrap -->
        <script src="<?= $root ?>/assets/js/popper.js"></script>
        <script src="<?= $root ?>/assets/js/bootstrap.min.js"></script>
        <script src="<?= $root ?>/assets/js/waypoints.min.js"></script>
        <!--slick carousel -->
        <script src="<?= $root ?>/assets/js/owl.carousel.min.js"></script>
        <!--parallax -->
        <script src="<?= $root ?>/assets/js/parallax.min.js"></script>
        <!--Counter up -->
        <script src="<?= $root ?>/assets/js/jquery.counterup.min.js"></script>
        <!--Counter down -->
        <script src="<?= $root ?>/assets/js/jquery.countdown.min.js"></script>
        <!-- WOW JS -->
        <script src="<?= $root ?>/assets/js/wow.min.js"></script>
        <!-- Custom js -->
        <script src="<?= $root ?>/assets/js/main.js"></script>
        <script src="{{ asset('public/js/bootstrap-tour.min.js') }}"></script>
        <script type="text/javascript">
                                $.ajaxSetup({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    }
                                });
                                var root_url = "<?= url('/'); ?>";

        </script>
        <script type="text/javascript">
            search_button = function () {
                $('#search_button').mousedown(function () {
                    var input = $('#exampleInputEmail1').val();
                    $.ajax({
                        type: 'GET',
                        url: "<?= url('search_certificate') ?>",
                        data: {
                            "tag": input
                        },
                        dataType: "json",
                        beforeSend: function (xhr) {
                            $('#search_status_loader').html('<a href="#/refresh"><i class="fa fa-spinner"></i> </a>');
                        },
                        complete: function (xhr, status) {
                            $('#search_status_loader').html('<span class="label label-success"><b>' + status + '</b></span>');
                        },
                        success: function (data) {
                            $('#search_status').html(data.message).removeClass().addClass('alert ' + data.alert_status);
                        }
                    });
                });
            }
            $(document).ready(search_button);
        </script>
    </body>
</html>
