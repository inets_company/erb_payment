<?php
$root = 'public/front/';
$event = \App\Model\Event::first();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- ========== Meta Tags ========== -->
        <meta charset="UTF-8">
        <meta name="description" content="ERB, Engineers Registration Board, Engineers Day, INETS Company Limited">
        <meta name="keywords" content="ERB, Engineers Registration Board, Engineers Day, INETS Company Limited">
        <meta name="author" content="INETS Company Limited">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- ========== Title ========== -->
        <title> ERB - Engineers Day</title>
        <!-- ========== Favicon Ico ========== -->
        <!--<link rel="icon" href="fav.ico">-->
        <!-- ========== STYLESHEETS ========== -->
        <!-- Bootstrap CSS -->
        <link href="<?= $root ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <!-- Fonts Icon CSS -->
        <link href="<?= $root ?>/assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?= $root ?>/assets/css/et-line.css" rel="stylesheet">
        <link href="<?= $root ?>/assets/css/ionicons.min.css" rel="stylesheet">
        <!-- Carousel CSS -->
        <link href="<?= $root ?>/assets/css/owl.carousel.min.css" rel="stylesheet">
        <link href="<?= $root ?>/assets/css/owl.theme.default.min.css" rel="stylesheet">
        <!-- Animate CSS -->
        <link rel="stylesheet" href="<?= $root ?>/assets/css/animate.min.css">
        <!-- Custom styles for this template -->
        <link href="<?= $root ?>/assets/css/main.css" rel="stylesheet">
        <link href="{{ asset('public/css/bootstrap-tour.min.css')}}" rel="stylesheet">
    </head>
    <body>
        <div class="loader">
            <div class="loader-outter"></div>
            <div class="loader-inner"></div>
        </div>

        <!--header starts here -->
        <header class="header navbar fixed-top navbar-expand-md">
            <div class="container">
                <a class="navbar-brand logo" href="#">
                    <img src="<?= $root ?>/assets/img/logo.png" alt="Evento"/>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#headernav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="lnr lnr-text-align-right"></span>
                </button>
                <div class="collapse navbar-collapse flex-sm-row-reverse" id="headernav create_booking">
                    <ul class=" nav navbar-nav menu">
                        <li class="nav-item">
                            <a class="nav-link active" href="#">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " href="#payment">Payment Instructions</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link btn btn-primary" href="<?= url('booking') ?>">Book Here</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " href="<?= url('login') ?>">Staff Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        <!--header end here-->

        <!--sliders section slider -->
        <section id="home" class="home-cover">
            <div class="cover_slider owl-carousel owl-theme">
                <div class="cover_item" style="background: url('<?= $root ?>/assets/img/bg/slider.jpg');">
                    <div class="slider_content">
                        <div class="slider-content-inner">
                            <div class="container">
                                <div class="slider-content-center">
                                    <h2 class="cover-title">
                                        Engineers Registration  
                                    </h2>
                                    <strong class="cover-xl-text">Board</strong>
                                    <p class="cover-date">
                                        <?= $event->description ?>  - <?= $event->location . ' <br/>' ?>


                                    </p>
                                    <a href="<?= url('booking') ?>" class=" btn btn-primary " >
                                        Book Now
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="cover_item" style="background: url('<?= $root ?>/assets/img/bg/bg-img2.jpg');">
                    <div class="slider_content">
                        <div class="slider-content-inner">
                            <div class="container">
                                <div class="slider-content-left">
                                    <h2 class="cover-title">
                                       AQRB, CRB AND ERB 
                                    </h2>
                                    <strong class="cover-xl-text"> </strong>
                                    <p class="cover-date">
                                        <b>CONSULTATIVE MEETING AND EXHIBITION -2019</b><br>
                                        
Wajibu wa wadau wa Sekta ya Ujenzi katika kufikia uchumi wa viwandaendelevu na ustawi wa jamii 

                                
                                    </p>
                                    <a href="<?= url('booking') ?>" class=" btn btn-primary " >
                                        Book Now
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="cover_item" style="background: url('<?= $root ?>/assets/img/bg/bg-img.jpg');">
                    <div class="slider_content">
                        <div class="slider-content-inner">
                            <div class="container">
                                <div class="slider-content-center">
                                    <h2 class="cover-title">
                                        Prepare yourself for the
                                    </h2>
                                    <strong class="cover-xl-text">AQRB, CRB AND ERB </strong>
                                    <p class="cover-date">
   
                                        <b>CONSULTATIVE MEETING AND EXHIBITION</b><br>
29TH – 30TH AUGUST 2019 – AGA KHAN DIAMOND JUBILEE HALL, DAR ES SALAAM, TANZANIA

                                    </p>
                                    <a href="<?= url('booking') ?>" class=" btn btn-primary " >
                                        Book Now
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cover_nav">
                <ul class="cover_dots">
                    <li class="active" data="0"><span>1</span></li>
                    <li  data="1"><span>2</span></li>
                    <li  data="2"><span>3</span></li>
                </ul>
            </div>
        </section>
        <!--sliders section slider end -->

        <!--event info -->
        <section class="pt100 pb100">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-6 col-md-3  ">
                        <div class="icon_box_two">
                            <?php
                            $day = $event->date;

// add 1 days to the date above
                            $NewDate = date('Y-m-d', strtotime($day . " +1 days"));
                            ?>

                            <a title="Add to my calender" href="https://www.google.com/calendar/render?action=TEMPLATE&text=<?= $event->name ?>&dates=<?= date('Ymd', strtotime($event->date)) ?>T224000Z/<?= date('Ymd', strtotime($NewDate)) ?>T221500Z&details=<?= $event->theme ?>.. For+more+details,+click+here:+http://engineersday.co.tz&location=<?= $event->location ?>&sf=true&output=xml" target="_blank"> <i class="ion-ios-calendar-outline"  id="google_calender"></i></a>
                            <div class="content">
                                <h5 class="box_title">
                                    DATE
                                </h5>
                                <p>
                                    <?= $event->description ?>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-6 col-md-3  ">
                        <div class="icon_box_two">
                            <i class="ion-ios-location-outline"></i>
                            <div class="content">
                                <h5 class="box_title">
                                    location
                                </h5>
                                <p>
                                    <?= $event->location ?>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-6 col-md-3  ">
                        <div class="icon_box_two">
                            <i class="ion-ios-person-outline"></i>
                            <div class="content">
                                <h5 class="box_title">
                                    Theme
                                </h5>
                                <p>
                                    <?= $event->theme ?>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-6 col-md-3  ">
                        <div class="icon_box_two">
                            <i class="ion-ios-pricetag-outline"></i>
                            <div class="content">
                                <h5 class="box_title">
                                    Tickets
                                </h5>
                                <p>
                                    Tsh <?= number_format($fee->amount) ?>/= early booking
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--event info end -->


        <!--event countdown -->
        <section class="bg-img pt70 pb70" style="background-image: url('<?= $root ?>/assets/img/bg/bg2-img.jpg');">
            <div class="overlay_dark"></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10">
                        <h4 class="mb30 text-center color-light">Time left until the event</h4>
                        <div class="countdown"></div>
                    </div>
                </div>
            </div>
        </section>
        <!--event count down end-->


        <!--about the event -->
        <section class="pt100 pb100">
            <div class="container">
                <div class="section_title">
                    <h3 class="title">
                        Registration Fee
                    </h3>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-4 col-12">

                    </div>
                    <div class="col-md-4 col-12">
                        <div class="price_box active">
                            <div class="price_highlight">
                                recommended
                            </div>
                            <div class="price_header">
                                <h4>
                                    Early Bird
                                </h4>
                                <h6>
                                    For the early bookings
                                </h6>
                            </div>
                            <div class="price_tag">
                                <?= number_format($fee->amount) ?> <sup> Tsh</sup>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-4 col-12">
                        <div class="price_box">
                            <div class="price_header">
                                <h4>
                                    Later
                                </h4>
                                <h6>
                                    After  <?= date('d M Y', strtotime($fee->end_date)) ?>
                                </h6>
                            </div>
                            <div class="price_tag">
                                <?= number_format($fee->amount + $fee->penalty_amount) ?> <sup>Tsh</sup>
                            </div>

                            <!-- <div class="price_footer">
                                 <a href="#" class="btn btn-primary ">BOOK NOW</a>
                             </div>-->
                        </div>
                    </div>
                </div>

                <!--event features-->

                <!--event features end-->
            </div>
        </section>
        <!--about the event end -->


        <!--event Instructions and Information-->
        <div id="payment" class="pb100">

        </div>
        <section class="pb100" >
            <div class="container">
                <div class="table-responsive">
                    <table class="event_calender table">

                        <thead class="event_title">
                            <tr>
                                <th>
                                    <i class="ion-ios-help-outline"></i>
                                    <span>Payment Instructions</span>
                                </th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>

                                <td class="event_date">
                                    <h4 class="modal-title">How it Works</h4>
                                </td>
                                <td>
                                    <div class="event_place">
                                        <p>To participate in the event, you need to book for an event by registering your name, phone number and email. Once you register, you will receive a booking reference (invoice) number which you will use to make payments for the event</p>
                                    </div>
                                </td>
                                <td>
                                    <a href="#" class="btn btn-primary ">BOOK NOW</a>
                                </td>

                            </tr>
                            <tr>

                                <td class="event_date">
                                    <h4 class="modal-title">Control Number</h4>
                                </td>
                                <td colspan="2">
                                    <div class="event_place">
                                        <p>This is a special unique number that will be used to identify your payments. Each payment must be associated with unique Control number.</p>
                                    </div>
                                </td>


                            </tr>
                            <tr>

                                <td class="event_date">
                                    <h4 class="modal-title">How to Make Payment</h4>
                                </td>
                                <td colspan="2">
                                    <div class="event_place">
                                        <p>Once you have your booking reference (invoice) number, you can choose any available payment channels to make your payments. Existing channels are M-pesa, Tigo-Pesa , Airtel Money.For Banks,Visit any branch or bank agent of NMB, CRDB, NBC with your control number obtained from the system. Kindly specify only reference number on making payment</p>
                                        <table>
                                            <thead>
                                                <tr>
                                                    <th>   <img src="public/images/mpesa.jpg" width="158" height="80" alt="M-pesa"></th>
                                                    <th>   <img src="public/images/tigo.jpg" width="158" height="80"  alt="Tigo-pesa"></th>
                                                    <th>   <img src="public/images/airtel.jpg" width="158" height="80"  alt="Airtel Money"></th>
                                                    <th>   <img src="public/images/nmb.jpg" width="158" height="80"  alt="NMB Bank"></th>
                                                    <th>   <img src="public/images/crdb.jpg" width="158" height="80"  alt="CRDB Bank"></th>

                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </td>


                            </tr>

                        </tbody>
                    </table>
                </div>
                <div class="alert clearfix" style="border: 1px solid #cccccc">
                    <span class="alert-icon"><i class="fa fa-search"></i></span>
                    <div class="notification-info">
                        <ul class="clearfix notification-meta">
                            <li class="pull-left notification-sender"><b>booked but lost your Reference ID ? </b></li>

                        </ul>
                        <p>
                            Just enter your email or phone below and we will find it
                        </p>
                    </div>
                    <span id="search_status_loader"></span><div id="search_status"></div>
                    <div class="col-lg-12" id="search_box">
                        <form role="form" action="<?= url('search') ?>">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email address or Phone</label>
                                <div class="row">
                                    <div class="col-lg-8">
                                        <input type="text" name="tag" required="required" class="form-control col-lg-6" id="exampleInputEmail1" placeholder="Enter email or phone"></div>
                                    <div class="col-lg-4">
                                        @csrf
                                        <button type="button" id="search_button" class="btn btn-search">Search</button>
                                    </div>
                                </div>
                            </div>


                        </form>
                    </div>
                </div>


            </div>

        </section>
        <!--event Instructions and Information end -->
        <div class="copyright_footer">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-6 col-12">
                        <p>
                            Copyright &copy; <script>document.write(new Date().getFullYear());</script> All rights reserved | By <a href="http://inetstz.com" target="_blank">INETS</a>
                        </p>
                    </div>
                    <div class="col-12 col-md-6 ">
                        <ul class="footer_menu">
                            <li>
                                <a href="#">Home</a>
                            </li>

                            <li>
                                <a href="#payment">Payment Instructions</a>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--footer end -->

        <!-- jquery -->
        <script src="<?= $root ?>/assets/js/jquery.min.js"></script>
        <!-- bootstrap -->
        <script src="<?= $root ?>/assets/js/popper.js"></script>
        <script src="<?= $root ?>/assets/js/bootstrap.min.js"></script>
        <script src="<?= $root ?>/assets/js/waypoints.min.js"></script>
        <!--slick carousel -->
        <script src="<?= $root ?>/assets/js/owl.carousel.min.js"></script>
        <!--parallax -->
        <script src="<?= $root ?>/assets/js/parallax.min.js"></script>
        <!--Counter up -->
        <script src="<?= $root ?>/assets/js/jquery.counterup.min.js"></script>
        <!--Counter down -->
        <script src="<?= $root ?>/assets/js/jquery.countdown.min.js"></script>
        <!-- WOW JS -->
        <script src="<?= $root ?>/assets/js/wow.min.js"></script>
        <!-- Custom js -->
        <script src="<?= $root ?>/assets/js/main.js"></script>
        <script src="{{ asset('public/js/bootstrap-tour.min.js') }}"></script>
        <script type="text/javascript">
 
                                $.ajaxSetup({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    }
                                });
                                var root_url = "<?= url('/'); ?>";

        </script>
        <script type="text/javascript">
            search_button = function () {
                $('#search_button').mousedown(function () {
                    var input = $('#exampleInputEmail1').val();
                    $.ajax({
                        type: 'GET',
                        url: "<?= url('search') ?>",
                        data: {
                            "tag": input
                        },
                        dataType: "json",
                        beforeSend: function (xhr) {
                            $('#search_status_loader').html('<a href="#/refresh"><i class="fa fa-spinner"></i> </a>');
                        },
                        complete: function (xhr, status) {
                            $('#search_status_loader').html('<span class="label label-success">' + status + '</span>');
                        },
                        success: function (data) {
                            $('#search_status').html(data.message).removeClass().addClass('alert ' + data.alert_status);
                        }
                    });
                });
            }
            $(document).ready(search_button);
            // Instance the tour
//            var tour = new Tour({
//                steps: [
//                    {
//                        element: "#create_booking",
//                        title: "Get your Invoice Number",
//                        content: "Click here to get your reference number. You will need this number to pay via Banks, M-pesa, Tigo-pesa and Airtel Money",
//                        placement: 'top', backdrop: true,
//                        backdropContainer: 'body',
//                        backdropPadding: false
//                    },
//                    {
//                        element: "#search_box",
//                        title: "Your Lost Invoice Number",
//                        content: "If you lost your invoice/reference number, just enter your phone number or your email here to view your invoice/reference number", placement: 'bottom', backdrop: true,
//                        backdropContainer: 'body',
//                        backdropPadding: false
//                    },
//                    {
//                        element: "#google_calender",
//                        title: "Link with your Google Calender",
//                        content: "Do you have a Gmail account ? You can put this event in your google calender and get notified in your email ", placement: 'bottom', backdrop: true,
//                        backdropContainer: 'body',
//                        backdropPadding: false
//                    }
//                ]});
            // Initialize the tour
            // tour.init();
            // Start the tour
            //tour.start();
        </script>
    </body>
</html>
