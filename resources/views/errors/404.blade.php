
<html lang="en_US">
<head>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="ERB">
        <link rel="shortcut icon">
        <!--Core CSS -->
        <link href="{{ asset('public/bs3/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('public/js/jquery-ui/jquery-ui-1.10.1.custom.min.css') }}" rel="stylesheet">
        <link href="{{ asset('public/css/bootstrap-reset.css') }}" rel="stylesheet">
        <link href="{{ asset('public/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
        <link href="{{ asset('public/js/jvector-map/jquery-jvectormap-1.2.2.css') }}" rel="stylesheet">
        <link href="{{ asset('public/css/clndr.css') }}?v=1" rel="stylesheet">
        <!--clock css-->
        <link href="{{ asset('public/js/css3clock/css/style.css') }}?v=1" rel="stylesheet">
        <!--Morris Chart CSS -->
        <link rel="stylesheet" href="{{ asset('public/js/morris-chart/morris.css') }}?v=1">
        <!-- Custom styles for this template -->
        <link href="{{ asset('public/css/style.css') }}?v=2" rel="stylesheet">
        <link href="{{ asset('public/css/style-responsive.css') }}?v=1" rel="stylesheet"/>
        <link href="{{ asset('public/select2/css/select2.css') }}?v=1" rel="stylesheet">
        <link href="{{ asset('public/select2/css/select2-bootstrap.css') }}?v=1" rel="stylesheet">
        <link href="{{ asset('public/select2/css/gh-pages.css') }}?v=1" rel="stylesheet">
        <link href="{{ asset('public/datatables/dataTables.bootstrap.css')}}" rel="stylesheet">
        <link href="{{ asset('public/css/bootstrap-tour.min.css')}}" rel="stylesheet">

        <!-- Just for debugging purposes. Don't actually copy this line! -->
        <!--[if lt IE 9]>
        <script src="js/ie8-responsive-file-warning.js"></script><![endif]-->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <title>{{ config('app.name', 'ERB') }}</title>

        <!-- Scripts -->

        <!-- Fonts -->
        <link rel="dns-prefetch" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            .invalid-feedback{color:red}
            .form-control{color:black !important}
        </style>
        <script src="{{ asset('public/js/jquery.js') }}"></script>
        <script src="{{ asset('public/bs3/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('public/js/bootstrap-tour.min.js') }}"></script>

        <script type="text/javascript">
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
var root_url = "<?= url('/'); ?>";

        </script>
        <script src="{{ asset('public/js/function.js') }}"></script>
    </head>

<body>
    <header>
        <nav class="navbar">
            <div class="pre-header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="navbar-header row">
                                <div class="col-xs-9 col-sm-12">
                                    
                              
                                </div>
                                <!-- /.col -->
                            
                                <!-- /.col -->
                            </div>
                            <!-- /.navbar-header row -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-6">
                           
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container -->
            </div>
            <!-- /.pre-header -->
            <div class="navbar-collapse collapse" id="navbar-main">
                <div class="navbar-container">
                                        <ul class="nav navbar-nav">
                        <li class="active">
                            <a class="home" href="<?=url('/')?>"> Home</a></li><li class="">
                      </li>                                                  
                            
                                        </ul>
                                    </div>
                <!-- /.navbar-main -->
            </div>
            <!-- /#navbar-main -->
        </nav>
        <!-- /.navbar -->
    </header>
    
    <section id="main" role="main">
    
        <div class="container">

             <div class="landing-page">
    
    <div class="row">
        
                
            <div class="col-xs-12">
                
                
            <div class="row text-center">   
                <div class="col-xs-12">
                    <div class="content">
            <h2 >404 - Page Not Found<br></h2>
            <p>Sorry, this page might have been moved, please check the url or contact the system administrator.<br/>You may also call us via Tel: +255 22 2122836,

Tel: +255 22 2129087,

Fax: +255 22 2115373,

Email: registrar@erb.go.tz,
Email: info@erb.go.tz </p>
        
    </div>  </div>
                <!-- /.col -->   
                                   
                
                        
            </div>
            <!-- /.row -->
        
        </div><!-- /col -->
            
                
        
    </div>
    <!-- /.row -->

    

    </div>
<!-- /.landing-page -->
    
        </div>
        <!-- /.container -->
    </section>
    <!-- /#main -->
    



<div id="overlay"></div>
<div id="loading">
    <div class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Please wait...</h4>
                </div>
                <div class="modal-body">
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                            <span class="sr-only">100% Complete</span>
                        </div>
                    </div>
                </div>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>
<!-- /.loading -->

<!-- Scripts -->
</body>
</html>