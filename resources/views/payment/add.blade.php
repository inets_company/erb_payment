@extends('layouts.app')

@section('content')

<!-- page start-->

<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                Add Payments

            </header>

            <form class="cmxform form-horizontal " id="commentForm" method="post" action="<?= url('user') ?>">

                <div class="modal-content">

                    <div class="modal-body">
                        <div class="panel-body">
                            <div class=" form">
                                <div class="form-group ">
                                    <label for="cname" class="control-label col-lg-3">Customer Name</label>
                                    <div class="col-lg-6">
                                        <input class="form-control " disabled="" name="customer_name" value="<?= $invoice->user->name ?>"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="cemail" class="control-label col-lg-3">Amount to Pay</label>
                                    <div class="col-lg-6">

                                        <input class="form-control " disabled="" value="<?= number_format($invoice->invoiceFee()->sum('amount') - $invoice->invoiceFeesPayment()->sum('paid_amount')) ?>"/>

                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="phone" class="control-label col-lg-3">Paid Amount</label>
                                    <div class="col-lg-6">
                                        <input class="form-control " id="phone" type="text" name="amount" value="<?= old('amount', $invoice->invoiceFee()->sum('amount') - $invoice->invoiceFeesPayment()->sum('paid_amount')) ?>">
                                    </div>
                                    @if ($errors->has('amount'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('amount') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group ">
                                    <label for="type" class="control-label col-lg-3">Payment Type</label>
                                    <div class="col-lg-6">
                                        <select name="paymentType" id="type"  class="form-control m-bot15">
                                            <option value="BANK">BANK</option>
                                            <option value="CASH">CASH</option>
                                            <option value="CHEQUE">CHEQUE</option>
                                             <option value="MPESA">MPESA</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group is_not_cash">
                                    <label for="method" class="control-label col-lg-3">Payment Method</label>
                                    <div class="col-lg-6">
                                        <select name="method" id="method"  class="form-control m-bot15">
                                            <?php
                                            $financial_entities = \App\Model\Financial_entity::all();
                                            foreach ($financial_entities as $entity) {
                                                ?>
                                                <option value="<?= $entity->name ?>"><?= $entity->name ?></option>
                                            <?php } ?>

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group is_not_cash">
                                    <label for="number" class="control-label col-lg-3" id="number">Transaction ID</label>
                                    <div class="col-lg-6">
                                        <input class="form-control " id="transaction_id" type="text" name="transaction_id" value="<?= old('transaction_id') ?>">
                                        @if ($errors->has('transaction_id'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('transaction_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group ">
                                    <label for="number" class="control-label col-lg-3" id="nametag">Print Nametag</label>
                                    <div class="col-lg-6">
                                        <input  id="nametag" type="checkbox" name="nametag">
                                    </div>
                                </div>



                            </div>

                        </div>


                    </div>
                    <div class="modal-footer">
                        <?= csrf_field() ?>
                        <input type="hidden" name="user" value="payment"/>
                        <input type="hidden" name="number" value="<?= $invoice->number ?>"/>
                        <button class="btn btn-success" type="submit">Save changes</button>
                    </div>
                </div>
            </form>

    </div>
</section>
</div>
</div>
<!-- page end-->
<script type="text/javascript">
    $('#type').change(function () {
        var method = $(this).val();
        if (method == 'CHEQUE') {
            $('#number').html('Cheque Number');
            $('.is_not_cash').show();
            $('#transaction_id').val('');
        } else if (method == 'CASH') {
            $('.is_not_cash').hide();
            $('#transaction_id').val(<?= time()?>);
        } else {
            $('.is_not_cash').show();
            $('#number').html('Transaction ID');
             $('#transaction_id').val('');
        }
    });
    open_edit_model = function (a, b) {
        $.ajax({
            type: 'POST',
            url: "<?= url('setting/getedit') ?>",
            data: {
                "id": a,
                "table": "user"
            },
            dataType: "json",
            success: function (data) {
                $('#title_page').html('Edit user type');
                $('#commentForm').attr('action', b);
                $("#commentForm").attr("method", "get");
                $.each(data, function (i, item) {
                    $('#' + i).val(item);
                });
            }
        });
    }
    reset_form = function () {
        $('#title_page').html('Add New User Type');
        $('#commentForm').attr('action', '<?= url('user') ?>');
        $("#commentForm").attr("method", "post");
        $("input:not(:hidden)").val('');
        $('.delete').html('Delete');
    }
</script>
@endsection