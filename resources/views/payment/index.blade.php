@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                Payments Report
            
            </header>
            <div class="panel-body">
                    <div class="position-center">
                    <form class="form-inline" role="form" action="<?=url('invoice/bulk')?>" method="get">
                            <div class="form-group">
                                <label class="from" for="From">From Date</label>
                                <input type="date" class="form-control" id="from" name="from" placeholder="" value="{{$from}}">
                            </div>
                            <div class="form-group">
                                <label class="to" for="To">To Date</label>
                                <input type="date" class="form-control" id="to" name="to" placeholder="" value="{{$to}}">
                            </div>
                    
                            <button type="submit" class="btn btn-success">Search</button>
                        </form>
                        </div>
                <p></p>
                <section id="unseen">
                    <table class="table table-bordered table-striped table-condensed dataTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Payer Name</th>
                                <th class="numeric">Invoice No</th>
                                <th class="numeric">Paid Amount</th>
                                <th class="numeric">Method</th>
                                <th class="numeric">Channel</th>
                                <th class="numeric">Transaction ID</th>
                                <th class="numeric">Mobile Transaction ID</th> 
                                <th class="numeric">Account Number</th>
                                <th class="numeric">Time</th>
                                <th class="numeric">Status</th>
                                <th class="numeric">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $total_amount = 0;
                            $total_paid = 0;
                            $total_unpaid = 0;
                            $i = 0;
                            ?>
                            @foreach($payments as $payment)
                             <?php $total_amount+=$payment->amount; $i++;?>
                            <tr>
                                <td>{{$i}}</td>
                                <td class="numeric">{{$payment->invoice->user->name}}</td> 
                                <td>{{$payment->invoice->number}}</td>
                                <td class="numeric">{{money($payment->amount)}}</td>
                                <td class="numeric">{{$payment->financial_entity->name}}</td> 
                                <td class="numeric">{{$payment->method}}</td> 
                                <td>{{$payment->transaction_id}}</td>
                                <td class="numeric">{{$payment->mobile_transaction_id}}</td>
                                <td class="numeric">{{$payment->account_number}}</td>   
                                <td>{{$payment->created_at}}</td>
                                <td class="numeric">{{$payment->status}}</td>
                                <td> 
                                     <?php 
                                        if($payment->token ==''){
                                        ?>
                                     <?= can_access('delete_payments') ? btn_delete('payment/' . $payment->id, ''):'' ?>
                                  
                                   
                                        <?php }?>
                                </td>

                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="3">Total</td>
                                <td>{{money($total_amount)}}</td>
                                <td colspan="8"></td>
                            </tr>
                        </tfoot>
                    </table>
               </section>
            </div>
        </section>
    </div>
</div>
@endsection