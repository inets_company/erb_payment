@extends('layouts.app')

@section('content')
<!-- page start-->

<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                Payment Receipts
              
            </header>
            <div class="panel-body">
                 <?php if((int) request('use')==0 ){?>
                <div class="position-center">
                    <form class="form-inline" role="form" action="<?= url('invoice/bulk') ?>" method="get">
                        <div class="form-group">
                            <label class="from" for="From">From Date</label>
                            <input type="date" class="form-control" id="from" name="from" placeholder="" value="{{$from}}">
                        </div>
                        <div class="form-group">
                            <label class="to" for="To">To Date</label>
                            <input type="date" class="form-control" id="to" name="to" placeholder="" value="{{$to}}">
                        </div>

                        <button type="submit" class="btn btn-success">Search</button>
                    </form>
                </div>
                <p></p>
                <section id="unseen">
                    <table class="table table-bordered table-striped table-condensed dataTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Code</th>
                                <th>Number</th>
                                <th>Payer Name</th>
                                <th class="numeric">Payment Date</th>
                                <th>Payment Method</th>
                                <th class="numeric">Invoice Number</th>
                                <th class="numeric">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $total_amount = 0;
                            $total_paid = 0;
                            $total_unpaid = 0;
                            $i = 1;
                            if (count($receipts) > 0) {
                                ?>
                                @foreach($receipts as $receipt)
                                <tr>
                                    <td>{{$i}}</td>
                                    <td>{{$receipt->code}}</td>
                                    <td>{{$receipt->number}}</td>
                                    <td>{{$receipt->payment->invoice->user->name}}</td>
                                    <td class="numeric">{{$receipt->payment->created_at}}</td>
                                    <td class="numeric">{{$receipt->payment->method}}</td>
                                    <td class="numeric">{{$receipt->payment->invoice->number}}</td>  
                                    <td class="numeric"> 
                                        <a href="<?= url('payment/receipts/?use=' . $receipt->id) ?>" class="btn btn-xs btn-warning"><i class="fa fa-file-o"></i> Receipt</a>  </td>  
                                </tr>
                                <?php $i++ ?>
                                @endforeach
                            <?php } ?>
                        </tbody>
                    </table>
                </section>
               
                <?php }else{?>
                @include('payment.receipt_template')
                 <?php }?>
            </div>
        </section>
    </div>
</div>
<!-- page end-->
@endsection