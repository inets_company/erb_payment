@extends('layouts.app')

@section('content')
<!-- page start-->

<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                API requests
                <span class="tools pull-right">
                    <a href="javascript:;" class="fa fa-chevron-down"></a>
                    <a href="javascript:;" class="fa fa-cog"></a>
                    <a href="javascript:;" class="fa fa-times"></a>
                </span>
            </header>
            <div class="panel-body">
                <section id="unseen">
                    <table class="table table-bordered table-striped table-condensed dataTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Content</th>
                                <th>Request Date</th>
                                <th>Updated time</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $total_amount = 0;
                            $total_paid = 0;
                            $total_unpaid = 0;
                            $i = 1;
                            ?>
                            @foreach($requests as $request)
                            <tr>
                                <td>{{$i}}</td>
                                <td><?php
                                    $req = explode(':', $request->content);
                                    echo json_encode($req,JSON_PRETTY_PRINT);
                                    ?></td>
                                <td>{{$request->created_at}}</td>
                                <td>{{$request->updated_at}}</td>            
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </section>
            </div>
        </section>
    </div>
</div>
<!-- page end-->
@endsection