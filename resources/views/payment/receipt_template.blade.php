<?php
$receipt = \App\Model\Receipt::find(request('use'));
?>

<div class="widget-body">
    <span class=" pull-right">
        <div class="tab-pane">
            <div style="">

                <a href="#" onmousedown="printElem('outside_print_div')" class="btn btn-primary btn-sm"><i class="fa fa-print"></i> Print </a>
            </div>
        </div>
    </span><br/><br/>
    <style scoped>
        @import "https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css";
        @page { size: A5 landscape }
    </style>
    <div id="outside_print_div">
    <div class="widget-main padding-24 A5 landscape sheet padding-10mm" id="main_receipt_div" style=" font-size: 100%">
        <div class="row">
             <div class="col-sm-12">
            <table   style="border:none; width: 100%">
                <tbody>
                    <tr>
                        <th style="width: 30%">
                            <img src="<?= url('public/images/tanzania.png') ?>" height="120" width="120"/>
                        </th>
                        <th style="width: 50%; text-align: center">
                            THE UNITED REPUBLIC OF TANZANIA
                            <h3 style="font-weight: bolder">ENGINEERS REGISTRATION BOARD</h3>
                        </th>
                        <th style="width: 20%;">
                            <img src="<?= url('public/images/erb.png') ?>" height="120" width="120" style="float:right;"/>
                        </th>
                    </tr>
                    <tr>
                        <th colspan="2" style="font-size: 15px">
                            <h5>PHONE NO. 2122836,2129087,2115373</h5>
                            <h5>FAX NO. 2115373</h5>
                            <h5>Email: registrar@erb.go.tz</h5>
                            <h5>Web Address: www.erb.go.tz</h5>
                        </th>
                        <th width='30%' style="text-align: right">
                            <h5>P.O BOX 14942</h5>
                            <h5>DAR ES SALAAM</h5>
                            <h5>TANZANIA</h5>
                        </th>

                    </tr>
                </tbody>
            </table>
             </div>
        </div><!-- /.row -->

        <h2 align='center' style="font-weight: bolder">RECEIPT</h2>
        <br/>
        <div class="row">
            <div class="col-sm-12">
             
                To
                <div>
                    <table  style="border:none; width: 100%">
                        <tbody>
                            <tr>
                                <td>
                                    <ul class="list-unstyled spaced" style="font-size: 17px">
                                        <li>
                                            <?= $receipt->payment->invoice->user->name ?>
                                        </li>

                                        <li>
                                            <?= $receipt->payment->invoice->user->email ?>
                                        </li>

                                        <li>
                                            Cust-TIN:
                                        </li>

                                        <li class="divider"></li>

                                    </ul>  
                                </td>
                                <td>
                                    <ul class="list-unstyled  spaced" style="text-align: right;font-size: 17px">
                                        <li>
                                            Receipt No: <?= $receipt->number ?>
                                        </li>

                                        <li>
                                            TIN : 101-052-451
                                        </li>

                                        <li>
                                            Receipt Date: <?= date('d/m/Y', strtotime($receipt->created_at)) ?>
                                        </li>
                                        <li>
                                            Currency: TZS
                                        </li>

                                        <li class="divider"></li>
                                    </ul> 
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->

        <div class="space"></div>

        <div>
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th class="center">#</th>
                        <th>ITEM CODE</th>
                        <th class="hidden-480">ITEM DESCRIPTION</th>
                        <th class="hidden-480">QTY</th>
                        <th class="hidden-480">UNIT PRICE</th>
                        <th class="hidden-480">UOM</th>
                        <th>AMOUNT</th>
                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <td class="center">1</td>

                        <td>
                            <a href="#"><?= $receipt->payment->invoice->number ?></a>
                        </td>
                        <td class="hidden-480">
                            ERB Annual Engineers Day
                        </td>
                        <td class="hidden-480"> --- </td>
                        <td class="hidden-480"> <?= $receipt->payment->amount ?> </td>

                        <td class="hidden-480"> FEE </td>
                        <td><?= $receipt->payment->amount ?></td>
                    </tr>


                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="5"></td>
                        <td class="hidden-480"> GAMOUNT</td>
                        <td><?= $receipt->payment->amount ?></td>
                    </tr>
                </tfoot>
            </table>
        </div>

        <div class="hr hr8 hr-double hr-dotted"></div>

        <div class="row">
            <div class="col-md-7">
                <h5>REG, EFD, P/LICENCE, R.STAMP AND ANNUAL FOR PE</h5>
                <br/>
                <h5>Payment Mode: TT# <?= $receipt->payment->transaction_id ?>, A/C: <?= $receipt->payment->method ?></h5>
        </div>
            <div class="col-sm-5 pull-right">
                <h4 class="pull-right">
                    <p>.........................................</p>
                    <span  style="text-align: center !important"><small>For and Onbehalf of  Engineers <br/>Registration Board</small></span>
                </h4>
            </div>
        </div>


        <div class="space-6"></div>
    </div>
    </div>
</div>
<script>
    /*Function to print any div, remember to add an outside div with id=outside_print_div and on the inner div
     add the class=A5 landscape sheet padding-10mm for size, sheet styling and padding respectively*/
    function printElem(divId) {
        var content = document.getElementById(divId).innerHTML;

        var mywindow = window.open();

        mywindow.document.write('<html><head><title>{{$receipt->payment->invoice->user->name.' Receipt'}}</title> <style>font-size:50%</style>' +
            '<link href="{{ asset('public/bs3/css/bootstrap.min.css') }}" rel="stylesheet">'+
            '<link href="{{ asset('public/js/jquery-ui/jquery-ui-1.10.1.custom.min.css') }}" rel="stylesheet">' +
            '<link href="{{ asset('public/css/bootstrap-reset.css') }}" rel="stylesheet">'+
            '<link rel="stylesheet" href="{{ asset('public/css/normalize.min.css') }}">' +
            '<link rel="stylesheet" href="{{ asset('public/css/paper.css') }}">');
        mywindow.document.write('</head><body>');
        mywindow.document.write(content);
        mywindow.document.write('</body></html>');

        mywindow.document.close();
        mywindow.focus()
        mywindow.print();
       // mywindow.close();
        return true;
    }
</script>
