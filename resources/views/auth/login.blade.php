@extends('layouts.app')
@section('content')
<form class="form-signin"  method="POST" action="{{ route('login') }}">
    @csrf
    <h2 class="form-signin-heading">sign in now</h2>
    <div class="login-wrap">
        <div class="form-group row">
            <label for="email" class="col-sm-12 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

            <div class="col-md-12">
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                @if ($errors->has('email'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="password" class="col-md-12 col-form-label text-md-right">{{ __('Password') }}</label>

            <div class="col-md-12">
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                @if ($errors->has('password'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <label class="checkbox">
            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                   <span class="pull-right">
                <a class="btn btn-link" href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password?') }}
                </a>
            </span>
        </label>
        <button class="btn btn-lg btn-login btn-block" type="submit"> {{ __('Login') }}</button>

    </div>
</form>


@endsection
 @include('layouts.help')