@extends('layouts.app')

@section('content')
<?php $event=\App\Model\Event::first(); ?>
<div class="container">

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-8 col-sm-12">
                    <form class="cmxform form-horizontal " id="commentForm" method="post" action="<?= url('booking') ?>">

                        <div class="modal-content">
                            <h4 class="modal-title">Book for the Event</h4>

                            <div class="modal-body">
                                <div class="panel-body">
                                    <div class=" form">
                                             <div class="form-group ">
                                            <label for="type" class="control-label col-lg-3">Employer</label>
                                            <div class="col-lg-6">
                                                <select class="form-control select2_single select2"  name="employer_id">
                                                     <option value=""></option> 
                                                    <?php $userype = \App\Model\Employer::all() ?>
                                                    @foreach ($userype as $type)
                                                    <option value="{{$type->id}}">{{$type->name}}</option>
                                                    @endforeach
                                                </select>

                                            </div>
                                             <?php echo form_error($errors, 'employer_id'); ?>
                                            <a data-toggle="modal" href="#myModalEmployer">Or Add new</a>
                                        </div>
                                        
                                        <div class="form-group ">
                                            <label for="name" class="control-label col-lg-3">Your Name (required)</label>
                                            <div class="col-lg-6">
                                                <input class=" form-control" value="<?= old('name') ?>" id="name" name="name" minlength="2" type="text" required="" placeholder="Firstname Lastname" onblur="this.value = this.value.toUpperCase()"  pattern="[a-zA-Z\. ]{5,}" >
                                            </div>

                                            <span class="help-block">E.g John Joseph, only string and . </span>
                                            <?php echo form_error($errors, 'name'); ?>
                                        </div>
                                        <div class="form-group ">
                                            <label for="amount" class="control-label col-lg-3">Your Phone (required)</label>
                                            <div class="col-lg-6">
                                                <input class=" form-control" value="<?= old('phone') ?>" id="phone" name="phone" minlength="2" type="text" required="">
                                            </div>
                                            <?php echo form_error($errors, 'phone'); ?>
                                        </div>
                                        <div class="form-group ">
                                            <label for="user_email" class="control-label col-lg-3">Your Email(required)</label>
                                            <div class="col-lg-6">
                                                <input class=" form-control" value="<?= old('email') ?>" id="user_email" name="email" minlength="2" type="email" required="" onblur="this.value = this.value.toLowerCase()">
                                            </div>
                                            <?php echo form_error($errors, 'email'); ?>
                                        </div>
                                        <div class="form-group ">

                                            <label for="number" class="control-label col-lg-3">Specialization (Required)</label>

                                            <div class="col-lg-6">
                                                <select class="form-control select2_single select2" name="profession_id">
                                                             <option value=""></option>                                     
                                                    <?php $professions = App\Model\Profession::where('invitee',0)->get(); ?>
                                                    @foreach ($professions as $profession)
                                                    <option value="{{$profession->id}}">{{$profession->name}}</option>                                                  @endforeach;
                                                </select>

                                            </div>
                                             <?php echo form_error($errors, 'profession_id'); ?>
                                            <!--<a data-toggle="modal" href="#myModal">Or Add new</a>-->
                                        </div>
                                  
                                   
                                      
                                        <div class="form-group ">
                                            <label for="type" class="control-label col-lg-3"></label>
                                            <div class="col-lg-6" style="color:black !important;">
                                                Reference Number will be sent to your email
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="type" class="control-label col-lg-3"></label>
                                            <div class="col-lg-6">
                                                <button class="btn btn-primary btn-lg col-sm-8" style="font-size: 13px;" type="submit">Get Your Reference Number</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <?= csrf_field() ?>

                        </div>
                    </form>

                </div>
                
                <div class="col-lg-4 col-sm-12">
                    <div class="feed-box text-center">
                        <section class="panel">
                            <div class="panel-body">
                                <?php
                                $day = $event->date;

// add 1 days to the date above
                                $NewDate = date('Y-m-d', strtotime($day . " +1 days"));
                                ?>
                                <a title="Add to my calender" href="https://www.google.com/calendar/render?action=TEMPLATE&text=<?= $event->name ?>&dates=<?= date('Ymd', strtotime($event->date)) ?>T224000Z/<?= date('Ymd', strtotime($NewDate)) ?>T221500Z&details=<?= $event->theme ?>.. For+more+details,+click+here:+http://engineersday.co.tz&location=<?= $event->location ?>&sf=true&output=xml" target="_blank"><div class="corner-ribon blue-ribon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                </a>
                                <span class="bar-label-value"> Annual Engineers Day</span><br/>
                                <span>By Engineers Registration Board(ERB)</span>
                                             <p><br/><i class="fa fa-time"></i>&nbsp; <?= $event->description ?></p>
                                <p><br/><i class="fa fa-map-marker"></i>&nbsp; <?=$event->location.' <br/>'?></p>
                            </div>
                            <div class="mini-stat clearfix">
                                <div class="mini-stat-info">
                                    <span>Tsh <?= number_format($fee->amount) ?>/=</span>
                                    Early Booking
                                </div>
                            </div>
                            <hr/>
                            <div class="mini-stat clearfix">
                                <div class="mini-stat-info">
                                    <span>Tsh <?= number_format($fee->amount + $fee->penalty_amount) ?>/=</span>
                                   After <?= date('d M Y', strtotime($fee->end_date)) ?>
                                </div>
                            </div>

                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    
  <div class="modal fade" id="myModalEmployer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <form class="cmxform form-horizontal " id="commentForm" method="post" action="<?=url('user')?>">

                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Add New Entity</h4>
                        </div>
                        <div class="modal-body">
                            <div class="panel-body">
                                <div class=" form">
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Name (required)</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="cname" name="name" minlength="2" type="text" required="">
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Abbreviation</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="cname" name="abbreviation"  type="text" required=""  onblur="this.value = this.value.toUpperCase()">
                                        </div>
                                    </div>
                                 <div class="form-group "  style="display: none;">
                                        <label for="cemail" class="control-label col-lg-3">E-Mail (required)</label>
                                        <div class="col-lg-6">
                                            <input class="form-control " id="cemail" type="email" name="email" required="" value="<?= time().'jkdjs@engineers.co.tz'?>">
                                        </div>
                                    </div>
                           <!--            <div class="form-group ">
                                        <label for="curl" class="control-label col-lg-3">Phone (required)</label>
                                        <div class="col-lg-6">
                                            <input class="form-control " id="curl" type="text" name="phone">
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="ccomment" class="control-label col-lg-3">Location(required)</label>
                                        <div class="col-lg-6">
                                            <textarea class="form-control " id="ccomment" name="location" required=""></textarea>
                                        </div>
                                    </div>-->

                                </div>

                            </div>


                        </div>
                        <div class="modal-footer">
                           <?= csrf_field() ?>
                            <input type="hidden" name="user" value="employer"/>
                             <input type="hidden" name="auth" value="0"/>
                            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                            <button class="btn btn-success" type="submit">Save changes</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
@endsection