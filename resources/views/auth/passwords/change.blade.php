@extends('layouts.app')

@section('content')
<!-- page start-->

<div class="row"><div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Change Password    
            </header>
            <div class="panel-body">
                <form role="form" method="post" class="form-horizontal ">
                    <div class="form-group has-success">
                        <label class="col-lg-3 control-label">Current Password</label>
                        <div class="col-lg-6">
                            <input type="password" placeholder="" name="current" id="f-name" class="form-control">
                            <!--<p class="help-block">Successfully done</p>-->
                        </div>
                    </div>
                    <div class="form-group has-error">
                        <label class="col-lg-3 control-label">New Password</label>
                        <div class="col-lg-6">
                            <input type="password" placeholder="" name="new1" id="l-name" class="form-control">
                            <p class="help-block">Letters,numbers and one special character</p>
                        </div>
                    </div>
                    <div class="form-group has-warning">
                        <label class="col-lg-3 control-label">Confirm New Password</label>
                        <div class="col-lg-6">
                            <input type="password" placeholder="" name="new2" id="email2" class="form-control">
                            <p class="help-block">Rewrite new password</p>
                        </div>
                    </div>
                    <?= csrf_field() ?>
                    <div class="form-group">
                        <div class="col-lg-offset-3 col-lg-6">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>
<!-- page end-->
@endsection