@extends('layouts.app')

@section('content')
<!-- page start-->

<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                Fees
                <span class="tools pull-right">
                    <a href="javascript:;" class="fa fa-chevron-down"></a>
                    <a href="javascript:;" class="fa fa-cog"></a>
                    <a href="javascript:;" class="fa fa-times"></a>
                </span>
            </header>
                  <?php if (can_access('add_settings')) { ?>
            <p><br/>&nbsp;&nbsp;&nbsp;<a class="btn btn-success" data-toggle="modal" href="#myModal" onmousedown="reset_form()">
                    Add  New Fee
                </a></p>
                  <?php }?>
            <div class="panel-body">
                <section id="unseen">
                    <table class="table table-bordered table-striped table-condensed dataTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th class="numeric">Amount</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Penalty Amount</th>
                                <th>Date Registered</th>                        
                                <th class="numeric">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            ?>
                            @foreach($fees as $fee)
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$fee->name}}</td>
                                <td class="numeric">{{$fee->amount}}</td>
                                <td>{{$fee->start_date}}</td>
                                <td>{{$fee->end_date}}</td>
                                <td>{{$fee->penalty_amount}}</td>
                                <td class="numeric">{{$fee->created_at}}</td>

                                <td class="numeric">
                                     <?=can_access('delete_settings')? btn_delete( 'user/' . $fee->id, 'fee'):'' ?>
                                        <?php if (can_access('edit_settings')) { ?>
                                    <a data-toggle="modal" href="#myModal" onmousedown="open_edit_model('<?= $fee->id ?>', '<?= url('setting/' . $fee->id . '/edit') ?>')" class="btn btn-xs btn-info">Edit</a>  
                                        <?php }?>
                                </td>
                            </tr>
                            <?php $i++; ?>
                            @endforeach
                        </tbody>
                    </table>
                </section>
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <form class="cmxform form-horizontal " id="commentForm" method="post" action="<?= url('user') ?>">

                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="title_page">Add New Fee</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="panel-body">
                                        <div class=" form">
                                            <div class="form-group ">
                                                <label for="cname" class="control-label col-lg-3">Name (required)</label>
                                                <div class="col-lg-6">
                                                    <input class=" form-control" id="name" name="name" minlength="2" type="text" required="">
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label for="amount" class="control-label col-lg-3">Amount (required)</label>
                                                <div class="col-lg-6">
                                                    <input class=" form-control" id="amount" name="amount" minlength="2" type="text" required="">
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label for="cname" class="control-label col-lg-3">Start Date (required)</label>
                                                <div class="col-lg-6">
                                                    <input class=" form-control" id="start_date" name="start_date" minlength="2" type="date" required="">
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label for="cname" class="control-label col-lg-3">End Date (required)</label>
                                                <div class="col-lg-6">
                                                    <input class=" form-control" id="end_date" name="end_date" minlength="2" type="date" required="">
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label for="cname" class="control-label col-lg-3">Penalty Amount (required)</label>
                                                <div class="col-lg-6">
                                                    <input class=" form-control" id="penalty_amount" name="penalty_amount" minlength="2" type="text" required="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <?= csrf_field() ?>
                                    <input type="hidden" name="user" value="fee"/>
                                    <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                    <button class="btn btn-success" type="submit">Save changes</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
    </div>
</section>
</div>
</div>
<!-- page end-->
<script type="text/javascript">
    open_edit_model = function (a, b) {
        $.ajax({
            type: 'POST',
            url: "<?= url('setting/getedit') ?>",
            data: {
                "id": a,
                "table": "fee"
            },
            dataType: "json",
            success: function (data) {
                console.log(data);
                $('#title_page').html('Edit Fee');
                $('#commentForm').attr('action', b);
                $("#commentForm").attr("method", "get");
                $.each(data, function (i, item) {
                    console.log(i);
                    $('#' + i).val(item);
                });
            }
        });
    }
    reset_form = function () {
        $('#title_page').html('Add New Fee');
        $('#commentForm').attr('action', '<?= url('user') ?>');
        $("#commentForm").attr("method", "post");
        $('input').val('');
    }
</script>
@endsection