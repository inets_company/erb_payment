@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Configuration Settings

            </header>
            <div class="panel-body">
                <div class="row">  
                     <?php if (can_access('edit_settings')) { ?>
                    <div class="form-group ">
                        <label for="number" class="control-label col-lg-3 text-right">Select Role</label>
                        <div class="col-lg-6">
                            
                            <select class="form-control" id="role" name="role">
                                <option value=""></option> 

                                @foreach ($roles as $role)
                                <option value="{{$role->id}}" <?= (int) $role_id > 0 && $role_id==$role->id ? 'selected' : '' ?>>{{$role->name}}</option>                                                  @endforeach;
                            </select>
                          

                        </div>
                    </div>
                       <?php }?>
                </div>

                <section id="unseen">
                    <?php
                    if ((int) $role_id > 0) {
                        $role = \App\Model\Role::find($role_id);
                        ?>

                        <div class="x_content">
                            <div class="col-md-12 col-sm-12 col-xs-12">

                                <div class="x_panel">
                                    <br/>
                                    <br/>
                                    <div class="monthly-stats pink">
                                        <div class="clearfix">
                                            <h4 class="pull-left"> <?= $role->name ?></h4>
                                            <!-- Nav tabs -->

                                        </div>

                                    </div>
                                    <div class="x_content">

                                        <!-- start accordion -->
                                        <div class="panel-group m-bot20" id="accordion">

                                            <?php
                                            $i = 1;
                                            foreach ($groups as $group) {
                                                ?>
                                                <div class="panel">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne<?= $i ?>">
                                                                <?= $group->name ?>
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne<?= $i ?>" class="panel-collapse collapse <?= $i == 1 ? 'in' : '' ?>" style="height: auto;">
                                                        <div class="panel-body">
                                                            <?php
                                                            foreach ($group->permission()->get() as $permission) {
                                                                ?>

                                                                <p style="margin-left: 10%;">
                                                                    <?php
                                                                    $checked = $permission->rolePermission()->where('role_id', $role_id)->count() > 0 ? 'checked' : '';
                                                                    ?>
                                                                    <input type="checkbox" <?= $checked ?> value="<?= $permission->id ?>" class="permission"><?= $permission->display_name ?>
                                                                    <span id="status<?= $permission->id ?>"></span>
                                                                </p>

                                                                <?php
                                                            }
                                                            $i++;
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                                $i++;
                                            }
                                            ?>
                                        </div>
                                        <!-- end of accordion -->



                                    </div>
                                </div>

                            </div>

                        </div>
                    <?php } ?>
                </section>
            </div>
        </section>
    </div>
</div>
<script type="text/javascript">
    sort_user = function () {
        $('#role').change(function () {
            var type = $(this).val();
            window.location.href = '<?= url()->current() ?>/?id=' + type;
        });
    }
    $('.permission').click(function () {
        var id = $(this).val();
        var role_id = '<?= (int) $role_id > 0 ? $role_id : '0' ?>';
        if (parseInt(id)) {
            if (!this.checked) {
                // It is not checked, show your div...
                var url_obj = "<?= url('setting') ?>";
                var tag = 'removePermission';
            } else {
                var url_obj = "<?= url('setting') ?>";
                var tag = 'addPermission';
            }
            $.ajax({
                type: 'POST',
                url: url_obj,
                data: {"id": id, role_id: role_id, tag: tag},
                dataType: "html",
                success: function (data) {

                    $('#status' + id).html('<span class="label label-success">'+data+'</span>');
                }
            });
        }
    });

    $(document).ready(sort_user);
</script>
@endsection