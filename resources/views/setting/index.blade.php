@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Configuration Settings

            </header>
            <div class="panel-body">
               
                    <div class=" form">
                        <?php
                        if (session('return_message')) {
                            ?>
                            <div class="alert alert-success fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                                <?= session('return_message') ?>

                            </div>
                        <?php } ?>
                        <form class="cmxform form-horizontal " id="commentForm" method="post" action="{{url('setting')}}">
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-3">Institution Name (required)</label>
                                <div class="col-lg-6">
                                    <input class=" form-control" id="cname" name="name" minlength="2" type="text" required="" value="{{old('name',count($setting)==1 ? $setting->name :'')}}">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="cphone" class="control-label col-lg-3">Phone (required)</label>
                                <div class="col-lg-6">
                                    <input class=" form-control" id="cphone" name="phone" minlength="2" type="text" required="" value="{{old('phone',count($setting)==1 ? $setting->phone :'')}}">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="cemail" class="control-label col-lg-3">E-Mail (required)</label>
                                <div class="col-lg-6">
                                    <input class="form-control " id="cemail" type="email" name="email" required="" value="{{old('email',count($setting)==1 ? $setting->email :'')}}">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="curl" class="control-label col-lg-3">Website (optional)</label>
                                <div class="col-lg-6">
                                    <input class="form-control " id="curl" type="url" name="website" value="{{old('website',count($setting)==1 ? $setting->website :'')}}">
                                </div>
                            </div>

                            <div class="form-group ">
                                <label for="cbox" class="control-label col-lg-3">P.o Box (required)</label>
                                <div class="col-lg-6">
                                    <input class=" form-control" id="cbox" name="box" minlength="2" type="text" required="" value="{{old('box',count($setting)==1 ? $setting->box :'')}}">
                                </div>
                            </div>

                            <div class="form-group ">
                                <label for="caddress" class="control-label col-lg-3">Address (required)</label>
                                <div class="col-lg-6">
                                    <textarea class="form-control " id="caddress" name="address" required="">{{count($setting)==1 ? $setting->address :''}}</textarea>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="caddress" class="control-label col-lg-3">Site Mode</label>
                                <div class="col-lg-6">
                                    <select name="site_mode" class="form-control">
                                        <option value="1" <?= $setting->site_mode == 1 ? 'selected' : '' ?>>Active</option>
                                        <option value="2" <?= $setting->site_mode == 2 ? 'selected' : '' ?>>Not Active</option>
                                        <option value="3" <?= $setting->site_mode == 3 ? 'selected' : '' ?>>Certificate</option>

                                    </select>
                                </div>
                            </div>
                              <div class="form-group ">
                                <label for="cbox" class="control-label col-lg-3">Control Number Deadline</label>
                                <div class="col-lg-6">
                                    <input class=" form-control calender" id="cbox" name="payment_deadline" minlength="2" type="date" required="" value="{{old('payment_deadline',count($setting)==1 ? $setting->payment_deadline :'')}}">
                                </div>
                            </div>

                             <?php if (can_access('edit_settings')) { ?>
                            <div class="form-group">
                                <div class="col-lg-offset-3 col-lg-6">
                                    <button class="btn btn-primary" type="submit">Save</button>
                                    <button class="btn btn-default" type="button">Cancel</button>
                                </div>
                            </div>
                              <?php } ?>
                            <?= csrf_field() ?>
                        </form>
                    </div>
             
            </div>
        </section>
    </div>
</div>

<script type="text/javascript">
   // $('#commentForm input,#commentForm select, #commentForm textarea').prop('disabled', true);
</script>
 
@endsection