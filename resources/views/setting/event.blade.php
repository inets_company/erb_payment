@extends('layouts.app')

@section('content')
<!-- page start-->

<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                Events
                <span class="tools pull-right">
                    <a href="javascript:;" class="fa fa-chevron-down"></a>
                    <a href="javascript:;" class="fa fa-cog"></a>
                    <a href="javascript:;" class="fa fa-times"></a>
                </span>
            </header>
            <?php if (can_access('add_settings')) { ?>
            <p><br/>&nbsp;&nbsp;&nbsp;<a class="btn btn-success" data-toggle="modal" href="#myModal" onmousedown="reset_form()">
                    Add  New Event
                </a></p>
                   <?php }?>
            <div class="panel-body">
                <section id="unseen">
                    <table class="table table-bordered table-striped table-condensed dataTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th class="numeric">Status</th>
                                <th>Location</th>
                                <th>Theme</th>
                                <th>Description</th>
                                <th>Date Registered</th>                        
                                <th class="numeric">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            ?>
                            @foreach($events as $event)
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$event->name}}</td>
                                <td><?= $event->status == 1 ? '<b class="badge bg-success">Active</b>' : '<b class="badge badge-default">In Active</b>' ?></td>
                                <td>{{$event->location}}</td>
                                <td>{{$event->theme}}</td>
                                <td>{{$event->description}}</td>
                                <td class="numeric">{{$event->created_at}}</td>

                                <td class="numeric">
                                     <?=can_access('delete_settings')? btn_delete('user/' . $event->id, 'event'):'' ?>
                                        <?php if (can_access('edit_settings')) { ?>
                                    <a data-toggle="modal" href="#myModal" onmousedown="open_edit_model('<?= $event->id ?>', '<?= url('setting/' . $event->id . '/edit') ?>')" class="btn btn-xs btn-info">Edit</a>  
                                        <?php } ?>
                                </td>
                            </tr>
                            <?php $i++; ?>
                            @endforeach
                        </tbody>
                    </table>
                </section>
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <form class="cmxform form-horizontal " id="commentForm" method="post" action="<?= url('user') ?>">

                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="title_page">Add New Event</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="panel-body">
                                        <div class=" form">
                                            <div class="form-group ">
                                                <label for="cname" class="control-label col-lg-3">Name (required)</label>
                                                <div class="col-lg-6">
                                                    <input class=" form-control" id="name" name="name" minlength="2" type="text" required="">
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label for="amount" class="control-label col-lg-3">Location</label>
                                                <div class="col-lg-6">
                                                    <input class=" form-control" id="location" name="location" minlength="2" type="text" required="">
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label for="cname" class="control-label col-lg-3">Theme (required)</label>
                                                <div class="col-lg-6">
                                                    <textarea id="theme" class="form-control" name="theme" minlength="2" type="text" required=""></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label for="cname" class="control-label col-lg-3">Description (required)</label>
                                                <div class="col-lg-6">
                                                    <textarea id="description" class="form-control" name="description" minlength="2" type="text" required=""></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label for="cname" class="control-label col-lg-3">Status(required)</label>
                                                <div class="col-lg-6">
                                                    <select class="form-control" name="status" id="status">
                                                        <option  value="1">Active</option>
                                                        <option value="0">Not Active</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <?= csrf_field() ?>
                                    <input type="hidden" name="user" value="event"/>
                                    <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                    <button class="btn btn-success" type="submit">Save changes</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
    </div>
</section>
</div>
</div>
<!-- page end-->
<script type="text/javascript">
    open_edit_model = function (a, b) {
        $.ajax({
            type: 'POST',
            url: "<?= url('setting/getedit') ?>",
            data: {
                "id": a,
                "table": "event"
            },
            dataType: "json",
            success: function (data) {
                $('#title_page').html('Edit Fee');
                $('#commentForm').attr('action', b);
                $("#commentForm").attr("method", "get");
                $.each(data, function (i, item) {
                    console.log(i);
                    $('#' + i).val(item);
                });
            }
        });
    }
    reset_form = function () {
        $('#title_page').html('Add New Fee');
        $('#commentForm').attr('action', '<?= url('user') ?>');
        $("#commentForm").attr("method", "post");
        $("input:not(:hidden)").val('');
        $('.delete').html('Delete');
    }
</script>
@endsection