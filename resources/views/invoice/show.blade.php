@extends('layouts.app')

@section('content')
<style>
    @media print {
        a[href]:after {
            content: none !important;
        }
        #myTab {display: block !important; opacity: 1 !important;}
        table,thead,tbody,tr,td,th {border: 1px solid black !important;}
        #invoice_name{font-size: 15px !important; font-weight: bolder}
    }
</style>
<div class="container">

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="feed-box text-center" id="head_one">
                <section class="panel">
                    <div class="panel-body">
                        <div class="corner-ribon blue-ribon">
                            <?php
                            $day = $event->date;

// add 1 days to the date above
                            $NewDate = date('Y-m-d', strtotime($day . " +1 days"));
                            ?>
                            <a title="Add to my calender" href="https://www.google.com/calendar/render?action=TEMPLATE&text=<?= $event->name ?>&dates=<?= date('Ymd', strtotime($event->date)) ?>T224000Z/<?= date('Ymd', strtotime($NewDate)) ?>T221500Z&details=<?= $event->theme ?>.. For+more+details,+click+here:+http://engineersday.co.tz&location=<?= $event->location ?>&sf=true&output=xml" target="_blank"><div class="corner-ribon blue-ribon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                            </a>
                        </div>
                        <h4> <a href="#">
                            <!--<img alt="" src="images/lock_thumb.jpg">-->
                                Hello, <?= $invoice->user->name ?>
                            </a></h4>
                        <?php
                        if(strlen($invoice->number)>7){
                        ?>
                        <h1 id="invoice_number" class="animated bounce infinite">Your Control Number is: <span style="color:rgb(0, 102, 204);"><?= $invoice->number ?></span></h1>
                        <?php }?>

                        <p>Valid untill <strong><?= date('d M Y', strtotime($fee->end_date)) ?></strong>, Tsh <?= number_format($fee->penalty_amount) ?> penalty will be added after the date </p>
                    </div>
                </section>
            </div>
            <div class="row">
                <section class="panel">
                    <header class="panel-heading tab-bg-dark-navy-blue" id="tab_panel_heading">
                        <ul class="nav nav-tabs">
                            <li class="active" id="your_invoice">
                                <a data-toggle="tab" href="#about-2">
                                    <i class="fa fa-user"></i>
                                    Your Invoice
                                </a>
                            </li>
                            <li class="" id="how_to_pay">
                                <a data-toggle="tab" href="#home-2">
                                    <i class="fa fa-home"></i> How To Make Payments
                                </a>
                            </li>

                            <li class="">
                                <a data-toggle="tab" href="#contact-2">
                                    <i class="fa fa-envelope-o"></i>
                                    FAQ
                                </a>
                            </li>
                        </ul>
                    </header>
                    <div class="panel-body">
                        <div class="tab-content">

                            <div id="about-2" class="tab-pane active">
                                <div class="text-right" style="">
<!--                                    <a class="btn btn-danger btn-sm" data-toggle="modal" href="#myModal"><i class="fa fa-check"></i> Send Invoice </a>-->
                                    <a href="#" onmousedown="print_page()" class="btn btn-primary btn-sm"><i class="fa fa-print"></i> Print </a>

                                </div>
                                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title">Send Invoice to Email</h4>
                                            </div>
                                            <form class="form-horizontal bucket-form" method="get">
                                                <div class="modal-body">


                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Email</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control">
                                                            <span class="help-block">For more than one email, separate them by comma.</span>
                                                        </div>
                                                    </div>




                                                </div>
                                                <div class="modal-footer">
                                                    <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                                    <button class="btn btn-success" type="submit">Send</button>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                                <div class="row" style="" id="print_div">
                                    <div class="col-md-12">
                                        <section class="panel">
                                            <div class="panel-body invoice">
                                                <div class="invoice-header">
                                                    <div class="invoice-title col-md-3 col-xs-2">
                                                        <h1 id="invoice_name">Control Number</h1>
                                                    </div>
                                                    <div class="invoice-info col-md-9 col-xs-10">

                                                        <div class="pull-right">
                                                            <div class="col-md-6 col-sm-6 pull-left">
                                                                Engineers Registration Board <br>
                                                                Tetex Building (2nd and 4th Floor),<br>
                                                                P.o Box 14942, <br/>Dar es salaam
                                                            </div>

                                                            <div class="col-md-6 col-sm-6 pull-right">
                                                                <p>Tel: +255 22 2122836 Or<br>+255 22 2129 087<br>
                                                                    Email : registrar@erb.go.tz</p>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="row invoice-to">
                                                    <div class="col-md-4 col-sm-4 pull-left">
                                                        <h4>Invoice To:</h4>
                                                        <h2><?= $invoice->user->name ?></h2>
                                                        <p>
                                                            <br>

                                                            Phone: <?= $invoice->user->phone ?><br>
                                                            Email : <?= $invoice->user->email ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-4 col-sm-5 pull-right">
                                                        <div class="row">
                                                            <div class="col-md-4 col-sm-5 inv-label">Control Number</div>
                                                            <div class="col-md-8 col-sm-6">
                                                                <?php
                        if(strlen($invoice->number)>7){
                        ?>
                                                                <b style="font-size: 17px"><?= $invoice->number ?></b>
                        <?php }?></div>
                              
                                                        </div>
                                                        <br>
                                                        <div class="row">
                                                            <div class="col-md-4 col-sm-5 inv-label">Date :</div>
                                                            <div class="col-md-8 col-sm-7"><?= date('d M Y', strtotime($invoice->date)) ?></div>
                                                        </div>
                                                        <br>
                                                        <div class="row">
                                                            <div class="col-md-12 inv-label">
                                                                <h3>TOTAL DUE</h3>
                                                            </div>
                                                            <?php
                                                            $am = $invoice->invoiceFee()->sum('amount');

                                                            $paid = $invoice->invoiceFeesPayment()->sum('paid_amount');

                                                            $unpaid = $am - $paid;
                                                            ?>
                                                            <div class="col-md-12">
                                                                <h1 class="amnt-value">Tsh <?= number_format($unpaid) ?></h1>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                                <table class="table table-invoice">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Description</th>
                                                            <th class="text-center">Quantity</th>
                                                            <th class="text-center">Amount</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $invoice_fee = $invoice->invoiceFee()->get();
                                                        $i = 1;
                                                        foreach ($invoice_fee as $fee) {
                                                            ?>
                                                            <tr>
                                                                <td><?= $i ?></td>
                                                                <td>
                                                                    <h4><?= strtoupper($fee->item_name) ?></h4>
                                                                    <p><?= $fee->note ?></p>
                                                                </td>
                                                                <td class="text-center">1</td>
                                                                <td class="text-center"><?= number_format($fee->amount) ?></td>
                                                            </tr>
                                                            <?php
                                                            $i++;
                                                        }
                                                        ?>

                                                    </tbody>
                                                </table>
                                                <div class="row">
                                                    <div class="col-md-8 col-xs-7 payment-method">

                                                        <p><b style="color:#0066cc">FOR BANKS</b>
                                                            <br/>
                                                      Visit any branch or bank agent of NMB, CRDB, NBC with your control number obtained from the system


                                                            <br/>
                                                            <b>(You are advised to print this invoice and submit it to the bank along with the appreciate amount)</b>


                                                        </p>
                                      

                                                        
                                                        <p><br><b style="color:#0066cc">KUPITIA BANK</b>
                                                            <br/>
                                                      Fika tawi lolote au wakala wa benki ya NMB, CDRB, NBC ukiwa na namba yako ya  kumbukumbu kutoka Kwenye mfumo. 
                                                          


                                                        </p>                     
                                                        
                                                        
                                                        <p>
                                                            <br/><b  style="color:#0066cc">Via Mobile Network Operators (MNO)</b><br/>	Enter to the respective USSD Menu of MNO,Select 4 (Make Payments) for Tigopesa and Mpesa and Select 5 for AirtelMoney,Select 5 (Government Payments),Enter control number,Enter password</p>

                                                        <br>
                                                         <p>
                                                           <b  style="color:#0066cc">Kupitia Mitandao ya Simu</b><br/>
                                                            
Ingia kwenye menyu ya mtandao husika ,Chagua 4 (Lipa Bili) kwa Tigopesa na Mpesa na Chagua 5 kwa AirtelMoney Chagua 5 (Malipo ya Serikali),Ingiza namba ya kumbukumbu ,Weka namba ya siri
</p><br>
                                                        <p><b  style="color:#0066cc">NB;</b><br/>
                                     in case you face any challenge, please call +255 222780228 (INETS CO LTD) OR +255 22 2122836 -(ERB)</p>

                                                        <h3 class="btn btn-primary" data-toggle="tab" href="#home-2" style="background:#0066cc">How to Pay</h3>
                                                    </div>
                                                    <div class="col-md-4 col-xs-5 invoice-block pull-right">
                                                        <ul class="unstyled amounts">
                                                            <li>Sub - Total amount : <?= number_format($invoice->getAmount()) ?></li>
                                                            <li>Paid Amount : <?= $paid > 0 ? $paid : 0 ?> </li>
                                                            <li>Discount :___ </li>
                                                            <li class="grand-total">Total : Tsh <?= number_format($unpaid) ?></li>
                                                        </ul>
                                                    </div>
                                                </div>



                                            </div>
                                        </section>
                                    </div>
                                </div>

                            </div>
                            <div id="home-2" class="tab-pane  ">
                                <div class="col-md-12 col-lg-12">            

                                    <h6 class="text-muted text-normal text-uppercase">CHOOSE YOUR PREFERRED PAYMENT OPTION</h6>
                                    <hr class="margin-bottom-1x">
                                    <section class="panel">
                                        <header class="panel-heading tab-bg-dark-navy-blue tab-right ">
                                            <ul class="nav nav-tabs pull-right" role="tablist">
                                                <li class="nav-item active">
                                                    <a class="nav-link active" href="#mpesa" data-toggle="tab" role="tab" aria-selected="true"><img src="<?= url('public/images/mpesa.jpg') ?>" width="30" height="30" alt="M-pesa Methods">
                                                        MPESA</a></li>
                                                <li class="nav-item"><a class="nav-link" href="#tigopesa" data-toggle="tab" role="tab" aria-selected="false"><img src="<?= url('public/images/tigo.jpg') ?>"  width="30" height="30" alt="M-pesa Methods">TIGOPESA</a></li>
                                                <li class="nav-item"><a class="nav-link" href="#airtelmoney" data-toggle="tab" role="tab" aria-selected="false"><img src="<?= url('public/images/airtel.jpg') ?>"  width="30" height="30" alt="M-pesa Methods">AIRTEL MONEY</a></li>
                                                <li class="nav-item"><a class="nav-link" href="#nmb" data-toggle="tab" role="tab" aria-selected="false"><img src="<?= url('public/images/nmb.jpg') ?>"  width="30" height="30" alt="M-pesa Methods">NMB</a></li> 
                                                <li class="nav-item"><a class="nav-link" href="#crdb" data-toggle="tab" role="tab" aria-selected="false"><img src="<?= url('public/images/crdb.jpg') ?>"  width="30" height="30" alt="M-pesa Methods">CRDB</a></li>

<!--                                                <li class="nav-item"><a class="nav-link" href="#cards" data-toggle="tab" role="tab" aria-selected="false"><img src="<?= url('public/images/nmb.jpg') ?>"  width="30" height="30" alt="M-pesa Methods">Bank Cards</a></li>-->

                                            </ul>
                                        </header>
                                        <div class="panel-body">
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="mpesa" role="tabpanel">
                                                    <ol>
                                                        <ol>
                                                            <li>Go to <strong>MPESA</strong> menu and choose <strong>Pay Bill.</strong></li>
                                                            <li>select Government Payments </li>
                                                            <li>Enter <strong>Control Number </strong> number .</li>
                                                            <li>Enter the amount to pay.</li>
                                                            <li>Key in your <strong>MPESA PIN</strong></li>
                                                            <li>Confirm that you have entered the correct details, then send to complete the transaction.</li>
                                                            <li>Wait for the confirmation SMS</li>
                                                        </ol>
                                                    </ol>
                                                    
                                                </div>
                                                <div class="tab-pane fade" id="tigopesa" role="tabpanel">
                                                    <ol>
                                                        <ol>
                                                            <li>Dial <strong>*150*01#</strong> to access your Tigo Pesa account</li>
                                                            <li>Select 4 – <strong>Pay Bills.</strong>.</li>
                                                            <li>Select 3 – slect  government payments </li>
                                                            <li>Enter <strong>Control Number </strong> number .</li>
                                                            <li>Enter the amount to pay.</li>
                                                            <li>Enter your <strong> PIN</strong> to confirm</li>
                                                            <li>Confirm that you have entered the correct details, then send to complete the transaction.</li>
                                                            <li>Wait for the confirmation SMS</li>
                                                        </ol>
                                                    </ol>
                                                </div>
                                                <div class="tab-pane fade" id="airtelmoney" role="tabpanel">
                                                    <ol>
                                                        <ol>
                                                            <li>Dial <strong>*150*60#</strong> to access your Airtel Money account</li>
                                                            <li>Select 5 – <strong>Pay Bills.</strong>.</li>
                                                            <li>Select 4 –select government payments</li>
                                                            <li>Enter <strong>Control Number </strong> number .</li>
                                                            <li>Enter the amount to pay.</li>
                                                            <li>Enter your <strong> PIN</strong> to confirm</li>
                                                            <li>Confirm that you have entered the correct details, then send to complete the transaction.</li>
                                                            <li>Wait for the confirmation SMS</li>
                                                        </ol>
                                                    </ol>
                                                </div>
                                                <div class="tab-pane fade" id="crdb" role="tabpanel">
                                                    <ol>
                                                        <ol>
                                                            <li>Visit any CRDB BRANCH <strong><a href="http://crdbbank.co.tz/en/branch-locator/" target="_blank">Click here</a></strong> to see list of CRDB Branches near you</li>
                                                            <li>Deposit Total amount by specifying Control Number in a paying slip.</li>

                                                            <li>Wait for the confirmation SMS</li>
                                                        </ol>
                                                    </ol>
                                                </div>
                                                <div class="tab-pane fade" id="nmb" role="tabpanel">
                                                    <ol>
                                                        <ol>
                                                            <li>Visit any NMB BRANCH <strong><a href="https://www.nmbbank.co.tz/about-us/resources/branch-atm-and-wakala-locator" target="_blank">Click here</a></strong> to see list of NMB Branches near you</li>
                                                            <li>Deposit Total amount by specifying Control Number in a paying slip.</li>

                                                            <li>Wait for the confirmation SMS</li>
                                                        </ol>
                                                    </ol>
                                                </div>
                                                <!--                                                <div class="tab-pane fade" id="cards" role="tabpanel">
                                                                                                    <ol>
                                                                                                        <ol>
                                                                                                            <li>Pay Via Master Card or Visa<strong>
                                                                                                                    <a href="<?= url()->current() . '/card' ?>" data-toggle="modal" href="#pay_cards">Click here to pay</a></strong></li>
                                                                                                        </ol>
                                                                                                    </ol>
                                                                                                </div>-->

                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            <div id="contact-2" class="tab-pane ">

                                <div class="col-md-12">
                                    <h3>Common Questions and Answers </h3>
                                    <br/>
                                    <!--collapse start-->
                                    <div class="panel-group m-bot20" id="accordion">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                        What is Reference Control number ?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse" style="height: 0px;">
                                                <div class="panel-body">
                                                    Is the unique number used to identify payments done. Each payment must be done by specifying that Control number. If any payment is received without this reference number, such payment will not be recognized and accepted
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                                        How much will I be charged per transaction ?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseTwo" class="panel-collapse collapse" style="height: 0px;">
                                                <div class="panel-body">
                                                    Transaction fee will be subjected to a payment method you choose. Each payment method has specified charges. For more information about charges, please contact respective financial institution 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                                        I forgot my reference number 
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseThree" class="panel-collapse collapse in" style="height: auto;">
                                                <div class="panel-body">
                                                    If you forgot your control number, you can search it by writing your email address or phone number at the top.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--collapse end-->
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    print_page = function () {
        $('#head_one,#tab_panel_heading').hide();
        $('.widget-header, .btn, .breadcrumb, .clearfix').hide();
        $('#myTab').removeClass('nav-tabs');
        $('#myTab').removeClass('bar_tabs');
        window.print();
        $('#head_one,#tab_panel_heading').show();
        $('.widget-header, .btn, .breadcrumb, .clearfix').show();
        $('#myTab').addClass('nav-tabs');
        $('#myTab').addClass('bar_tabs');
    }
    var tour = new Tour({
        steps: [
            {
                element: "#invoice_number",
                title: "Your Control Number",
                content: "This is your payment Control number. You will use this number to make payments either via Mobile money payments (M-pesa (888999), Tigo-pesa(888999) and Airtel Money(888999) ), via NMB bank or via CRDB Bank",
                placement: 'top', backdrop: true,
                backdropContainer: 'body',
                backdropPadding: false
            },
            {
                element: "#your_invoice",
                title: "Your Control ",
                content: "This is your invoice document. You can print this document for your record purpose", placement: 'bottom', backdrop: true,
                backdropContainer: 'body',
                backdropPadding: false
            },
            {
                element: "#how_to_pay",
                title: "How to pay",
                content: "Choose your preffered option to pay for ERB Annual day. With either method, we will receive your payments and notify you back", placement: 'bottom', backdrop: true,
                backdropContainer: 'body',
                backdropPadding: false
            }
        ]});
    // Initialize the tour
    tour.init();
    // Start the tour
    tour.start();

    $('#invoice_number').removeClass('btn btn-default').addClass('btn btn-success');
    $("#invoice_number").animate({
        opacity: 0.3
    }, 1000, function () {
        $(this).after(
                $("#invoice_number").animate({
            opacity: 1
        }, 1000, function () {
            $(this).after($("#invoice_number").removeClass('btn btn-success').addClass('btn btn-default'))
        })
                )
    });

</script>
@endsection
@include('layouts.help')