@extends('layouts.app')

@section('content')
<style>
    .pt-3-half {
        padding-top: 1.4rem;
    }
</style>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <?php
            //dd(md5('ERB-MIS'));
            if (request('bulk') == 1) {
                ?>
                <header class="panel-heading">
                    Create Sponsored Invoice

                </header>
                <div class="panel-body" style="background: white">
                    <div class="col-lg-12 col-xl-12 ">
                        <!-- <h6 class="sub-title">Tab With Icon</h6> -->
                        <div class="sub-title">Please select a tab you want to add new sponsored members</div>
                        <br/>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs md-tabs " role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#home7" role="tab"><i class="icofont icofont-home"></i>Add Subscriber without Excel</a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#profile7" role="tab"><i class="icofont icofont-ui-user "></i>Upload Via Excel File</a>
                                <div class="slide"></div>
                            </li>

                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content card-block">
                            <div class="tab-pane active" id="home7" role="tabpanel">
                                <div class=" form">
                                    <br/>
                                    <form class="cmxform form-horizontal " id="single" method="post" action="#">
                                        <div class="form-group ">
                                            <label for="type" class="control-label col-lg-3">Employer</label>
                                            <div class="col-lg-6">
                                                <select class="form-control select2_single select2" name="user_id" id="employer_id">
                                                    <?php $userype = \App\Model\Employer::all(); ?>
                                                    @foreach ($userype as $type)
                                                    <option value="{{$type->id}}">{{$type->name}}</option>                                                  @endforeach;
                                                </select>

                                            </div>
                                        </div>


                                        <div class="form-group ">
                                            <label for="price" class="control-label col-lg-3">Price Per User</label>
                                            <div class="col-lg-6">

                                                <input class="form-control " id="amount" type="text" name="amount" value="<?= $amount ?>">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-3">Add Users</label>
                                            <div class="col-lg-6">
                                                <div class="card">

                                                    <div class="card-body">
                                                        <div id="table" class="table-editable">
                                                            <span class="table-add float-right mb-3 mr-2">
                                                                <a href="#!" class="btn btn-sx"><i class="fa fa-plus fa-2x" aria-hidden="true"></i></a></span>
                                                            <table class="table table-bordered table-responsive-md table-striped text-center" id='excel_table'>
                                                                <thead>
                                                                    <tr>
                                                                        <th class="text-center">Person Name</th>
                                                                        <th class="text-center">Phone</th>
                                                                        <th class="text-center">Email</th>
                                                                        <th class="text-center">Profession</th>

                                                                        <th class="text-center">Remove</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id='user_area'>
                                                                    <tr class=''>
                                                                        <td class="firstname" contenteditable="true" >ShuleSoft Hamisi </td>

                                                                        <td class="phone" contenteditable="true">+255655406004</td>

                                                                        <td class="email" contenteditable="true">support@shulesoft.com</td>

                                                                        <td class="profession" contenteditable="true">Engineer</td>
                                                                        <td>
                                                                            <span class="table-remove"><button type="button"
                                                                                                               class="btn btn-danger btn-rounded btn-sm my-0">Remove</button></span>
                                                                        </td>
                                                                    </tr>



                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Editable table -->
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-lg-offset-3 col-lg-6">
                                                <?= csrf_field() ?>
                                                <input type="hidden" value="1" name="noexcel"/>
                                                <button class="btn btn-primary" id="noexcel" type="button">Create Invoice</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="tab-pane" id="profile7" role="tabpanel">
                                <div class="card-block">

                                    <div class="table-responsive dt-responsive">
                                        <div class="card-header">
                                            <div class="panel-body">
                                                <div class="alert alert-info">This will be a one invoice with multiple users within a certain organization. You need to upload excel file with list of applicants who will be paid by this invoice. </div>
                                                <p>Sample Excel Format. </p>
                                                <img src="<?= url('public/images/sample_excel.jpg') ?>"/>
                                                <br/>
                                                <div class=" form">
                                                    <br/>
                                                    <form class="cmxform form-horizontal " id="commentForm" method="post" action="<?= url('invoice') ?>" enctype="multipart/form-data">
                                                        <div class="form-group ">
                                                            <label for="type" class="control-label col-lg-3">Employer</label>
                                                            <div class="col-lg-6">
                                                                <select class="form-control select2_single select2" name="user_id">
                                                                    <?php $userype = \App\Model\Employer::all(); ?>
                                                                    @foreach ($userype as $type)
                                                                    <option value="{{$type->id}}">{{$type->name}}</option>                                                  @endforeach;
                                                                </select>

                                                            </div>
                                                        </div>


                                                        <div class="form-group ">
                                                            <label for="price" class="control-label col-lg-3">Price Per User</label>
                                                            <div class="col-lg-6">

                                                                <input class="form-control " id="number" type="text" name="amount" value="<?= $amount ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group ">
                                                            <label for="cname" class="control-label col-lg-3">Choose Excel File (required)</label>
                                                            <div class="col-lg-6">
                                                                <input class=" form-control" id="cname" name="file" type="file" required="">
                                                            </div>
                                                        </div>


                                                        <div class="form-group">
                                                            <div class="col-lg-offset-3 col-lg-6">
                                                                <?= csrf_field() ?>
                                                                <button class="btn btn-primary" type="submit">Upload to Create Invoice</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>

                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            <?php } else { ?>
                <header class="panel-heading">
                    Create Reference number For Single User

                </header>
                <div class="panel-body">
                    <div class=" form">
                        <form class="cmxform form-horizontal " id="commentForm" method="post" action="<?= url('invoice') ?>">
                            <div class="form-group ">
                                <label for="type" class="control-label col-lg-3">Employer</label>
                                <div class="col-lg-6">
                                    <select class="form-control select2_single select2"  name="employer_id">
                                        <?php $userype = \App\Model\Employer::all() ?>
                                        @foreach ($userype as $type)
                                        <option value="{{$type->id}}">{{$type->name}}</option>                                                  @endforeach;
                                    </select>

                                </div>
                                <?php echo form_error($errors, 'employer_id'); ?>
                                <a data-toggle="modal" href="#myModalEmployer">Or Add new</a>
                            </div>
                            <div class="form-group ">
                                <label for="number" class="control-label col-lg-3">Professional (Required)</label>
                                <div class="col-lg-6">
                                    <select class="form-control select2_single select2" name="profession_id">
                                        <?php
                                        $professions = App\Model\Profession::where('invitee', 0)->get();
                                        ;
                                        ?>
                                        @foreach ($professions as $profession)
                                        <option value="{{$profession->id}}">{{$profession->name}}</option>                                                  @endforeach;
                                    </select>

                                </div>
                                <?php echo form_error($errors, 'profession_id'); ?>
                                <a data-toggle="modal" href="#myModal">Or Add new</a>
                            </div>


                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-3">Name (required)</label>
                                <div class="col-lg-6">
                                    <input class=" form-control" id="cname" name="name" minlength="2" type="text" required="" value="{{old('name')}}"  onblur="this.value = this.value.toUpperCase()"  pattern="[a-zA-Z\. ]{5,}">
                                </div>
                                <span class="help-block">E.g John Joseph</span>
                                @if ($errors->has('name'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group ">
                                <label for="cemail" class="control-label col-lg-3">E-Mail (required)</label>
                                <div class="col-lg-6">
                                    <input class="form-control " id="cemail" type="email" name="email" required=""  value="<?= old('email') ?>"  onblur="this.value = this.value.toLowerCase()">
                                </div>
                                @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group ">
                                <label for="phone" class="control-label col-lg-3">Phone Number</label>
                                <div class="col-lg-6">
                                    <input class="form-control " id="phone" type="text" name="phone"  value="<?= old('phone') ?>">
                                </div>
                                @if ($errors->has('phone'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                                @endif
                            </div>


                            <div class="form-group">
                                <div class="col-lg-offset-3 col-lg-6">
                                    <?= csrf_field() ?>
                                    <button class="btn btn-primary" type="submit">Create Invoice</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <form class="cmxform form-horizontal " id="commentForm" method="post" action="<?= url('user') ?>">

                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title">Add New Profession</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="panel-body">
                                        <div class=" form">
                                            <div class="form-group ">
                                                <label for="cname" class="control-label col-lg-3">Name (required)</label>
                                                <div class="col-lg-6">
                                                    <input class=" form-control" id="name" name="name" minlength="2" type="text" required="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <?= csrf_field() ?>
                                    <input type="hidden" name="user" value="profession"/>
                                    <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                    <button class="btn btn-success" type="submit">Save changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal fade" id="myModalEmployer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <form class="cmxform form-horizontal " id="commentForm" method="post" action="<?= url('user') ?>">

                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title">Add New Entity</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="panel-body">
                                        <div class=" form">
                                            <div class="form-group ">
                                                <label for="cname" class="control-label col-lg-3">Name (required)</label>
                                                <div class="col-lg-6">
                                                    <input class=" form-control" id="cname" name="name" minlength="2" type="text" required="">
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label for="cname" class="control-label col-lg-3">Abbreviation</label>
                                                <div class="col-lg-6">
                                                    <input class=" form-control" id="cname" name="abbreviation"  type="text" required="">
                                                </div>
                                            </div>
                                            <div class="form-group " style="display: none;">
                                                <label for="cemail" class="control-label col-lg-3">E-Mail (required)</label>
                                                <div class="col-lg-6">
                                                    <input class="form-control " id="cemail" type="email" name="email" required="" value="<?= time() . 'jkdjs@engineers.co.tz' ?>">
                                                </div>
                                            </div>
                                            <!--                                            <div class="form-group ">
                                                                                            <label for="curl" class="control-label col-lg-3">Phone (required)</label>
                                                                                            <div class="col-lg-6">
                                                                                                <input class="form-control " id="curl" type="text" name="phone">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-group ">
                                                                                            <label for="ccomment" class="control-label col-lg-3">Location(required)</label>
                                                                                            <div class="col-lg-6">
                                                                                                <textarea class="form-control " id="ccomment" name="location" required=""></textarea>
                                                                                            </div>
                                                                                        </div>-->

                                        </div>

                                    </div>


                                </div>
                                <div class="modal-footer">
                                    <?= csrf_field() ?>
                                    <input type="hidden" name="user" value="employer"/>
                                    <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                    <button class="btn btn-success" type="submit">Save changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            <?php } ?>
        </section>
    </div>
</div>
<script type="text/javascript">
    var table_id = $('#table');
    var $BTN = $('#export-btn');
    var $EXPORT = $('#export');

    const newTr = `
 <tr class="hide">
   <td class="pt-3-half" contenteditable="true">Example</td>
   <td class="pt-3-half" contenteditable="true">Example</td>
   <td class="pt-3-half" contenteditable="true">Example</td>
   <td class="pt-3-half" contenteditable="true">Example</td>
   <td>
     <span class="table-remove"><button type="button" class="btn btn-danger btn-rounded btn-sm my-0 waves-effect waves-light">Remove</button></span>
   </td>
 </tr>`;

    $('.table-add').on('click', 'i', () => {

        const $clone = table_id.find('tbody tr').last().clone(true).removeClass('hide table-line');

        if (table_id.find('tbody tr').length === 0) {

            $('tbody').append(newTr);
        }

        table_id.find('table').append($clone);
    });

    table_id.on('click', '.table-remove', function () {

        $(this).parents('tr').detach();
    });

    table_id.on('click', '.table-up', function () {

        const $row = $(this).parents('tr');

        if ($row.index() === 1) {
            return;
        }

        $row.prev().before($row.get(0));
    });

    table_id.on('click', '.table-down', function () {

        const $row = $(this).parents('tr');
        $row.next().after($row.get(0));
    });

    // A few jQuery helpers for exporting only
    jQuery.fn.pop = [].pop;
    jQuery.fn.shift = [].shift;

    $BTN.on('click', () => {

        const $rows = table_id.find('tr:not(:hidden)');
        const headers = [];
        const data = [];

        // Get the headers (add special header logic here)
        $($rows.shift()).find('th:not(:empty)').each(function () {

            headers.push($(this).text().toLowerCase());
        });

        // Turn all existing rows into a loopable array
        $rows.each(function () {
            const $td = $(this).find('td');
            const h = {};

            // Use the headers from earlier to name our hash keys
            headers.forEach((header, i) => {

                h[header] = $td.eq(i).text();
            });

            data.push(h);
        });

        // Output the result
        $EXPORT.text(JSON.stringify(data));
    });


    single_no_excel_submit = function () {

        $('#noexcel').mousedown(function () {
            var employer_id = $('#employer_id').val();
            var amount = $('#amount').val();
            var ary = [];
            $(function () {
                $('#user_area tr').each(function (a, b) {
                    var name = $('.firstname', b).text();
                    var phone = $('.phone', b).text();
                    var email = $('.email', b).text();
                    var profession = $('.profession', b).text();
                    ary.push({name: name, phone: phone, email: email, profession: profession});

                });
                // alert(JSON.stringify(ary));
            });
            $.ajax({
                type: 'POST',
                url: "<?= url('invoice') ?>",
                data: {amount: amount, user_id: employer_id, noexcel: 1, users: ary},
                dataType: "html",
                success: function (data) {
                    if (data === 'success') {
$('#home7').html('<div clas="alert alert-success">Recorded successfully. To add new record, please refresh </div>');

                    }

                }
            });
        });
    }
    $(document).ready(single_no_excel_submit);
</script>
@endsection


