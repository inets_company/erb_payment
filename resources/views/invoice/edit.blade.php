@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Update Bulk Invoice

            </header>
            <div class="panel-body">
                <div class=" form">
                    <form class="cmxform form-horizontal " id="commentForm">
                        <div class="form-group ">
                            <label for="cname" class="control-label col-lg-3">Organization Name (required)</label>
                            <div class="col-lg-6">
                                <input class=" form-control" id="cname" name="name" minlength="2" type="text" required="" value="{{$invoice->user->name}}" disabled="">
                            </div>
                            @if ($errors->has('name'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group ">
                            <label for="cemail" class="control-label col-lg-3">Reference Number</label>
                            <div class="col-lg-6">
                                <input class="form-control " id="cemail" type="text" disabled="" name="number" required=""  value="<?= $invoice->number ?>">
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="phone" class="control-label col-lg-3">Applicants Members</label>
                            <div class="col-lg-6">
                                <?php
                                $invoice_fees = $invoice->invoiceFee()->get();
                                ?>
                                <section class="panel">
                                    <header class="panel-heading">
                                        Members Associated
                                    </header>
                                    <div class="panel-body">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>Phone</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if (count($invoice_fees) > 0) {
                                                    $i = 1;
                                                    foreach ($invoice_fees as $invoice_fee) {
                                                        ?>
                                                        <tr>
                                                            <td>{{$i}}</td>
                                                            <td>{{$invoice_fee->user->name}}</td>
                                                            <td>{{$invoice_fee->user->email}}</td>
                                                            <td>{{$invoice_fee->user->phone}}</td>
                                                            <td> <a href="<?= url('invoice/feeDelete/' . $invoice_fee->id) ?>" class="btn btn-xs btn-danger" onclick="return confirm('you are about to delete a record. This cannot be undone. are you sure? ')">Delete</a></td>
                                                        </tr>
                                                        <?php
                                                        $i++;
                                                    }
                                                }
                                                ?>
                                                <tr>
                                                    <td colspan="4"></td>
                                                    <td><a  data-toggle="modal" href="#add_member" class="btn btn-xs btn-success"><i class="fa fa-plus"></i>Add</a></td> </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </section>   

                            </div>
                        </div>
   </form>
                        <div class="modal fade" id="add_member" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title">Add Member</h4>
                                    </div>
                                    <form class="cmxform form-horizontal " id="commentForm"  method="post" action="<?= url('invoice/addUserFee/'.$invoice->id) ?>">
                                        <div class="modal-body">


                                            <div class="form-group ">
                                                <label for="cname" class="control-label col-lg-3">Name (required)</label>
                                                <div class="col-lg-6">
                                                    <input class=" form-control" id="cname" name="name" minlength="2" type="text" required="" value="{{old('name')}}"  onblur="this.value = this.value.toUpperCase()">
                                                </div>
                                                @if ($errors->has('name'))
                                                <span class="invalid-feedback">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group ">
                                                <label for="cemail" class="control-label col-lg-3">E-Mail (required)</label>
                                                <div class="col-lg-6">
                                                    <input class="form-control " id="cemail" type="email" name="email" required=""  value="<?= old('email') ?>"  onblur="this.value = this.value.toLowerCase()">
                                                </div>
                                                @if ($errors->has('email'))
                                                <span class="invalid-feedback">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group ">
                                                <label for="phone" class="control-label col-lg-3">Phone Number</label>
                                                <div class="col-lg-6">
                                                    <input class="form-control " id="phone" type="text" name="phone"  value="<?= old('phone') ?>">
                                                </div>
                                                @if ($errors->has('phone'))
                                                <span class="invalid-feedback">
                                                    <strong>{{ $errors->first('phone') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        
                                                 <div class="form-group ">
                                            <label for="number" class="control-label col-lg-3">Specialization (Required)</label>
                                            <div class="col-lg-6">
                                                <select class="form-control select2_single select2" name="profession_id">
                                                             <option value=""></option>                                     
                                                    <?php $professions = App\Model\Profession::where('invitee',0)->get();; ?>
                                                    @foreach ($professions as $profession)
                                                    <option value="{{$profession->id}}">{{$profession->name}}</option>                                                  @endforeach;
                                                </select>

                                            </div>
                                             <?php echo form_error($errors, 'profession_id'); ?>
                                            <!--<a data-toggle="modal" href="#myModal">Or Add new</a>-->
                                        </div>
                                        </div>
                                        <div class="modal-footer">
                                            
                                            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button> <input type="hidden" name="employer_id" value="{{$invoice->user->employer_id}}"/>
                                             <input type="hidden" name="user_type_id" value="7"/>
                                              <?= csrf_field() ?>
                                            
                                            <button  class="btn btn-success" type="submit">Save</button>
                                        </div>
                                   </form>
                                </div>
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-3 col-lg-6">
                              
                                <a class="btn btn-primary" href="<?= url('invoice/bulk') ?>">Return Back</a>
                            </div>
                        </div>
                  
                </div>

            </div>
        </section>
    </div>
</div>
@endsection