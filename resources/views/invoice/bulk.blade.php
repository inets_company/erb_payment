@extends('layouts.app')

@section('content')

<!-- page start-->

<div class="row">
    <div class="col-sm-12">
      
        <section class="panel">
            <header class="panel-heading">
                Sponsored Invoices

            </header>
              <div class="col-md-12">
                    <div class="mini-stat clearfix">
                        <span class="mini-stat-icon orange"><i class="fa fa-user"></i></span>
                        <div class="mini-stat-info">
                            <span><?=\App\Model\Invoice_fee::whereIn('invoice_id',\App\Model\Invoice::where('type',1)->get(['id']))->count()?></span>
                            Total sponsored
                        </div>
                    </div>
                </div>
            <?php
           // dd(md5('ERB-MIS'));
            if (can_access('add_invoices')) { ?>
                <p>
                    <br/>
                    &nbsp; <a href="<?= url('invoice/create/?bulk=1') ?>" class="btn btn-primary">Create Sponsored Invoice</a>
                </p>
            <?php } ?>
                
            <div class="panel-body">
                <div class="position-center">
                    <form class="form-inline" role="form" action="<?= url('invoice/bulk') ?>" method="get">
                        <div class="form-group">
                            <label class="from" for="From">From Date</label>
                            <input type="date" class="form-control" id="from" name="from" placeholder="" value="{{$from}}">
                        </div>
                        <div class="form-group">
                            <label class="to" for="To">To Date</label>
                            <input type="date" class="form-control" id="to" name="to" placeholder="" value="{{$to}}">
                        </div>

                        <button type="submit" class="btn btn-success">Search</button>
                    </form>
                </div>
                <p></p>
                <section id="unseen">
                    <table  class="table table-bordered table-striped table-condensed dataTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Organization Name</th>
                                <th>Total Users</th>
                                <th class="numeric">Reference Number</th>
                                <th class="numeric">Date</th>
                                <th class="numeric">Amount</th>
                                <th class="numeric">Payment For</th>
                                <th class="numeric">Paid Amount</th>
                                <th class="numeric">UnPaid Amount</th>
                                <th class="numeric">Payment Status</th>
                                <th class="numeric col-md-3">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $total_amount = 0;
                            $total_paid = 0;
                            $total_unpaid = 0;
                            $i = 1;
                            ?>
                            @foreach($invoices as $invoice)
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$invoice->user->name}}</td>
                                <td>{{$invoice->invoiceFee()->count()}}</td>
                                <td class="numeric">{{$invoice->number}}</td>
                                <td class="numeric">{{$invoice->date}}</td>
                                <td data-title="">
                                    <?php
                                    $am = $invoice->invoiceFee()->sum('amount');
                                    $total_amount += $am;
                                    echo money($am);
                                    ?>
                                </td>
                                <td class="numeric">{{$invoice->title}}</td>

                                <td data-title="">
                                    <?php
                                    $paid = $invoice->invoiceFeesPayment()->sum('paid_amount');
                                    $total_paid += $paid;
                                    echo money($paid);
                                    ?>
                                </td>
                                <td data-title="">
                                    <?php
                                    $unpaid = $am - $paid;
                                    $total_unpaid += $unpaid;
                                    echo money($unpaid);
                                    ?>
                                </td>
                                <td class="numeric"><?php
                                    if ($invoice->status == 1) {
                                        echo '<span class="label label-success">Paid</span>';
                                    } else if ($invoice->status == 2) {
                                        echo '<span class="label label-warning">Partially Paid</span>';
                                    } else {
                                        echo '<span class="label label-danger">Not Paid</span>';
                                    }
                                    $i++;
                                    ?></td>

                                <td class="numeric">
                                    <a href="<?= url('invoice/' . $invoice->id) ?>" class="btn btn-xs btn-success">View</a>
                                    <?php if (can_access('edit_invoices')) { ?>
                                    <a href="<?= url('invoice/' . $invoice->id . '/edit') ?>" class="btn btn-xs btn-info">Edit</a>   <?php } ?>       
                                    <?= can_access('delete_invoices') ? btn_delete('invoice/' . $invoice->id, '') : '' ?>
                                    <?php if ($invoice->status <> 1 && can_access('add_payments')) { ?>
                                        <a href="<?= url('payment/add?id=' . $invoice->id) ?>" class="btn btn-primary btn-xs">Payment </a>
                                    <?php } ?>


                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="5">Total</td>
                                <td><?= money($total_amount) ?></td>
                                <td></td>
                                <td><?= money($total_paid) ?></td>
                                <td><?= money($total_unpaid) ?></td>
                                <td colspan="3"></td>
                            </tr>
                        </tfoot>
                    </table>
                </section>
           </div>
        </section>
    </div>
</div>
<!-- page end-->

@endsection
