<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('number');
            $table->integer('user_id');
            $table->string('title');
            $table->string('optional_name');
            $table->timestampTz('date');
            $table->smallInteger('status');
             $table->smallInteger('type')->default(0);
            $table->string('year');
            $table->smallInteger('active')->default(1);
            $table->smallInteger('sync')->default(0);
            $table->string('return_message');
            $table->string('push_status')->comment('Final push status for such invoice. It can be invoice update or invoice submission ');
            $table->mediumText('note');
            $table->timestamps();
            $table->unique('number');
            $table->foreign('user_id')->references('id')->on('users')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('invoices');
    }

}
