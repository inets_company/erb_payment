<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceFeesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('invoice_fees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('invoice_id');
            $table->float('paid_amount');
            $table->float('amount');
            $table->string('paymenttype');
            $table->timestampTz('paid_date');
            $table->smallInteger('status')->default(0)->comment('checking if amount is paid, 0-not paid,1-paid,2-partially paid');
            $table->string('note');
            $table->string('item_name');
            $table->integer('user_id')->comment('Redundant column. This column was not supposed to be here, but, due to the concept of bulk invoice (one company with mutliple users), this column is useful to track users belonging to this one entity');
            $table->timestamps();
            $table->foreign('invoice_id')->references('id')->on('invoices')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('user')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('invoice_fees');
    }

}
