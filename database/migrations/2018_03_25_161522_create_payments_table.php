<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('invoice_id');
            $table->float('amount');
            $table->float('transaction_fee');
            $table->string('method');
            $table->integer('transaction_id');
            $table->integer('mobile_transaction_id');
            $table->timestampTz('transaction_time')->comment('from third party, bank, mobile etc. At what time that payment was made');
            $table->string('account_number');
            $table->string('token')->comment('If token is not empty, then this transaction is done with e-payments so we need to also disable option to delete this transaction');
            $table->integer('bank_account_id');
            $table->integer('financial_entity_id');
            $table->smallInteger('status');
            $table->smallInteger('reconciled');
            $table->mediumText('note');
            $table->timestamps();
            $table->unique('transaction_id');
            $table->foreign('invoice_id')->references('id')->on('invoices')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('payments');
    }

}
